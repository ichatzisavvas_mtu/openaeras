import scipy as sp
import numpy as np

def g_spearman_coeff_calc(param_study, input_param, output_param):
    """
    Calculate and plot the Pearson coefficients.       

    Parameters
    ----------
    param_study : TYPE
        DESCRIPTION.
    input_param : TYPE
        DESCRIPTION.
    output_param : TYPE
        DESCRIPTION.

    Returns
    -------
    s_coef : TYPE
        DESCRIPTION.

    """
    s_coef = np.zeros([input_param, output_param])
    s_coef_p = np.zeros([input_param, output_param])
    for jj in range (input_param):
        for kk in range (output_param):
            print (param_study.iloc[:,jj+1])
            s_coef[jj,kk] = sp.stats.spearmanr(param_study.iloc[:,jj+1], param_study.iloc[:,input_param + kk + 1])[0]
            s_coef_p[jj,kk] = sp.stats.spearmanr(param_study.iloc[:,jj+1], param_study.iloc[:,input_param + kk + 1])[1]
            
    
    return s_coef, s_coef_p
