"""
Flexibility matrix parameter study with Gmsh and CalculiX.

The following steps are performed in this file:
    1) The parameters are loaded from a .csv file
    2) The 2d geometry of the conical beam element is created
    3) The 3d geometry is extruded
    4) The 3d mesh is generated
    5) Physical groups for the CalculiX calculation are defined
    6) The mesh is exported
    7) The CalculiX static analysis is performed

author: Lukas Braun
"""

import numpy as np
import os
import shutil
import gmsh
import io

# mesh parameter:
# number of grid points in z axis direction
number_horizontal = 50
# number of grid points in x axis direction
number_vertical = 4
# a third of the number of grid points in peripheral direction
number_rotational = 30

# specify nodes for fixation 
#   whole surface (fix_stat = 1)
#   only outer circumferential line (fix_stat = 2)
fix_stat = 1


#%%
def calculiX_static_analysis_str():
    
    """    
    String with the CalculiX commands.
    
    Parameters
    ----------
    None.
    
    Returns
    ----------
    None.
    
    """
    
    calc_script = """**
*MATERIAL,NAME=SQC_MAT
*ELASTIC,TYPE=ISO
*DENSITY
*SOLID SECTION, ELSET=Volume, MATERIAL=SQC_MAT
**
*INITIAL CONDITIONS, TYPE=TEMPERATURE
**
************
*STEP
*STATIC
*BOUNDARY, OP=NEW
Fixation,1,6,0.0
*CLOAD, OP=NEW
REFNOD,       1,    1.0
*NODE FILE
U
*EL FILE
S,E
*NODE PRINT, NSET = OUTPUT
U,
*END STEP
************
*STEP
*STATIC
*BOUNDARY, OP=NEW
Fixation,1,6,0.0
*CLOAD, OP=NEW
ROTNOD,       2,    1.0
*NODE FILE
U
*EL FILE
S,E
*NODE PRINT, NSET = OUTPUT
U
*END STEP
************
*STEP
*STATIC
*BOUNDARY, OP=NEW
Fixation,1,6,0.0
*CLOAD, OP=NEW
REFNOD,       3,    1.0
*NODE FILE
U
*EL FILE
S,E
*NODE PRINT, NSET = OUTPUT
U
*END STEP
************
*STEP
*STATIC
*BOUNDARY, OP=NEW
Fixation,1,6,0.0
*CLOAD, OP=NEW
ROTNOD,       3,    1.0
*NODE FILE
U
*EL FILE
S,E
*NODE PRINT, NSET = OUTPUT
U,
*END STEP
************
"""
                    
    return calc_script


#%%
# read csv parameter file
# material
# Young's modulus, Poisson's ratio, temperature, density
mat = np.genfromtxt("parameter.csv", skip_header=2, delimiter=",")
E = mat[0, 0]
nu = mat[0, 1]
T = mat[0, 2]
rho = mat[0, 3]

# geometry parameter
# angle, length, radius, inner radius left
data = np.loadtxt("parameter.csv", skiprows=5, delimiter=",")
# number of geometrical cases
number_case = len(data)
number_points = 4

# loop over the geometrical cases
for l in range(0, number_case):
    
    # initial the gmsh model
    gmsh.initialize()
    gmsh.model.add("element")
    
    # define parameter for current case
    angle = data[l, 0]
    length = data[l, 1]
    thickness = data[l, 2]
    radius = data[l, 3]
    
    # define points of model 
    gmsh.model.geo.addPoint(radius,0,0,1)
    gmsh.model.geo.addPoint(radius+length*np.tan(angle*np.pi/180),0,length,2)
    gmsh.model.geo.addPoint(radius+thickness+length*np.tan(angle*np.pi/180),0,length,3)
    gmsh.model.geo.addPoint(radius+thickness,0,0,4)
    
    # define points for static analysis
    p1 = gmsh.model.geo.addPoint(0,0,0,1000001)
    p2 = gmsh.model.geo.addPoint(0,0,length,1000002)
    p3 = gmsh.model.geo.addPoint(1e-5,0,length,1000003)
    
    # define lines            
    for j in range(0, number_points-1):
        gmsh.model.geo.addLine(j+1, j+2, j+1)    
    gmsh.model.geo.addLine(number_points, 1, number_points)  
    
    # define surface            
    gmsh.model.geo.addCurveLoop(np.linspace(1, number_points, number_points), 1)
    poly = gmsh.model.geo.addPlaneSurface([1], 1)
    
    # define transfinite curve and surface for a rectangular 2D grid      
    for i in range(0, number_points//2-1):
        gmsh.model.geo.mesh.setTransfiniteCurve(i, number_horizontal, "Progression", 1.0)
    for i in range(number_points//2, number_points-1):
        gmsh.model.geo.mesh.setTransfiniteCurve(i, number_horizontal, "Progression", 1.0)    
    gmsh.model.geo.mesh.setTransfiniteCurve(number_points, number_vertical, "Progression", 1.0)
    gmsh.model.geo.mesh.setTransfiniteCurve(number_points//2, number_vertical, "Progression", 1.0)
    gmsh.model.geo.mesh.setTransfiniteSurface(1, "Left", [1, number_points//2, number_points//2+1, number_points]) 
    gmsh.model.geo.mesh.setRecombine(2, 1)
    
    # whole surface as fixation
    if fix_stat == 1:
        # create nodes for fixation
        fix1 = gmsh.model.geo.revolve([(1,number_points)],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        fix2 = gmsh.model.geo.revolve([(1,fix1[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        fix3 = gmsh.model.geo.revolve([(1,fix2[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        # create nodes for force and momentum     
        force1 = gmsh.model.geo.revolve([(1,number_points//2)],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        force2 = gmsh.model.geo.revolve([(1,force1[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        force3 = gmsh.model.geo.revolve([(1,force2[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        
    # outer circumferential line as fixation
    elif fix_stat == 2:
        # create nodes for fixation
        fix1 = gmsh.model.geo.revolve([(0,number_points)],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        fix2 = gmsh.model.geo.revolve([(0,fix1[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        fix3 = gmsh.model.geo.revolve([(0,fix2[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        # create nodes for force and momentum     
        force1 = gmsh.model.geo.revolve([(0,number_points//2+1)],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        force2 = gmsh.model.geo.revolve([(0,force1[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        force3 = gmsh.model.geo.revolve([(0,force2[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
        
    # create 3D geometry    
    vol1 = gmsh.model.geo.revolve([(2,1)],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
    vol2 = gmsh.model.geo.revolve([(2,vol1[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
    vol3 = gmsh.model.geo.revolve([(2,vol2[0][1])],0,0,0,0,0,1,2*np.pi/3,[number_rotational], [], True)
    
    # create the 3D mesh (20 node brick)    
    gmsh.model.geo.synchronize()
    # with the following two commands a mesh of 20 node bricks is created
    gmsh.option.setNumber("Mesh.ElementOrder", 2)
    gmsh.option.setNumber("Mesh.SecondOrderIncomplete", 1)
    gmsh.model.mesh.generate(3)
    
    # define physical groups 
    # whole surface as fixation
    if fix_stat == 1:
        Fixation = gmsh.model.addPhysicalGroup(2, [fix1[1][1], fix2[1][1], fix3[1][1]])
        gmsh.model.setPhysicalName(2, Fixation, "Fixation")
        Force_moment = gmsh.model.addPhysicalGroup(2, [force1[1][1], force2[1][1], force3[1][1]])
        gmsh.model.setPhysicalName(2, Force_moment, "Force_moment")
        
    # outer circumferential line as fixation    
    if fix_stat == 2:
        Fixation = gmsh.model.addPhysicalGroup(1, [fix1[1][1], fix2[1][1], fix3[1][1]])
        gmsh.model.setPhysicalName(1, Fixation, "Fixation")
        Force_moment = gmsh.model.addPhysicalGroup(1, [force1[1][1], force2[1][1], force3[1][1]])
        gmsh.model.setPhysicalName(1, Force_moment, "Force_moment")     
        
    Volume = gmsh.model.addPhysicalGroup(3, [vol1[1][1], vol2[1][1], vol3[1][1]])
    gmsh.model.setPhysicalName(3, Volume, "Volume")
    REFNOD = gmsh.model.addPhysicalGroup(0, [p2])
    gmsh.model.setPhysicalName(0, REFNOD, "REFNOD")
    ROTNOD = gmsh.model.addPhysicalGroup(0, [p3])
    gmsh.model.setPhysicalName(0, ROTNOD, "ROTNOD")
    OUTPUT = gmsh.model.addPhysicalGroup(0, [p2, p3])
    gmsh.model.setPhysicalName(0, OUTPUT, "OUTPUT")
    
    # export the mesh   
    # export options
    gmsh.option.setNumber("Mesh.SaveGroupsOfElements", 1)
    gmsh.option.setNumber("Mesh.SaveGroupsOfNodes", 1)
    # gmsh.write("conical_geo_" + str(l+1) + ".geo_unrolled")
    gmsh.write("element_raw_" + str(l+1) + ".inp")
    
    # in the following the .inp file is modified and adapted to CalculiX   
    flag = 1
    f = open("element_" + str(l+1) + ".inp", "w")
    f.truncate(0)
    
    # delete 2D elements in the .inp file 
    linelist = open("element_raw_" + str(l+1) + ".inp").readlines()
    for line in linelist:
        if line.startswith("*ELEMENT, type=CPS8"):
            flag = 0
        if line.startswith("*ELEMENT, type=T3D3"):
            flag = 0   
        if line.startswith("*ELSET,ELSET=Fixation"):
            flag = 0   
        if line.startswith("*ELSET,ELSET=Force_moment"):
            flag = 0        
        if line.startswith("*ELEMENT, type=C3D20"):
            flag = 1
        if line.startswith("*ELSET,ELSET=Volume"):
            flag = 1    
        if flag:
            f.write(line)
           
    # include CacluliX commands        
    f.write("******CalculiX analysis******\n") 
    f.write("************\n")
    f.write("****** Geometry:\n")
    f.write("****** Angle: %s°\n" % angle)
    f.write("****** Length: %s mm\n" % length)
    f.write("****** Thickness: %s mm\n" % thickness)
    f.write("****** Inner radius left: %s mm\n" % radius)
    f.write("************\n")
    f.write("*RIGID BODY,NSET=Force_moment,REF NODE=  %s,ROT NODE=  %s\n" % (number_points+2, number_points+3))
    calc_script = calculiX_static_analysis_str()
    text = io.StringIO(calc_script)
    linelist = text.readlines()
    for line in linelist:
        f.write(line)
        # include the material data
        if line.startswith("*ELASTIC"):
            f.write("%s,%s,%s\n" % (E, nu, T))
        if line.startswith("*DENSITY"):
            f.write("%s\n" % rho)
        if line.startswith("*INITIAL CONDITIONS"):    
            f.write("Volume, %s\n" % T)
    f.close()   
    
    # delete file that included 2D elements
    os.remove("element_raw_" + str(l+1) + ".inp")    
    
    # finish gmsh  
    # gmsh.fltk.run()
    gmsh.finalize()   
    
    # create new folder and move nastran .bdf file there
    if os.path.exists("element_" + str(l+1)):
        shutil.rmtree("element_" + str(l+1))
    os.makedirs("element_" + str(l+1))
    shutil.move("element_%s.inp" % str(l+1), "element_%s/element_%s.inp" % (str(l+1), str(l+1)))

    # perform the Nastran linear static analysis
    os.chdir("element_%s" % str(l+1))
    os.system("yes | ccxlsf -j element_" + str(l+1) + ".inp")
    #os.system("ccx element_" + str(l+1))
    os.chdir("..")
