"""
Parameter study of cylindrical beam elements.

author: Lukas Braun
"""

import numpy as np
import csv

from openAERAS.Analysis import quasi_static_analysis
from openAERAS.Structure_Modeling import beam_assembly
from openAERAS.Graphical_Interface import non_graphical_interface

# values of the force and moment load 
force_value = 1.
moment_value = 1.

# read csv parameter file
# number of lines of the header
header = 5
# material
# Young's modulus, Poisson's ratio, temperature, density
mat = np.genfromtxt("parameter.csv", skip_header=2, delimiter=",")
E = mat[0, 0]
nu = mat[0, 1]
T = mat[0, 2]
rho = mat[0, 3]
# geometry parameter
# angle, length, radius, inner radius left, number of elements
data = np.loadtxt("parameter.csv", skiprows=header, delimiter=",")
# number of geometrical samples
number_case = len(data)

# initialize vector for alpha, beta, gamma, delta and epsilon
alpha = np.zeros(number_case)
beta = np.zeros(number_case)
gamma = np.zeros(number_case)
delta = np.zeros(number_case)
epsilon = np.zeros(number_case)

# loop over the geometrical samples
for l in range(0, number_case):
    # define geometry parameter for current sample
    angle = data[l, 0]
    length = data[l, 1]
    thickness = data[l, 2]
    radius = data[l, 3]
    number_of_elements = int(data[l, 4])

    # Rotating Part Definition
    damping_factor = 0    # damping in shaft
    damping_type = 'stiffness_proportional'  # type of damping
    rotor_start = {"lp_1": 0}  # starting point of rotors [mm]
    # rotor speed ratio
    rotor_speed_ratio = [["lp", [1], 'negative_z']]

    # define the shaft with beam elements
    rotor_shaft = ([
     [[1, rotor_start["lp_1"], damping_type, damping_factor, rotor_speed_ratio[0]]],
    ])
    # radius in the middle of the geometry is used
    radius_new = radius+length/number_of_elements/2*np.tan(angle*np.pi/180)
    for k in range(number_of_elements):
        rotor_shaft[0].append([["Beam"], length/number_of_elements,
                               [2*(radius_new+thickness), 2*(radius_new)], [], rho, E, nu])
        radius_new = radius_new+length/number_of_elements*np.tan(angle*np.pi/180)
        
    # Disk Definition
    rotor_blade_disk = ([])
    
    # Casing Definition
    casing_flex = ([])
    
    casing_rigid = ([])
    
    # Bearing Definition
    rotor_casing_bearing = ([
     ["linear_bearing_cc", ["rotor", 1, 1], [], [1e30, 1e30, 1e30, 1e30, 1e30, 1e30], [0, 0, 0, 0, 0, 0]]
    ])
    
    # Span Coupling Definition
    span_coupling = ([])

    # Unbalance
    rotor_unbalance = ([])

    # define forces and moments at the right side
    static_force = ([
     [["rotor", 1, number_of_elements+1], [force_value, 0, 0, 0, 0, 0]],
     [["rotor", 1, number_of_elements+1], [0, 0, 0, 0, moment_value, 0]],
     [["rotor", 1, number_of_elements+1], [0, 0, force_value, 0, 0, 0]],
     [["rotor", 1, number_of_elements+1], [0, 0, 0, 0, 0, moment_value]]
    ])
    
    # Collecting rotor/bearing/casing/unbalance
    rotor_casing_geometry = [rotor_shaft, rotor_blade_disk, casing_flex, casing_rigid,\
                             rotor_casing_bearing, span_coupling, [rotor_unbalance], static_force]

    # For quasistatic analysis
    qs_ax_eq = 1  # axis equal
    qs_n_p_e = 6  # 6 or 4 (axial and torsional removed)
    qs_analysis_type = "static_force"
    
    # Define Analysis Type
    graphic_plot = False
    q_static_analysis = True
    
    # Rotor/Casing Visualisation
    if graphic_plot:
        non_graphical_interface.vis_rotor_casing(rotor_casing_geometry, ax_eq=1)
     
    # Model Creation
    M, C, G, K = beam_assembly.beam_structure_assembly(rotor_casing_geometry, False)
        
    # Quasi Static Analysis
    for i in range(len(static_force)):
        subcase = i+1
        if q_static_analysis:
            quasi_static_response = \
                quasi_static_analysis.calcs_quasi_static_analysis_flex(M, C, G, K,
                                                                       rotor_casing_geometry, qs_ax_eq, qs_n_p_e,
                                                                       qs_analysis_type,subcase)
        
        if subcase == 1:
            alpha[l] = quasi_static_response[-1-5]
            gamma[l] = quasi_static_response[-1-1]
        elif subcase == 2:
            beta[l] = quasi_static_response[-1-1]
        elif subcase == 3:
            delta[l] = quasi_static_response[-1-3]
        elif subcase == 4:
            epsilon[l] = quasi_static_response[-1]
        
# open the original parameter file 
with open("parameter.csv") as csvin:
    readfile = csv.reader(csvin, delimiter=',')
    # open the new parameter file with output data 
    with open("parameter_output_cylindrical.csv", 'w') as csvout:
        writefile = csv.writer(csvout, delimiter=',', lineterminator='\n')
        # line index i
        i = 1
        # write output data as new columns next to the parameters
        for row in readfile:
            # write just ","  in the extended columns
            if i < header+1: 
                nothing = ''
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                writefile.writerow(row)
                i = i+1
            # write alpha, beta, gamma, delta and epsilon  
            else:
                # open .txt file with the necessary data for each sample
                row.append(alpha[i-header-1])  # alpha
                row.append(beta[i-header-1])  # beta
                row.append(gamma[i-header-1])  # gamma
                row.append(delta[i-header-1])  # delta
                row.append(epsilon[i-header-1])  # epsilon
                writefile.writerow(row)
                i = i+1
