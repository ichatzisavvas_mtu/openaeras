import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import rc
rc('font', **{'family': 'serif', 'serif': ['Palatino']})
# rc('text', usetex=False)
rc('text', usetex=True)


#%%    
def g_pearson_spearman_plots(coef_param, coefficient):
    
    """
    This file plots Pearson or Spearman coefficients as a heatmap.
    
   
    """
    
    # create plain plot
    fig, ax = plt.subplots() 
    im = ax.imshow(coef_param[0]) 
    
    # We want to show all ticks...
    ax.set_xticks(np.arange(len(coef_param[2])))
    ax.set_yticks(np.arange(len(coef_param[1])))
    # ... and label them with the respective list entries
    ax.set_xticklabels(coef_param[2])
    ax.set_yticklabels(coef_param[1])
    
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    
    # Loop over data dimensions and create text annotations.
    for i in range(len(coef_param[1])):
        for j in range(len(coef_param[2])):
            text = ax.text(j, i, np.floor(coef_param[0][i, j]*100)/100,
                           ha="center", va="center", color="w")
    
    if coefficient == "Pearson":
        ax.set_title("Pearson Coefficients")
#    fig.tight_layout()
#    plt.show()
    
    return


#%%
def g_pearson_spearman_plots_advanced(coef, inp, outp):
  
    """
    This function plots a heatmap with Pearson or Spearman coefficients. The input parameters are on the horizontal
    axis and the output parameters on the vertical axis.

    author: Lukas Braun

    based on: https://towardsdatascience.com/better-heatmaps-and-correlation-matrix-plots-in-python-41445d0f2bec

    Parameters
    ----------
    coef : array with floats
        Pearson or Spearman coefficients in an array.
    inp : vector with strings
        names of the input parameters (x axis).
    outp : vector with  strings
        names of the output parameters (y axis).

    Returns
    -------
    None.

    """
    
    # fond size
    fsize = 15
    # marker size
    size_scale = 1200
    # number of colors 
    n_colors = 256
    
    # create plain plot
    fig, ax = plt.subplots() 
    
    # create data frame
    corr = pd.DataFrame(data=coef[0:, 0:],    # values
                        index=inp,    # input space
                        columns=outp)  # output space
    
    # create pairs
    corr = pd.melt(corr.reset_index(), id_vars='index') 
    corr.columns = ['x', 'y', 'value']
    x = corr['x']
    y = corr['y']
    size = corr['value'].abs()
    color = corr['value']
    
    # mapping from column names to integer coordinates
    x_labels = [v for v in x.unique()]
    y_labels = [v for v in y.unique()]
    x_to_num = {p[1]: p[0] for p in enumerate(x_labels)}
    y_to_num = {p[1]: p[0] for p in enumerate(y_labels)}
    
    # create the color palette
    palette = sns.diverging_palette(20, 220, n=n_colors)
    # Range of values that will be mapped to the palette, i.e. min and max possible correlation
    color_min, color_max = [-1, 1]

    # define a function that returns a color based on its value
    def value_to_color(val):
        # position of value in the input range, relative to the length of the input range
        val_position = float((val - color_min)) / (color_max - color_min)
        # target index in the color palette
        ind = int(val_position * (n_colors - 1))
        return palette[ind]
    
    # setup of the grid
    plot_grid = plt.GridSpec(1, 15, hspace=0.2, wspace=0.1) 
    ax = plt.subplot(plot_grid[:, :-1])
    
    # plot the data
    ax.scatter(
        x=x.map(x_to_num),  # Use mapping for x
        y=y.map(y_to_num),  # Use mapping for y
        s=size * size_scale,  # Vector of square sizes, proportional to size parameter
        c=color.apply(value_to_color),  # Vector of square color values, mapped to color palette
        marker='s'  # Use square as scatterplot marker
    )
    
    # show column labels on the axes
    ax.set_xticks([x_to_num[v] for v in x_labels])
    ax.set_xticklabels(x_labels, fontsize=fsize)
    ax.set_yticks([y_to_num[v] for v in y_labels])
    ax.set_yticklabels(y_labels, fontsize=fsize, rotation=45, horizontalalignment='right')
    
    # change the grid and the position of the markers
    ax.grid(False, 'major')
    ax.grid(True, 'minor')
    ax.set_xticks([t + 0.5 for t in ax.get_xticks()], minor=True)
    ax.set_yticks([t + 0.5 for t in ax.get_yticks()], minor=True)
    ax.set_xlim([-0.5, max([v for v in x_to_num.values()]) + 0.5]) 
    ax.set_ylim([-0.5, max([v for v in y_to_num.values()]) + 0.5])
    
    # add color legend on the right side of the plot
    ax = plt.subplot(plot_grid[:, -1])
    col_x = [0]*len(palette) 
    bar_y = np.linspace(color_min, color_max, n_colors)
    bar_height = bar_y[1] - bar_y[0]
    ax.barh(
        y=bar_y,
        width=[5]*len(palette), 
        left=col_x, 
        height=bar_height,
        color=palette,
        linewidth=0
    )
    ax.set_xlim(1, 2) 
    ax.grid(False) 
    ax.set_facecolor('white') 
    ax.set_xticks([]) 
    # show 5 ticks
    ax.set_yticks(np.linspace(min(bar_y), max(bar_y), 5)) 
    ax.tick_params(labelsize=fsize)
    ax.yaxis.tick_right() 
    
    return None
