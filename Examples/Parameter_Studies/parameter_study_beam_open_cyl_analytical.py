"""
This file performs a parameter study of a cylindrical beam with closed analytical equations
based on the Timoshenko beam theory.

author: Lukas Braun
"""

import numpy as np
import csv

import openAERAS.Benchmarks.Analytical_Solutions.clamped_beam_cylinder_analytical_solution as analytics

# values of the force and moment load 
force_value = 1.
moment_value = 1.

# read csv parameter file
# number of lines of the header
header = 5
# material
# Young's modulus, Poisson's ratio, temperature, density
mat = np.genfromtxt("parameter.csv", skip_header=2, delimiter=",")
E = mat[0, 0]
nu = mat[0, 1]
T = mat[0, 2]
rho = mat[0, 3]
# geometry parameter
# angle, length, radius, inner radius left, number of elements
data = np.loadtxt("parameter.csv", skiprows=header, delimiter=",")
# number of geometrical samples
number_case = len(data)

# initialize the vector for alpha, beta, gamma, zeta1 and zeta2
alpha = np.zeros(number_case)
beta = np.zeros(number_case)
gamma = np.zeros(number_case)
zeta1 = np.zeros(number_case)
zeta2 = np.zeros(number_case)

# loop over the geometrical samples
for l in range(0, number_case):
    # define geometry parameter for current sample
    angle = data[l, 0]
    length = data[l, 1]
    thickness = data[l, 2]
    radius = data[l, 3]
    number_of_elements = data[l, 4]

    # unit transverse force in order to determine alpha
    deflection_trans, rotation_trans, deflection_ax, rotation_ax = \
        analytics.beam_cylinder_analytical(E, nu, length, radius, thickness, force_value, 0, 0, 0)
    alpha[l] = deflection_trans
 
    # unit transverse moment in order to determine beta and gamma  
    deflection_trans, rotation_trans, deflection_ax, rotation_ax = \
        analytics.beam_cylinder_analytical(E, nu, length, radius, thickness, 0, moment_value, 0, 0)
    beta[l] = rotation_trans
    gamma[l] = deflection_trans
    
    # unit axial force in order to determine zeta1
    deflection_trans, rotation_trans, deflection_ax, rotation_ax = \
        analytics.beam_cylinder_analytical(E, nu, length, radius, thickness, 0, 0, force_value, 0)
    zeta1[l] = deflection_ax
    
    # unit axial moment in order to determine zeta2
    deflection_trans, rotation_trans, deflection_ax, rotation_ax = \
        analytics.beam_cylinder_analytical(E, nu, length, radius, thickness, 0, 0, 0, moment_value)
    zeta2[l] = rotation_ax
    
# open the original parameter file 
with open("parameter.csv") as csvin:
    readfile = csv.reader(csvin, delimiter=',')
    # open the new parameter file with output data 
    with open("parameter_output_analytical_cylindrical.csv", 'w') as csvout:
        writefile = csv.writer(csvout, delimiter=',', lineterminator='\n')
        # line index i
        i = 1
        # write output data as new columns next to the parameters
        for row in readfile:
            # write just ","  in the extended columns
            if i < header+1: 
                nothing = ''
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                writefile.writerow(row)
                i = i+1
            # write alpha, beta, gamma, zeta1 and zeta2 
            else:
                row.append(alpha[i-header-1])  # alpha
                row.append(beta[i-header-1])  # beta
                row.append(gamma[i-header-1])  # gamma
                row.append(zeta1[i-header-1])  # delta
                row.append(zeta2[i-header-1])  # epsilon
                writefile.writerow(row)
                i = i+1
