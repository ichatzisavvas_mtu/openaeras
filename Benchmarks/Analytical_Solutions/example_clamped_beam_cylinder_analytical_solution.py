"""
Application of the analytic beam solution to a cylindrical beam.

authors: Lukas Braun

"""

from openAERAS.Benchmarks.Analytical_Solutions import clamped_beam_cylinder_analytical_solution

# material
E = 210000
nu = 0.3
rho = 7.85E-9
# geometry
length = 100
radius = 10
thickness = 5
# loads
force_trans = 1
moment_trans = 0
force_ax = 0
moment_ax = 0

deflection_trans, rotation_trans, deflection_ax, rotation_ax = clamped_beam_cylinder_analytical_solution.beam_cylinder_analytical(E, nu, length, radius, thickness, force_trans, moment_trans, force_ax, moment_ax)