"""
This modules `Rotor Frequency Response` is used to performed a linear(-ized) 
frequency response analysis.
The excitation can be 
 - Unbalance
 - Rotor-bow (not yet implemented)

The bearings can be
 - Linear isotropic
 - Linear anisotropic
 - Linearized Rolling Element Bearings (speed and excentricity dependent)
 - Linearized Hydrodynamic Journal Bearings (speed and excentricity dependent)
 - Linearized Squeeze Film Dampers (speed and excentricity dependent)

"""


import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from openAERAS.Bearings.Radial_Bearings.linear_bearings import calcs_linear_bearing
from openAERAS.Structure_Modeling.beam_help_function import span_dof, lateral_dof



#%%
def calcs_frequency_response (M, C, G, K, rotor_casing_geometry, 
                                   ax_eq=1, fmax=20000, n_fpoints = 100, plot_node =[[1,1]],
                                   x_plot_lim=20000, log_lin="linear", n_p_e = 6):
    """
    The main function for the frequency response calculations


    Parameters
    ----------
    M : TYPE
        DESCRIPTION.
    C : TYPE
        DESCRIPTION.
    G : TYPE
        DESCRIPTION.
    K : TYPE
        DESCRIPTION.
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    ax_eq : TYPE, optional
        DESCRIPTION. The default is 1.
    fmax : TYPE, optional
        DESCRIPTION. The default is 20000.
    n_fpoints : TYPE, optional
        DESCRIPTION. The default is 100.
    plot_node : TYPE, optional
        DESCRIPTION. The default is [[1,1]].
    x_plot_lim : TYPE, optional
        DESCRIPTION. The default is 20000.
    log_lin : TYPE, optional
        DESCRIPTION. The default is "linear".
    n_p_e : TYPE, optional
        DESCRIPTION. The default is 6.

    Returns
    -------
    freq_resp : TYPE
        DESCRIPTION.

    """
    # rotor DoFs
    r_dofs, r_dof_span, r_dof_span_start = span_dof(rotor_casing_geometry, "rotor")
    
    # unbalance definition
    f_un = freq_resp_unb (rotor_casing_geometry, r_dofs, r_dof_span, r_dof_span_start)

    # Number of bearings
    n_bearings = len(rotor_casing_geometry[4])
    # Check if there is speed dependent bearing
    n_speed_dep = []
    for bearing in range(n_bearings):
        if rotor_casing_geometry[4][bearing][0] == "linear_bearing_vc":
            n_speed_dep.append(bearing)
    
    # Frequency response calculation for constant coeff. and speed dependent coeff.    
    if not n_speed_dep:
        C_b, K_b = calcs_linear_bearing(rotor_casing_geometry, C, K)
        omega = np.linspace(0, fmax*2*np.pi, n_fpoints)
        freq_resp = freq_resp_calcs (M, C_b, G, K_b, omega, rotor_casing_geometry, f_un, 
                                     r_dofs, r_dof_span, r_dof_span_start, n_p_e)
    else:
        # this is the place for the speed dependent coefficients
        pass
    
    
    # Frequency Response Plots 
    r_dofs, r_dof_span, r_dof_span_start = rotor_span_dof(rotor_casing_geometry, n_p_e)
    freq_resp_plots (freq_resp, fmax,  n_fpoints, plot_node, x_plot_lim, log_lin, r_dof_span, r_dof_span_start, n_p_e)

    
    return freq_resp

#%%
def freq_resp_unb (rotor_casing_geometry, r_dofs, r_dof_span, r_dof_span_start):
    """
    Creation of the unbalance force vector
    The unbalance vector f_un = mr [gmm]

    Parameters
    ----------
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    r_dofs : TYPE
        DESCRIPTION.
    r_dof_span : TYPE
        DESCRIPTION.
    r_dof_span_start : TYPE
        DESCRIPTION.

    Returns
    -------
    f_un : TYPE
        DESCRIPTION.

    """
    # check if unbalance is applied at both rotors
    hp_unb = []
    lp_unb = []
    for n_unb in range(len(rotor_casing_geometry[6][0])):
        if rotor_casing_geometry[6][0][n_unb][0] == "hp":
            hp_unb.append(n_unb)
        elif rotor_casing_geometry[6][0][n_unb][0] == "lp":
            lp_unb.append(n_unb)
    if hp_unb and lp_unb:
        print ("Unbalance both at hp and lp. Two calculations wiil be performed")
        
    
    # Unbalance Vector
    ndofs = r_dofs # number of DOF per Element
    f_un = np.zeros ((ndofs,1)) + 1j*np.zeros ((ndofs,1))
    for n_unb in range(len(rotor_casing_geometry[6][0])):
        unb_mag = rotor_casing_geometry[6][0][n_unb][3]
        unb_pha = rotor_casing_geometry[6][0][n_unb][4]
        #DoF where the unbalance is placed
        unb_dof = r_dof_span_start[rotor_casing_geometry[6][0][n_unb][1]-1] + 6*(rotor_casing_geometry[6][0][n_unb][2]-1)
        f_un[unb_dof] = unb_mag * np.exp(1j*unb_pha)
        f_un[unb_dof+1] = -1j*unb_mag * np.exp(1j*unb_pha)
    
    return f_un


#%%    
def freq_resp_calcs (M, C_b, G, K_b, omega, rotor_casing_geometry, f_un, r_dofs, r_dof_span, r_dof_span_start, n_p_e):
    """
    Calculation of the frequency response function

    Parameters
    ----------
    M : TYPE
        DESCRIPTION.
    C_b : TYPE
        DESCRIPTION.
    G : TYPE
        DESCRIPTION.
    K_b : TYPE
        DESCRIPTION.
    omega : TYPE
        DESCRIPTION.
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    f_un : TYPE
        DESCRIPTION.
    r_dof_span : TYPE
        DESCRIPTION.
    r_dof_span_start : TYPE
        DESCRIPTION.

    Returns
    -------
    freq_resp : TYPE
        DESCRIPTION.

    """
    ndof = len (M)
    if n_p_e == 4:
        Matrices= lateral_dof(M, C_b, K_b)
        M = Matrices[0]
        C_b = Matrices[1]
        K_b = Matrices[2]
    
    # Frequency response analyis    
    freq_resp = []

    for freq in range (len(omega)):
        f_un_speed_factor = np.zeros([ndof,1], dtype=complex) # unbalance vector
        G_speed_factor = np.zeros([ndof,ndof]) # gyroscopic matrix
        for span in range(len(rotor_casing_geometry[0])):
            speed_ratio = rotor_casing_geometry[0][span][0][4][1][0]
            G_speed_factor[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]] = \
                G[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]]  * speed_ratio
            f_un_speed_factor[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]] = \
                f_un[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]] * speed_ratio
        # Calculate Frequency Response
        # freq_resp.append (
        #     (np.linalg.inv(K_b - omega[freq]**2 * M + 1j * omega[freq]*(omega[freq]*G_speed_factor + C_b)) 
        #      .dot(f_un_speed_factor* omega[freq]**2)))

        # Matrix reduction
        if n_p_e == 4:
            Matrices = lateral_dof(G_speed_factor)
            G1 = Matrices[0]
            # Unbalance reduction  
            dof_rem = np.arange(2, ndof, 3)        
            f_un_speed_factor = np.delete(f_un_speed_factor, dof_rem)
        else:
            G1 = G_speed_factor
        
        freq_resp.append (sp.linalg.solve(K_b - omega[freq]**2 * M + 1j * omega[freq]*(omega[freq]*G1 + C_b), 
                                                                                       f_un_speed_factor* omega[freq]**2))
   
    
    return freq_resp



#%%    
def freq_resp_plots (freq_resp, fmax,  n_fpoints, plot_node, x_plot_lim, log_lin, r_dof_span, r_dof_span_start, n_p_e):
    """
    Plot tje frequency response function for the specified nodes

    Parameters
    ----------
    freq_resp : TYPE
        DESCRIPTION.
    fmax : TYPE
        DESCRIPTION.
    n_fpoints : TYPE
        DESCRIPTION.
    plot_node : TYPE
        DESCRIPTION.
    x_plot_lim : TYPE
        DESCRIPTION.
    log_lin : TYPE
        DESCRIPTION.
    r_dof_span : TYPE
        DESCRIPTION.
    r_dof_span_start : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    omega = np.linspace(0, fmax*2*np.pi, n_fpoints)
    
    fig, ax = plt.subplots()
    for dof in range (len(plot_node)):
        plot_dof = r_dof_span_start[plot_node[dof][0]-1] + n_p_e*(plot_node[dof][1]-1)
        freq_resp_plot = []
        freq_resp_plot_ = []
        for ii in range (n_fpoints):
            freq_resp_plot.append(freq_resp[ii][plot_dof])
            freq_resp_plot_.append(freq_resp[ii][plot_dof+1])
    
    
        #ax.plot(np.asarray(Omega) * 60 / 2 / np.pi, np.abs(freq_resp[plot_dof]))
        #ax.plot(omega * 60 / 2 / np.pi, np.abs(freq_resp_plot))
        # frp = [x**2 for x in freq_resp_plot]
        # frp_ = [x**2 for x in freq_resp_plot_]
        frp_p = [sum(np.abs(x)**2) for x in zip(freq_resp_plot,freq_resp_plot_)]
        ax.plot(omega * 60 / 2 / np.pi, np.sqrt(frp_p))
        ax.set_xlim(0,x_plot_lim)
        ax.set_xlabel('Rotational Speed [rpm]', fontsize=18)
        ax.set_ylabel('Displacement Amplitude [m]', fontsize=18)
        ax.set_yscale(log_lin)
        ax.grid(True)
        
    # fig, ax = plt.subplots()
    # for dof in range (len(plot_node)):
    #     plot_dof = r_dof_span_start[plot_node[dof][0]-1] + n_p_e*(plot_node[dof][1]-1)
    #     freq_resp_plot = []
    #     for ii in range (n_fpoints):
    #         freq_resp_plot.append(np.sqrt(freq_resp[ii][plot_dof]**2 + freq_resp[ii][plot_dof+1]**2 ))
    
    
    #     #ax.plot(np.asarray(Omega) * 60 / 2 / np.pi, np.abs(freq_resp[plot_dof]))
    #     ax.plot(omega * 60 / 2 / np.pi, np.abs(freq_resp_plot))
    #     ax.set_xlim(0,x_plot_lim)
    #     ax.set_xlabel('Rotational Speed [rpm]', fontsize=18)
    #     ax.set_ylabel('Displacement Amplitude [m]', fontsize=18)
    #     ax.set_yscale(log_lin)
    #     ax.grid(True)
        
    return
