"""
This module is used to create surrogate models
"""

from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
import numpy as np
import pandas as pd
from sklearn import model_selection
from sklearn.model_selection import train_test_split
# nonlinear regression
from sklearn.ensemble import RandomForestRegressor
# support vector machine
from sklearn import svm
from sklearn import metrics
from sklearn.tree import export_graphviz
import pydot
import pickle


#%%
def g_linearmodel_polyfeatures(filename, dofs, input_param, output_param):
    """
    Create surrogate model with polynomial features

    Returns
    -------
    model
    """
    
    df  = pd.read_csv(filename, header=0)
    
    model = Pipeline([("poly", PolynomialFeatures(degree = dofs )),
                      ("linear", LinearRegression(fit_intercept=False))])
    
    X = df.iloc[:,input_param]
    y = df.iloc[:,output_param]
    model.fit(X,y)
    model.named_steps["linear"].coef_    
    
    model.predict(X)
    model_score = model.score(X, y) 
    
     # Print out the score
    print("Score:", model_score)

   
    return model, model_score
    
    

#%%
def g_linearmodel_test_train(filename, input_param, output_param):
    """
    Create linear surrogate model.
    
    Samples are splitted for training and testing
    
    """
    df  = pd.read_csv(filename, skiprows = 1, header = None)
    
    df[input_param] = df[input_param] / df[input_param].max()
    
    X = df[input_param].values
    y = df[output_param].values
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=50)
    
    # Linear regression
    regressor = LinearRegression()  
    regressor.fit(X_train, y_train) #training the algorithm
    reg_score = regressor.score(X,y)
    # Prediction
    y_pred = regressor.predict(X_test)
    # Comparison actual and predicted
    comp = pd.DataFrame({'Actual': y_test.flatten(), 'Predicted': y_pred.flatten()})
    
    # Print out the score
    print("Score:", reg_score)
    
    
    return regressor, reg_score
    
    
#%%
def g_random_forest_regression(filename, input_param, output_param):
    """
    Create surrogate model with polynomial features

    Returns
    -------
    model
    """
    
    #filename = pathfile + filename_py
    df  = pd.read_csv(filename, skiprows = 1, header = None)
    
    #df[input_param] = df[input_param] / df[input_param].max()
    
    X = df[input_param].values
    y = df[output_param].values
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=50)
    
   
    # Nonlinear regression
    regressor = RandomForestRegressor(n_estimators=10, criterion='mse')
    regressor.fit(X_train, y_train)
    reg_score = regressor.score(X,y)
    reg_score_test = regressor.score(X_test,y_test)
    
    # Get numerical feature importances
    importances = list(regressor.feature_importances_)
    
    # feature list
    header_line = pd.read_csv(filename)
    feature_list = (header_line.columns)
    feature_list = feature_list[input_param]
    
    # List of tuples with variable and importance
    feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
    
    # Sort the feature importances by most important first
    feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
    
    # Print out the score and the importances 
    print("Score:", reg_score)
    print("Score test:", reg_score_test)
    [print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];
    
    # save the model to disk
    filename = 'random_forest_model.sav'
    pickle.dump(regressor, open(filename, 'wb'))
    
    return regressor, reg_score, reg_score_test
    
#    max_error_stif = np.max(y[:,0] - y_pred_all[:,0])
#    max_error_stif_prozent = np.max(np.abs(y_pred_all[:,0]-y[:,0])/y[:,0])
#    max_error_stif_value = y[np.argmax (y[:,0] - y_pred_all[:,0]),0]
#    max_error_stif_prozent_value = y[np.argmax(np.abs(y_pred_all[:,0]-y[:,0])/y[:,0]),0]
#    max_error_stress = np.max(y[:,1] - y_pred_all[:,1])
#    print('max_error_stif = ', max_error_stif,'max_error_stif_value = ', max_error_stif_value)
#    print('max_error_stif_prozent = ', max_error_stif_prozent,'max_error_stif_prozent_value = ', max_error_stif_prozent_value)
#    print('max_error_stress = ', max_error_stress)


#%%
def g_random_forest_regression_application(modelname, x_test):
    
    """
    Use a saved random forest model to predict the relaive difference of
    the flexibility coefficients between the beam and flexibility approach.

    authors: Lukas Braun
    
    Parameters
    ----------
    modelname : string
        name of the saved random forest model.
    x_test : vector with iloats
        input parameters for the predicition.
    
    Returns
    ----------
    prediction : vector with floats
        predicitions of the the relaive difference 
        of the flexibility coefficients.

    """
    
    # load the model from disk
    loaded_model = pickle.load(open(modelname, 'rb'))
    prediction = loaded_model.predict(x_test)
    
    return prediction

#%%
def g_random_forest_regression_tree_plot(filename, input_param, output_param, depth): 
    
    """
    Plots a chosen tree of the Random Forest Model.

    author: Lukas Braun
    
    based on: https://towardsdatascience.com/random-forest-in-python-24d0893d51c0
    
    Parameters
    ----------
    filename : string
        name of the filename with the data.
    input_param : vector with integers
        vector of the columns of the input file used as feature data.
    output_param: vector with integers
        vector of the columns of the input file used as target data.
    depth: integer
        maximum depth of the tree to be plotted.
    
    Returns
    ----------
    None.

    """
    
    # filename = pathfile + filename_py
    df = pd.read_csv(filename, skiprows=1, header=None)
    
    X = df[input_param].values
    y = df[output_param].values
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=50)
    
    # Nonlinear regression
    regressor = RandomForestRegressor(n_estimators=10, criterion='mse')
    regressor.fit(X_train, y_train)
    
    # feature list
    header_line = pd.read_csv(filename)
    feature_list = (header_line.columns)
    feature_list = feature_list[input_param]
    
    # Limit depth of tree
    rf_small = RandomForestRegressor(n_estimators=10, max_depth=depth)
    rf_small.fit(X_train, y_train)
    # Extract the small tree
    tree_small = rf_small.estimators_[1]
    # Save the tree as a png image
    export_graphviz(tree_small, out_file='Plots/small_tree.dot', feature_names=feature_list, rounded=True, precision=1)
    (graph, ) = pydot.graph_from_dot_file('Plots/small_tree.dot')
    graph.write_png('Plots/small_tree.png')
    graph.write_pdf('Plots/small_tree.pdf')
