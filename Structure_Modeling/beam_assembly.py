"""
This module builds a rotor model with 6DOF for each element node
In total, 12 DOF for each element are used.

The displacement and angles used are
q : [ux, uy, uz, rx, ry, rz], where

    ux: displacement in x direction 
    uy: displacement in y direction 
    uz: displacement in z direction 
    rx: angle over x direction 
    ry: angle over y direction 
    rz: angle over z direction 
    
    z is the axial direction
    x,y are the lateral directions

"""

import numpy as np
from openAERAS.Structure_Modeling.beam_help_function import span_dof, rotor_span_coupling
from openAERAS.Structure_Modeling.beam_6DOF import beam_cylindrical_elements, beam_conical_elements
from openAERAS.Structure_Modeling.element_3D import static_red_stiff_matrix, static_red_lumped_matrix


#%%
def beam_structure_assembly(rotor_casing_geometry, *exp_mat_sep):
    """
    Assemble the beam element matrix for the structure (rotor and/or casing).
    
    More complex structure that cannot be modelled using beam theory can be
    statically reduced to be include here during the assembly.

    Parameters
    ----------
    rotor_casing_geometry : array
        geometry of the rotor.

    Returns
    -------
    M: array with floats
        assembled global mass matrix.
    C: array with floats
        assembled global damping matrix.
    G: array with floats
        assembled global gyroscopic matrix.
    K: array with floats
        assembled global stiffness matrix.

    """    
    
    # Number of spans (rotor)
    nspans_rotor = len(rotor_casing_geometry[0])
    # Number of disks / blades / rigid bodies (rotor)
    ndisks_rotor = len(rotor_casing_geometry[1])
    # Number of spans (casing)
    nspans_casing = len(rotor_casing_geometry[2])
    # Number of disks / blades / rigid bodies (casing)
    ndisks_casing = len(rotor_casing_geometry[3])

    if nspans_rotor > 0:
        M_r, C_r, G_r, K_r = _beam_structure_mat_assembly(rotor_casing_geometry, "rotor", nspans_rotor, ndisks_rotor)
    if nspans_casing > 0:
        M_c, C_c, G_c, K_c = _beam_structure_mat_assembly(rotor_casing_geometry, "casing", nspans_casing, ndisks_casing)


    if exp_mat_sep[0] == True:
        if nspans_casing > 0 and nspans_rotor > 0:
            return M_r, C_r, G_r, K_r, M_c, C_c, G_c, K_c
        elif nspans_casing == 0 and nspans_rotor > 0:
            return M_r, C_r, G_r, K_r
        elif nspans_casing > 0 and nspans_rotor == 0:
            return M_c, C_c, G_c, K_c
       
    else:
        if nspans_casing > 0 and nspans_rotor > 0:
            M, C, G, K = structure_mat_concat (M_r, C_r, G_r, K_r, M_c, C_c, G_c, K_c)
        elif nspans_casing == 0 and nspans_rotor > 0:
            M = M_r
            C = C_r
            G = G_r
            K = K_r
        elif nspans_casing > 0 and nspans_rotor == 0:
            M = M_c
            C = C_c
            G = G_c
            K = K_c
            

    return M, C, G, K

#%%
def _beam_structure_mat_assembly(rotor_casing_geometry, span_type, nspans, ndisks):
    """
    Assemble the beam element matrix for the structure (rotor and/or casing).
    
    More complex structure that cannot be modelled using beam theory can be
    statically reduced to be include here during the assembly.

    last changed by Lukas Braun

    Parameters
    ----------
    rotor_casing_geometry : array
        Geometry of the rotor.
    span_type : string
        string with "rotor" or "casing".
    nspans : integer
        number of spans.
    ndisks : integer
        number of disks.

    Returns
    -------
    M: array with floats
        assembled global mass matrix.
    C: array with floats
        assembled global damping matrix.
    G: array with floats
        assembled global gyroscopic matrix.
    K: array with floats
        assembled global stiffness matrix.

    """
    # calculate the DoFs per span and the starting DoF for each span
    dofs, dof_span, dof_span_start = span_dof(rotor_casing_geometry, span_type)
    
    # initiate matrices          
    M_t = np.zeros([dofs, dofs])  # mass matrix
    K_t = np.zeros([dofs, dofs])  # stiffness matrix
    C_t = np.zeros([dofs, dofs])  # damping matrix
    G_t = np.zeros([dofs, dofs])  # gyroscopic matrix

    if span_type == "rotor":
        geometry = rotor_casing_geometry[0]
        geometry_disk_blade = rotor_casing_geometry[1]
    elif span_type == "casing":
        geometry = rotor_casing_geometry[2]
        geometry_disk_blade = rotor_casing_geometry[3]
    
    # Shaft Loop for each span
    for span in range(nspans):
        # Number of DoFs
        ndofs = len(geometry[span])
        # initiate matrice          
        M = np.zeros([6*ndofs, 6*ndofs])  # mass matrix
        K = np.zeros([6*ndofs, 6*ndofs])  # stiffness matrix
        G = np.zeros([6*ndofs, 6*ndofs])  # gyroscopic matrix
        # Assemble matrices for shaft   
        for i in range(ndofs-1):
            # Shaft_Type = rotor1_shaft[i,0]
            el_length = geometry[span][i+1][1]
            
            # Beam formulation
            if geometry[span][i+1][0] == ["Beam"]:
                # cylindrical beam formulation
                if geometry[span][i+1][3] == []:
                    el_outer_diameter = geometry[span][i+1][2][0]
                    el_inner_diameter = geometry[span][i+1][2][1]
                # conical beam formulation
                else: 
                    el_outer_diameter_left = geometry[span][i+1][2][0]
                    el_inner_diameter_left = geometry[span][i+1][2][1]
                    el_outer_diameter_right = geometry[span][i+1][3][0]
                    el_inner_diameter_right = geometry[span][i+1][3][1]
                rho = geometry[span][i+1][4]
                E = geometry[span][i+1][5]
                nu = geometry[span][i+1][6]
                # cylindrical beam formulation
                if geometry[span][i+1][3] == []:
                    Me, Ge, Ke = beam_cylindrical_elements(el_length, el_outer_diameter, el_inner_diameter, rho, E, nu)   
# start of modifications and extensions by Lukas Braun
                # conical beam formulation
                else:    
                    Me, Ge, Ke = beam_conical_elements(el_length, el_outer_diameter_left, el_outer_diameter_right,
                                                       el_inner_diameter_left, el_inner_diameter_right, rho, E, nu)

            # Flexibility element formulation
            if geometry[span][i+1][0] == ["Static_Red"]:
                alpha = geometry[span][i+1][7][0]
                beta = geometry[span][i+1][7][1]
                gamma = geometry[span][i+1][7][2]
                delta = geometry[span][i+1][7][3]
                epsilon = geometry[span][i+1][7][4]
                # beam formulation for mass and gyroscopic matrices
                if geometry[span][i+1][8] == []:
                    # cylindrical beam formulation
                    if geometry[span][i+1][3] == []:
                        el_outer_diameter = geometry[span][i+1][2][0]
                        el_inner_diameter = geometry[span][i+1][2][1]
                    # conical beam formulation 
                    else: 
                        el_outer_diameter_left = geometry[span][i+1][2][0]
                        el_inner_diameter_left = geometry[span][i+1][2][1]
                        el_outer_diameter_right = geometry[span][i+1][3][0]
                        el_inner_diameter_right = geometry[span][i+1][3][1]
                    rho = geometry[span][i+1][4]
                    E = geometry[span][i+1][5]
                    nu = geometry[span][i+1][6]
                    # cylindrical beam formulation for mass and gyroscopic matrix
                    if geometry[span][i+1][3] == []:
                        Me, Ge, Ke = beam_cylindrical_elements(el_length, el_outer_diameter, el_inner_diameter,
                                                               rho, E, nu)
                    # conical beam formulation for mass and gyroscopic matrix
                    else:    
                        Me, Ge, Ke = beam_conical_elements(el_length, el_outer_diameter_left, el_outer_diameter_right,
                                                           el_inner_diameter_left, el_inner_diameter_right, rho, E, nu)
                # lumped mass formulation for mass and gyroscopic matrices
                else:
                    mass = geometry[span][i+1][8][0]
                    Ip = geometry[span][i+1][8][1]
                    Id = geometry[span][i+1][8][2]
                    Me, Ge = static_red_lumped_matrix(mass, Ip, Id)
                Ke = static_red_stiff_matrix(el_length, alpha, beta, gamma, delta, epsilon)
# end of modifications and extensions by Lukas Braun
                
            # assemble matrices
            M[6*i:12+6*i, 6*i:12+6*i] = M[6*i:12+6*i, 6*i:12+6*i] + Me
            G[6*i:12+6*i, 6*i:12+6*i] = G[6*i:12+6*i, 6*i:12+6*i] + Ge
            K[6*i:12+6*i,  6*i:12+6*i] = K[6*i:12+6*i, 6*i:12+6*i] + Ke

        # shaft matrices
        ds_start = dof_span_start[span]
        ds_stop = dof_span_start[span] + dof_span[span]
        M_t[ds_start:ds_stop, ds_start:ds_stop] = M
        K_t[ds_start:ds_stop, ds_start:ds_stop] = K
        G_t[ds_start:ds_stop, ds_start:ds_stop] = G
        if geometry[0][0][2] == "stiffness_proportional":
            damping_factor = geometry[0][0][3]
            C_t[ds_start:ds_stop, ds_start:ds_stop] = damping_factor * K
        
    # Disk Loop 
    for disk in range(ndisks):
        if geometry_disk_blade[disk][0] == "blade" or geometry_disk_blade[disk][0] == "disk":
            rho = geometry_disk_blade[disk][4]
            thickness = geometry_disk_blade[disk][5]
            outer_diameter = geometry_disk_blade[disk][6]
            inner_diameter = geometry_disk_blade[disk][7]
            # disk_pos = geometry_disk_blade[disk][4]
            Mdisc = 0.25*rho*np.pi*thickness*(outer_diameter**2-inner_diameter**2)
            Id = 0.015625*rho*np.pi*thickness*(outer_diameter**4-inner_diameter**4) + Mdisc*(thickness**2)/12
            Ip = 0.03125*rho*np.pi*thickness*(outer_diameter**4-inner_diameter**4)
        else:
            Mdisc = geometry_disk_blade[disk][4]
            Id = geometry_disk_blade[disk][5]
            Ip = geometry_disk_blade[disk][6]
            
        disk_pos = dof_span_start[geometry_disk_blade[disk][2]-1] + 6*(geometry_disk_blade[disk][3]-1)
       
        # assembly of matrices
        M_t[disk_pos, disk_pos] = M_t[disk_pos, disk_pos] + Mdisc
        M_t[disk_pos + 1,   disk_pos + 1] = M_t[disk_pos + 1,   disk_pos + 1] + Mdisc
        M_t[disk_pos + 2,   disk_pos + 2] = M_t[disk_pos + 2,   disk_pos + 2] + Mdisc
        M_t[disk_pos + 3,   disk_pos + 3] = M_t[disk_pos + 3,   disk_pos + 3] + Id
        M_t[disk_pos + 4,   disk_pos + 4] = M_t[disk_pos + 4,   disk_pos + 4] + Id
        M_t[disk_pos + 5,   disk_pos + 5] = M_t[disk_pos + 5,   disk_pos + 5] + Ip
        G_t[disk_pos + 3,   disk_pos + 3+1] = G_t[disk_pos + 3,   disk_pos + 3 + 1] + Ip
        G_t[disk_pos + 3+1, disk_pos + 3] = G_t[disk_pos + 3+1, disk_pos + 3] - Ip

    if span_type == "casing":
         G_t = np.zeros([dofs, dofs])
    
    return M_t, C_t, G_t, K_t


#%%
def structure_mat_concat (M_r, C_r, G_r, K_r, M_c, C_c, G_c, K_c):
    """
    """
    len_Mr = len (M_r)
    len_Mc = len (M_c)

    Z2 = np.zeros((len_Mc, len_Mr))
    Z1 = np.zeros((len_Mr, len_Mc))

    M = np.asarray (np.bmat([[M_r, Z1], [Z2, M_c]]))
    C = np.asarray (np.bmat([[C_r, Z1], [Z2, C_c]]))
    G = np.asarray (np.bmat([[G_r, Z1], [Z2, G_c]]))
    K = np.asarray (np.bmat([[K_r, Z1], [Z2, K_c]]))

    return M, C, G, K