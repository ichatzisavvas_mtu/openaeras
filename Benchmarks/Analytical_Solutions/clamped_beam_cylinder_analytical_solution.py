import numpy as np


#%%
def beam_cylinder_analytical(E, nu, length, radius, thickness, force_trans, moment_trans, force_ax, moment_ax):

    """
    Analytical solution for the displacements and rotations of a clamped cylindrical Timoshenko beam.

    source: Charalambides and Fang 2016: "The Mechanics of a Cantilever Beam with an Embedded Horizontal Crack Subjected
    to an End Transverse Force, Part A: Modelling"

    author: Lukas Braun

    Parameters
    ----------
    E : float
        Young's modulus.
    nu : float
        Poisson's ratio.
    length : float
        length of the beam element.
    radius : float
        inner radius of the beam element.
    thickness : float
        thickness of the beam element.
    force_trans, moment_trans, force_ax, moment_ax : float
        force and moment values in transverse and axial direction, respectively.
    
    Returns
    ----------
    deflection_trans, rotation_trans, deflection_ax, rotation_ax : float
        displacements and rotations in transverse and axial direction, respectively.
    
    """

    r_i = radius
    r_a = radius + thickness
    A = np.pi*(r_a**2-r_i**2)
    I = np.pi/4*(r_a**4-r_i**4)
    m = r_i/r_a
    G = E/(2*(1+nu))
    # shear coefficient
    # Hutchinson
    # kappa = 6*(1+nu)**2*(1+m**2)**2 / ((7+12*nu+4*nu**2)*(1+m**2)**2 + 4*(5+6*nu+2*nu**2)*m**2)
    # Cowper
    kappa = 6 * (1+m**2)**2 * (1+nu) / ((1+m**2)**2*(7+6*nu)+m**2*(20+12*nu))

    deflection_trans = force_trans*length/(kappa*A*G) + force_trans*length**3/(3*E*I) + moment_trans*length**2/(2*E*I)
    rotation_trans = force_trans*length**2/(2*E*I)+moment_trans*length/(E*I)
    deflection_ax = force_ax*length/(E*A)
    rotation_ax = moment_ax*length/(G*2*I)

    return deflection_trans, rotation_trans, deflection_ax, rotation_ax
