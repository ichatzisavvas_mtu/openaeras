# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np

#%%
def se_max_speed(xx_df, Fmax, *args):
    """
    
    
    Input
        xx_df
        Fmax
        *args
    
    Output
        gmodes_u_fr
        
    """
    # modes only until certain frequency
    gmodes_u_fr = _se_max_values (xx_df, Fmax)

    if len(args) > 0:
        ncs = args [0]
        modes_max = _se_keep(gmodes_u_fr, ncs)
        if os.path.isfile(args[1]):
            print ('results already exported')
        else:
            #_se_export_csv(modes_max, ncs, args[1], args[2])
            _se_param_study_df(modes_max, ncs, args[1], args[2])
        return modes_max
            
    return gmodes_u_fr

#%%
def _se_max_values (xx_df, Fmax):
    """
    Create a new dataframe with a maximum speed
    """
    # modes only until certain frequency
    gmodes_u_fr = [] # modes strain energy until a frequency limit
    for ii in range(0,len(xx_df.columns),2):
        if xx_df[ii].values[-1] > (Fmax):
            max_v = xx_df[xx_df[ii].gt(Fmax)].index[0]
            gmodes_u_fr.append(xx_df.iloc[:max_v,ii+1]) # modes until a certain frequency
        else:
            gmodes_u_fr.append(xx_df.iloc[:,ii+1]) # modes until a certain frequency 
    
    return gmodes_u_fr

#%%
def _se_keep(gmodes_u_fr, ncs):

    # collect the frequencies in a vector    
    modes_max = [] # strain energy of modes - Sorted
    for ii in range(len(gmodes_u_fr)):
        modes_max.append(gmodes_u_fr[ii].nlargest(ncs))
    # catch error: if the modes_max has only one strain energy
    for ii in range(len(gmodes_u_fr)):
        if (len(modes_max[ii])==1):
            modes_max[ii][1] = 0
            print ('Corrected Simulation' + str(ii))
            
    return modes_max


#%%
def _se_export_csv(modes_max, ncs, filename, var_par):
    """
    
    
    
    """
    title_file = ["Simulation #"]
    for ii in range (len(var_par)):
        title_file.append("Parameter" + str(ii+1))
        
    for ii in range (ncs):
        title_file.append("Strain Energy" + str(ii+1))
    title_file_str = str(title_file).strip("[]")
    
    
    with open(filename,'w',encoding='ISO-8859-1') as f:
        f.write(title_file_str +'\n')
        
        for i in range(len(var_par[0])):
            input_para = []
            output_para = []
            for jj in range(len(var_par)):
                input_para.append(str(var_par[jj][i]))
            for kk in range(ncs):
                output_para.append(str(modes_max[i].iloc[kk]))
            input_para_str = str(input_para).strip("[]")
            output_para_str = str(output_para).strip("[]")
            f.write((f'Sim{i+1},') + input_para_str + "," + output_para_str +'\n')
        
        
    return None


#%%
def _se_param_study_df(modes_max, ncs, filename, var_par):
    """
    
    """
    
    ps_df = pd.DataFrame()
    
    ps_df["Simulation #"] = range(1,len(var_par[0])+1)
    
    for ii in range (len(var_par)):
        ps_df["Parameter" + str(ii+1)] = var_par[ii]
        
    modes_max_vec = np.zeros([len(var_par[0]),ncs])
    for ii in range (len(var_par[0])):
        for kk in range (ncs):
            modes_max_vec[ii,kk] = modes_max[ii].iloc[kk]
        
    for ii in range (ncs):
        ps_df["Strain Energy" + str(ii+1)] = modes_max_vec[:,ii]
        
    ps_df.to_csv(filename, index = False)
    
    return None

#%%
def se_max__val_speed(xx_df, Fmax, filename, var_par, lhs_in_out):
    """
    Calculate the max value and the respective rot. speed for the parameters to be studied
    
    Input

    
    Output
        gmodes_u_fr
        
    """
    #xx_df = _se_max_values (xx_df, Fmax)
    # create two list, one with the max. amplitudes and one with the respective rot. speeds
    var_max_amp = [] # list of all the max values 
    var_max_speed = [] # list of all the rot. speed
    for ii in range (1,len(xx_df.columns),2):
        xx_df[ii] = xx_df[ii][xx_df[ii-1] < Fmax]
        max_tmp = xx_df[ii].max()
        var_max_amp.append(max_tmp)
        max_tmp_speed = xx_df[ii-1].loc[xx_df[ii].idxmax()]
        var_max_speed.append(max_tmp_speed)
    

    # create dataframe for the complete input / output parameter
    ps_df = pd.DataFrame()
    
    ps_df["Simulation #"] = range(1,len(var_par[0])+1) # create the number of the simulation
    
    for ii in range (len(var_par)):
        var_par2 = [float(i) for i in var_par[ii]]
        ps_df[lhs_in_out[0][ii]] = var_par2 # collect all the input parameters   
        
    if len(lhs_in_out[1]) == 2:
        ps_df[lhs_in_out[1][0]] = var_max_amp # collect all the input parameters
        ps_df[lhs_in_out[1][1]] = var_max_speed # collect all the input parameters
    else:
        for ii in range (int(len(lhs_in_out[1]))):
            ps_df[lhs_in_out[1][2*ii]] = var_max_amp[ii] # collect all the input parameters
            ps_df[lhs_in_out[1][2*ii+1]] = var_max_speed[ii] # collect all the input parameters
        
    # check if file exists and then export it
    if os.path.isfile(filename):
        print ('results already exported')
    else:
        ps_df.to_csv(filename, index = False)

            
    return ps_df
   
    