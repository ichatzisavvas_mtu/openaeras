"""
This file reads the calculated flexibility coefficients alpha, beta, gamma, delta and epsilon of each sample
and writes them back to the parameter.csv file by creating the new file "parameter_3d_output.csv".

author: Lukas Braun
"""

import numpy as np
import os
import csv

# number header lines
header = 5

# open the original parameter file 
with open("parameter.csv") as csvin:
    readfile = csv.reader(csvin, delimiter=',')
    # open the new parameter file with output data 
    with open("parameter_3d_output.csv", 'w') as csvout:
        writefile = csv.writer(csvout, delimiter=',', lineterminator='\n')
        # line index i
        i = 1
        # write output data as new columns next to the parameters
        for row in readfile:
            # write just ","  in the extended columns
            if i < header+1: 
                nothing = ''
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                row.append(nothing)
                writefile.writerow(row)
                i = i+1
            # write alpha, beta, gamma, delta and epsilon
            else:
                # open .txt file with the necessary data for each sample
                os.chdir("element_%s" % str(i-header))
                parameters = np.loadtxt("element_" + str(i-header) + ".txt", usecols=(1, 2, 3))
                os.chdir("..")    
                row.append(parameters[0, 0])  # alpha
                row.append(parameters[2, 1])  # beta
                row.append(parameters[1, 1])  # gamma
                row.append(parameters[3, 2])  # delta
                row.append(parameters[4, 2])  # epsilon
                writefile.writerow(row)
                i = i+ 1