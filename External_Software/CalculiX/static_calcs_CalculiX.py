import numpy as np
import os
import shutil
import io

    
#%%
def static_calcs(filename, software):      
    """
    Perform the static analysis in CalculiX.

    author: Lukas Braun
    
    Parameters
    ----------
    filename : string
        name of the input file defined in the beginning of the routine.
    software: integer
        if CalculiX is used: choose which command is necessary in the terminal: =1 with ccxlsf, =2 with ccx.
    
    Returns
    ----------
    None.

    """   

    # import data
    header = 5
    data = np.genfromtxt("%s.txt" % filename, skip_header=header, delimiter=",")
    E = data[0]
    nu = data[1]
    T = data[2]
    rho = data[3]
    number_points = int((len(data)-9)/3)    
    
    f = open("%s.inp" % filename, "a")
               
    # write CalculiX commands
    f.write("******CalculiX analysis******\n") 
    f.write("************\n")
    f.write("*RIGID BODY,NSET=Force_moment,REF NODE=  %s,ROT NODE=  %s\n" % (number_points+2, number_points+3))
    calc_script = calculiX_static_analysis_str()
    text = io.StringIO(calc_script)
    linelist = text.readlines()
    for line in linelist:
        f.write(line)
        # include the material data
        if line.startswith("*ELASTIC"):
            f.write("%s,%s,%s\n" % (E, nu, T))
        if line.startswith("*DENSITY"):
            f.write("%s\n" % rho)
        if line.startswith("*INITIAL CONDITIONS"):    
            f.write("Volume, %s\n" % T)
    f.close()   
        
    # create new folder and move CalculiX .inp file there
    if os.path.exists("%s" % filename):
        shutil.rmtree("%s" % filename)
    os.makedirs("%s" % filename)
    shutil.move("%s.inp" % filename, "%s/%s.inp" % (filename, filename))
    
    # perform the CalculiX linear static analysis
    os.chdir("%s" % filename)
    if software == 1:
        os.system("yes | ccxlsf -j %s.inp" % filename)
    if software == 2:
        os.system("ccx %s" % filename)
    os.chdir("..")
            
    return None


#%%
def calculiX_static_analysis_str():
    
    """    
    String with the CalculiX commands.

    Parameters
    ----------
    None.
    
    Returns
    ----------
    None.

    """

    calc_script = """**
*MATERIAL,NAME=SQC_MAT
*ELASTIC,TYPE=ISO
*DENSITY
*SOLID SECTION, ELSET=Volume, MATERIAL=SQC_MAT
**
*INITIAL CONDITIONS, TYPE=TEMPERATURE
**
************
*STEP
*STATIC
*BOUNDARY, OP=NEW
Fixation,1,6,0.0
*CLOAD, OP=NEW
REFNOD,       1,    1.0
*NODE FILE
U
*EL FILE
S,E
*NODE PRINT, NSET = OUTPUT
U,
*END STEP
************
*STEP
*STATIC
*BOUNDARY, OP=NEW
Fixation,1,6,0.0
*CLOAD, OP=NEW
ROTNOD,       2,    1.0
*NODE FILE
U
*EL FILE
S,E
*NODE PRINT, NSET = OUTPUT
U
*END STEP
************
*STEP
*STATIC
*BOUNDARY, OP=NEW
Fixation,1,6,0.0
*CLOAD, OP=NEW
REFNOD,       3,    1.0
*NODE FILE
U
*EL FILE
S,E
*NODE PRINT, NSET = OUTPUT
U
*END STEP
************
*STEP
*STATIC
*BOUNDARY, OP=NEW
Fixation,1,6,0.0
*CLOAD, OP=NEW
ROTNOD,       3,    1.0
*NODE FILE
U
*EL FILE
S,E
*NODE PRINT, NSET = OUTPUT
U,
*END STEP
************
"""
                    
    return calc_script
