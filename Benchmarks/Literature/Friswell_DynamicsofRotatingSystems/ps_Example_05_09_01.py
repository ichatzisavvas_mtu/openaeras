# -*- coding: utf-8 -*-
"""
Python Script for the Example 060601 / 5.9.1 pp.206.

Use example for checking the analysis methods
"""


import numpy as np
from openAERAS.Analysis import eigenfrequency_analysis, frequency_response, transient_analysis
from openAERAS.Structure_Modeling import beam_assembly
#from openAERAS.Casing_Modeling import casing_finite_beam_6DOF
from openAERAS.Graphical_Interface import non_graphical_interface


#%%
"""
- Rotating Part Definition
    - Shaft Definition
    - Blade / Disk / Rigid_body Definition
- Casing Definition
    - Flexible Body Definition
    - Rigid_body Definition
- Bearing Definition
- Span Coupling Definition
"""

##############################################################################
##############################################################################
##############################################################################
# Rotating Part Definition
E = 2.11e11 # Young Modulus [N/m^2]
nu = 0.29926 # Poisson ratio [-]
rho = 7810 # Density [kg/m^3]
damping_factor = 0    # damping in shaft
damping_type = 'stiffness_proportional' # type of damping
#damping_factor =0.05 * 2 / (11.372*2*np.pi)
shaft_od_1 = 0.05 # Shaft Outer Diameter [m]
shaft_id_1 = 0.0  # Shaft Inner Diameter [m]
rotor_start = {"hp_1" : 0.0} # starting point of rotors [mm]
# rotor speed ratio
#rotor_speed_ratio = {"lp_rot_speed_ratio" : 1, "hp_rot_speed_ratio" : 1.5} 
rotor_speed_ratio = [["hp", [1],'positive_z']]

##############################################################################
# Rotor & Shaft Definition
rotor_shaft = (
[
 [[1, rotor_start["hp_1"], damping_type, damping_factor, rotor_speed_ratio[0]],      
       [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu]]
]
)

    
##############################################################################
# Disk Definition
#rho = 3842.63/(0.25*np.pi*disk_thick*(disk_od**2-shaft_od**2))

# rotor_blade_disk = (
# [
#  ["blade",0,4, rho, disk_thick, disk_od, shaft_od],
#  ['disk', 1,4, rho, disk_thick, disk_od, shaft_od],
#  ['rigid_body', 1,4, mass, Id, Ip]
# ]
# )


rotor_blade_disk = (
[
 ["disk", "hp", 1, 3, 7810, 0.07, 0.28, 0.05],
 ["disk", "hp", 1, 5, 7810, 0.07, 0.35, 0.05]
]
)


##############################################################################
##############################################################################
##############################################################################
# Casing Definition
# casing_start = {"casing_1" : 0, "casing_2" : 0.4} # starting point of casing [mm]

casing_flex = ([])
# casing_flex = (
# [
#  [[1, casing_start["casing_1"], damping_type, damping_factor],      
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu]],
#  [[2, casing_start["casing_2"], damping_type, damping_factor],      
#        [0.2, 0.54, 0.5, rho, E, nu],
#        [0.2, 0.54, 0.5, rho, E, nu]]
# ]
# )

casing_rigid = ([])
# casing_rigid = (
# [
#  [1, 3, 0.5,100,200] 
# ]
# )



##############################################################################
##############################################################################
##############################################################################
# Bearing Definition
# 1 Parameter type
# 2 Parameter node
# 3 Parameter: list

# rotor_bearing = (
# [
#  ["linear_bearing_cc",rotor_number,rotor_dof, 1e16,1e16,1e16,0,0,0,100,100,0,0,0,0],
# ]
# )
    
rotor_casing_bearing = (
[
 ["linear_bearing_cc",["rotor",1,1],[], [1e6,1e6,1e10,0,0,1e10],[0,0,0,0,0,0]],
 ["linear_bearing_cc",["rotor",1,7],[], [1e6,1e6,1e10,0,0,1e10],[0,0,0,0,0,0]]
]
)

##############################################################################
##############################################################################
##############################################################################
# Span Coupling Definition
span_coupling = ([])
# span_coupling = (
# [
#  ["rotor",  [1,6], [2,1], [1e16,1e16,1e16,1e16,1e16,1e16]],
#  ["casing", [1,3], [2,1], [1e16,1e16,1e16,1e16,1e16,1e16]],
#  ["casing", [1,5], [2,3], [1e16,1e16,1e16,1e16,1e16,1e16]]
# ]
# )    
    
##############################################################################
##############################################################################
##############################################################################
# Unbalance
rotor_unbalance = (
[

]
)   


##############################################################################
##############################################################################
##############################################################################
# Collecting rotor/bearing/casing/unbalance
rotor_casing_geometry = [rotor_shaft, rotor_blade_disk, casing_flex, casing_rigid, \
                         rotor_casing_bearing, span_coupling, [rotor_unbalance]]


##############################################################################
##############################################################################
##############################################################################
# For eigenfrequency analysis
ea_ax_eq = 1 # axis equal   
ea_fmax = 150 # [Hz] max eigenfrequncy for y limit
ea_omega_max = 4000*2*np.pi/60 # [rad/s] max eigenfrequncy for the calculation of the diagram
ea_n_camp = 10 # number of points for the Campbell diagram
ea_n_eigs = 30 # number of eigenfrequencies to plot
ea_modes_plot = [1, 2] # modeshapes to plot
ea_modes_plot_speed = [1] # the speed of modeshapes to plot
ea_mode_tracking = False # True of False actication of mode tracking
eig_animate = True # Animate mode shape
ea_n_p_e = 4 # 6 or 4 (axial and torsional removed)
    
    
    
##############################################################################
##############################################################################
##############################################################################
# For frequency response analysis
fr_ax_eq = 1 # axis equal   
fr_fmax = 15000/60  # [Hz] max frequency for the frequency response calculation
fr_n_fpoints = 1501 # number of frequency points
fr_plot_node = [[1,2], [1,7], [2,2], [2,4]] # node to be plot
fr_x_plot_lim = 15000 # [rpm] Plot limit 
fr_log_lin = "log" # "linear", or "log" y scale
fr_n_p_e = 6 # 6 or 4 (axial and torsional removed)

##############################################################################
##############################################################################
##############################################################################
# For transient analysis
tr_ax_eq = 1 # axis equal   
tmax= 15  # [s] simulation time
#tr_n_fpoints = 1501 # number of frequency points
#tr_plot_node = [[1,2], [1,7], [2,2], [2,4]] # node to be plot
#tr_x_plot_lim = 15000 # [rpm] Plot limit 
#tr_log_lin = "log" # "linear", or "log" y scale
tr_n_p_e = 6 # 6 or 4 (axial and torsional removed)
omega_trans = [[["lp", "negative_z"],[0,0],[15,15000/60*2*np.pi]], \
               [["hp", "positive_z"],[0,0],[15,22500/60*2*np.pi]]]

    
##############################################################################
##############################################################################
##############################################################################
# For quasistatic analysis
qs_ax_eq = 1 # axis equal   
qs_tmax= 15  # [s] simulation time
g_loads = ["true", "x", "y", "z"]
gyro_loads = ["true", "yaw", "pitch", "roll"]
gyro_loads_acc = ["true", "yaw_acc", "pitch_acc", "roll_acc"]
#tr_n_fpoints = 1501 # number of frequency points
#tr_plot_node = [[1,2], [1,7], [2,2], [2,4]] # node to be plot
#tr_x_plot_lim = 15000 # [rpm] Plot limit 
#tr_log_lin = "log" # "linear", or "log" y scale
qs_n_p_e = 6 # 6 or 4 (axial and torsional removed)

    
    

##############################################################################
##############################################################################
##############################################################################
# Define Analysis Type
graphic_plot = True
eig_analysis = True
freq_analysis = False
tran_analysis = False
quasi_static_analysis = False

#%%
# Rotor/Casing Visualisation
if graphic_plot:
    non_graphical_interface.vis_rotor_casing (rotor_casing_geometry, ax_eq=1)

#%% 
# Model Creation
M_r, C_r, G_r, K_r = beam_assembly.beam_structure_assembly(rotor_casing_geometry, False)
if rotor_casing_geometry[2]: 
    M_c, C_c, K_c = beam_assembly.beam_structure_assembly(rotor_casing_geometry, False)
else:
    M = M_r
    C = C_r
    G = G_r
    K = K_r

#%%
# Eigenfrequency Analyis

if eig_analysis:
    g_vector_s_i, g_value_, AA, g_values = eigenfrequency_analysis.calcs_eigenfrequency_analysis \
                                            (M, C, G, K, rotor_casing_geometry, 
                                             ea_ax_eq, ea_omega_max, ea_fmax, ea_n_camp, ea_n_eigs, 
                                             ea_modes_plot, ea_modes_plot_speed, ea_mode_tracking, eig_animate, ea_n_p_e)
                                            
#%%
# Frequency Response Analyis

if freq_analysis:
    freq_resp = frequency_response.calcs_frequency_response (M, C, G, K, rotor_casing_geometry, 
                                                 fr_ax_eq, fr_fmax, fr_n_fpoints, 
                                                 fr_plot_node, fr_x_plot_lim, fr_log_lin, n_p_e)
    
    
#%%
# Transient Analyis

if tran_analysis:
    transient_response = transient_analysis.calcs_transient_analysis (M, C, G, K, rotor_casing_geometry, 
                                                 tr_ax_eq, tr_tmax, omega_trans, n_p_e)
    
#%%
if quasi_static_analysis:
    quasis_static_response = quasi_static_analysis.calcs_quasi_static_analysis (M, C, G, K, rotor_casing_geometry, 
                                                 tr_ax_eq, tr_tmax, omega_trans, n_p_e)
    




