import pyDOE as pdoe
from scipy.stats.distributions import norm
import matplotlib.pyplot as plt

def g_lhs(nvar, n_samples, var_par, g_dist, plot_param):
    """
    Perform the latin hypercube samplint
    
    Input
        nvar: Number of independent parameters to be varied
        n_samples: Number of samples for the paramter study
        var_par: List with the min/max values of all parameters
        g_dist: type of distribution. Support "uniform" 
        plot_param : type of plot for the input data. Support "hist"
    
    Output
        tb_params
    """
    
    res_lhs = pdoe.lhs(nvar, n_samples)
    #lhd = norm(loc=0, scale=1).ppf(res_lhs)
    
    # geometry parameters to be tested
    if g_dist == "uniform":
        tb_params = []
        for ii in range(0,len(var_par),2):
            tb_params.append(var_par[ii] + (var_par[ii+1]-var_par[ii])*res_lhs[:,int(ii/2)])
    
    
    #lhd = norm(loc=0, scale=1).ppf(res_lhs)
    
    # plot histogram to check distribution
    if plot_param == "hist":
        for ii in range(len(tb_params)):    
            fig, ax = plt.subplots()
            ax.hist(tb_params[ii])

    return tb_params

#%%
def g_lhs_split (lhs_vector, n_simulations):
    """
    Split the lhs_vector in a number of sub simulations
    
    Input: lhs_vector, n_simulations
    Output: lhs_vec_exp_f
    """
    lhs_vector_splitted = []
    for ii in range(len(lhs_vector)):
        par = lhs_vector[ii]
        par_split = []
        param_a = par[0] 
        param_b = par[0] + (par[1]-par[0])/n_simulations
        for ii in range(n_simulations):
            par_split.append([param_a, param_b])
            param_a =  param_a + (par[1]-par[0])/n_simulations
            param_b = (par[1]-par[0])/n_simulations + param_b
        lhs_vector_splitted.append(par_split)
    
    lhs_vec_exp_f = []   
    for ii in range(n_simulations):
        lhs_vec_exp = []
        for jj in range(len(lhs_vector)):
            lhs_vec_exp.append(lhs_vector_splitted[jj][ii])
        lhs_vec_exp_f.append(lhs_vec_exp)
        
    return lhs_vec_exp_f
