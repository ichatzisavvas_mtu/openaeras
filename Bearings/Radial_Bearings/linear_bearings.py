"""
This modules `Linear Bearing` is used to implement the stiffness and the damping
matrices of the bearings

- Linear Bearing with constant Coefficients
- Linear Bearing with speed-dependent Coefficients
- Linear Bearing between shafts (intershaft bearings)
"""


import numpy as np
from openAERAS.Structure_Modeling.beam_help_function import span_dof

#%% Main function for the calculations
def calcs_linear_bearing(rotor_casing_geometry, C, K, qs_n_p_e=6):
    """
    The main function for the linear bearing calculations

    Parameters
    ----------
    rotor_casing_geometry : TYPE
        DESCRIPTION.

    Returns
    -------
    C_b: damping matrix
    K_b: stiffness matrix

    """
    
    # Number of bearings
    n_bearings = len(rotor_casing_geometry[4])
    
    for bearing in range(n_bearings):
        if rotor_casing_geometry[4][bearing][0] == "linear_bearing_cc": 
            C_b, K_b = linear_bearings_constant_coef (rotor_casing_geometry, C, K, bearing, qs_n_p_e)
            C = C_b
            K = K_b
        elif rotor_casing_geometry[4][bearing][0] == "linear_bearing_vc":
            pass
    
    return C_b, K_b
    
    


#%% Linear Bearing Model with constant coeffcients
def linear_bearings_constant_coef (rotor_casing_geometry, C, K, bearing, qs_n_p_e=6):
    """
    

    Parameters
    ----------
    rotor_bearing : TYPE
        DESCRIPTION.
    C : TYPE
        DESCRIPTION.
    K : TYPE
        DESCRIPTION.
    bearing : TYPE
        DESCRIPTION.

    Returns
    -------
    C_b : TYPE
        DESCRIPTION.
    K_b : TYPE
        DESCRIPTION.

    """

   
    # rotor DoFs
    r_dofs, r_dof_span, r_dof_span_start = span_dof(rotor_casing_geometry, "rotor")
    # casing DoFs
    c_dofs, c_dof_span, c_dof_span_start = span_dof(rotor_casing_geometry, "casing")
    
    c_dof_span_start_all = [c_dof_span_start[i] + r_dof_span_start[-1] +  r_dof_span[-1]\
                            for i in range(len(c_dof_span_start))]
    
    
    span_1 = rotor_casing_geometry[4][bearing][1][1]
    if rotor_casing_geometry[4][bearing][2]:
        span_2 = rotor_casing_geometry[4][bearing][2][1]
        
    # identify the DoFs to be coupled
    dof_tmp_1 = rotor_casing_geometry[4][bearing][1][2]
    if rotor_casing_geometry[4][bearing][2]:
        dof_tmp_2 = rotor_casing_geometry[4][bearing][2][2]
    # identify which is the starting DoF of span: rotor or stator
    if rotor_casing_geometry[4][bearing][1][0] == "rotor":
        dof_span_start_1 = r_dof_span_start
    else:
        dof_span_start_1 = c_dof_span_start_all
    if rotor_casing_geometry[4][bearing][2]:
        if rotor_casing_geometry[4][bearing][2][0] == "rotor":
            dof_span_start_2 = r_dof_span_start
        else:
            dof_span_start_2 = c_dof_span_start_all
        
    dof_1 = dof_span_start_1[span_1-1] + 6 * (dof_tmp_1 - 1)
    if rotor_casing_geometry[4][bearing][2]:
        dof_2 = dof_span_start_2[span_2-1] + 6 * (dof_tmp_2 - 1)
    
    # DoFs for matrix
    dof_mat_1 = [dof_1, dof_1+1, dof_1+2, dof_1+3, dof_1+4, dof_1+5]
    if rotor_casing_geometry[4][bearing][2]:
        dof_mat_2 = [dof_2, dof_2+1, dof_2+2, dof_2+3, dof_2+4, dof_2+5]
    # Stiffness matrix
    K[dof_mat_1, dof_mat_1] = K[dof_mat_1, dof_mat_1] + rotor_casing_geometry[4][bearing][3]
    if rotor_casing_geometry[4][bearing][2]:
        K[dof_mat_2, dof_mat_2] = K[dof_mat_2, dof_mat_2] + rotor_casing_geometry[4][bearing][3]
        K[dof_mat_1, dof_mat_2] = K[dof_mat_1, dof_mat_2] - rotor_casing_geometry[4][bearing][3]
        K[dof_mat_2, dof_mat_1] = K[dof_mat_2, dof_mat_1] - rotor_casing_geometry[4][bearing][3]
    # Damping matrix
    C[dof_mat_1, dof_mat_1] = C[dof_mat_1, dof_mat_1] + rotor_casing_geometry[4][bearing][4]
    if rotor_casing_geometry[4][bearing][2]:
        C[dof_mat_2, dof_mat_2] = C[dof_mat_2, dof_mat_2] + rotor_casing_geometry[4][bearing][4]
        C[dof_mat_1, dof_mat_2] = C[dof_mat_1, dof_mat_2] - rotor_casing_geometry[4][bearing][4]
        C[dof_mat_2, dof_mat_1] = C[dof_mat_2, dof_mat_1] - rotor_casing_geometry[4][bearing][4]
    
    return C, K
