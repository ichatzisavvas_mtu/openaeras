"""
This example shows how the stiffness matrix / flexibility coefficients of a 3D element can be obtained with the
routine implemented in openAERAS, with interfaces to the software tools CalculiX, Gmsh and Nastran.

FIRST PART

author: Lukas Braun

"""

from openAERAS.Structure_Modeling import element_3D
from openAERAS.External_Software.Gmsh import flexibility_rot_symm_3D_geo
from openAERAS.External_Software.CalculiX import static_calcs_CalculiX, flexibility_coefficients


# The geometry and material data of 2D linear (conical) elements are exported.
# The names of these files are entered in "filenames_input"
# IMPORTANT: Elements with an inner radius of 0.0 mm are not possible, but for example 0.001 mm
filenames_input = ["3D_example_input_1", "3D_example_input_2", "3D_example_input_3", "3D_example_input_4", "3D_example_input_5"]
# Define the name of the input file
filename = "3D_example"

# Define the mesh by three mesh parameters
# Right now, there exist no rules/recommendation concerning the mesh parameters. It is recommended to
# check the mesh quaility with the Jacobian determinant
# number of horizontal grid points (along the axial axis) - for each imported linear element
number_horizontal = 10
# number of vertical grid points (perpendicular to the axial axis) - for each imported linear element
number_vertical = 12
# number of peripheral grid points - for a quarter section
number_rotational = 10
# Definition of the clamping boundary condition: which nodes of one end are fixed for the static analysis?
# = 0: whole surface, = 1: only inner circumferential line, = 2: inner and outer circumferential line)
fix_stat = 0
# Define the solver for the static analysis
# = 0: CalculiX,= 1: Nastran, = 2: Abaqus
software = 0

# INPUT FILE ASSEMBLY FUNCTION
# this function detects vertical steps between the exported linear elements and generates one input file "FILENNAME.txt"
# that includes the geometry and material of the flexibility element that is to be generated and
# the mesh parameters as well as the information about the boundary condition and the solver to be used
flexibility_rot_symm_3D_geo.assemble_input_files(filenames_input, number_horizontal, number_vertical, number_rotational,
                                                 fix_stat, software, filename)

# MESH GENERATION IN GMSH
# the generated input file is now used to first define the 3D geometry and then create the
# mesh that consists of hexahedrons. As a result, an .inp file for CalculiX or Abacus
# or a .bdf file for Nastran is generated.
flexibility_rot_symm_3D_geo.create_3D_mesh(filename)

# STATIC ANALYSIS
# if the solver CalculiXis chosen for the static analysis the following function performs the analysis
# (second argument determines which CalculiX command is used: = 1 with ccxlsf, = 2 with ccx)
static_calcs_CalculiX.static_calcs(filename, 1)