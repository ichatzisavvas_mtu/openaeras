"""
This extracts the necessary parameters from the CalculiX .dat file results 
and writes them into a .txt file.                           

author: Lukas Braun
"""

import numpy as np
import os

# read csv parameter file
# number of lines of the header
header = 5

# geometry parameter
data = np.loadtxt("parameter.csv", skiprows=header, delimiter=",")
# number of geometrical samples
number_case = len(data)

# write a text file that contains all the necessary displacements and rotations at the right side    
for l in range(0, number_case):
    number_of_elements = data[l, 4]
    # access and read the .pch file
    os.chdir("element_%s" % str(l+1))
    linelist = open("element_" + str(l+1) + ".dat").readlines()
    # write the .txt file
    f = open("element_" + str(l+1) + ".txt", "w")
    for line in linelist[int(3)]:
        f.write(line)
    for line in linelist[int(4)]:
        f.write(line)
    for line in linelist[int(9)]:
        f.write(line)   
    for line in linelist[int(13)]:
        f.write(line)   
    for line in linelist[int(19)]:
        f.write(line)        
    f.close
    os.chdir("..")
