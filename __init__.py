"""
==================================
openAERAS

Aircraft
Engine
Rotordynamic
Analysis
Software
==================================
"""

from openAERAS import Analysis
from openAERAS import Bearings
from openAERAS import Benchmarks
from openAERAS import Data_Processing
from openAERAS import Examples
from openAERAS import External_Software
from openAERAS import Graphical_Interface
from openAERAS import Optimization
from openAERAS import Structure_Modeling

__version__ = "0.2.0"
__author__ = "Ioannis Chatzisavvas, Lukas Braun"
