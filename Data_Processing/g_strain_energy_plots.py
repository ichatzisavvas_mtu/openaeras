import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def se_bars(se_list, part_name, **kargs):
    """
    Bar diagram for the strain energy of each rotor
    
    Input
        se_list
    
    Output
        
    """

    rotor_num = len(part_name)

    colors = []
    for rn in range(rotor_num):
        for ii in range(len(se_list)):
            if se_list[ii, rn * 2 + 1] < 20:
                colors.append("blue")
            else:
                colors.append("red")

    for ii in range(rotor_num):
        if kargs["pres"] == "yes":
            fig, ax = plt.subplots(figsize=(14, 2), dpi=150, facecolor='w', edgecolor='k')
        else:
            fig, ax = plt.subplots(figsize=(10, 6), dpi=150, facecolor='w', edgecolor='k')
        plt.xticks(range(1, len(se_list) + 1), se_list[0::, ii * 2])
        ax.bar(range(1, len(se_list) + 1), se_list[0::, ii * 2 + 1],
               color=colors[len(se_list) * ii:len(se_list) * (ii + 1)], edgecolor='black', hatch="/")
        for jj in range(len(se_list)):
            ax.text(jj + .9, se_list[jj, ii * 2 + 1] + 0.5, str(int(np.ceil(se_list[jj, ii * 2 + 1]))))
        ax.set_title(part_name[ii])
        ax.set_ylim([0, 1.1 * np.max(se_list[0::, ii * 2 + 1])])
        if kargs["save_fig"] == "yes":
            plt.savefig(kargs["file_path"] + str(part_name[ii]) + '.png')
            path_save = kargs["file_path"]
        if kargs["export_csv"] == "yes":
            dst_file = kargs["file_path"] + "strain_energy.csv"
            se_list_csv = pd.DataFrame(se_list)
            col_name = []
            for ii in range(rotor_num):
                col_name.append(["Rotational_Speed [rpm]", part_name[ii] + " Strain Energy %"])
            col_name_list = [item for x in col_name for item in x]
            se_list_csv.columns = col_name_list
            pd.DataFrame(se_list_csv).to_csv(dst_file)
    if kargs["single_bars"] == "yes":
        if kargs["save_fig"] == "yes":
            se_bars_single(se_list, part_name, path_save)
        else:
            se_bars_single(se_list, part_name, [])

                

    return None


def se_bars_single(se_list, part_name, path_save):
    """
    Create a single bar for all rotors
    
    Input
        se_list
    
    Output
        
    """
    rows = part_name.copy()
    #rows.insert(0, "rot. speed [rpm]")

    fig, ax = plt.subplots(figsize=(10, 6), dpi=150, facecolor='w', edgecolor='k')
       
    ax.set_xticks([])
    
    bar_width = 6/len(se_list)
    index = np.arange (len(se_list)) + bar_width/2
    
    y_offset = np.zeros(len(se_list))
    cell_text = []
    cell_text_speeds = []
    for ii in range(len(part_name)):
        ax.bar(index, se_list[0::, ii * 2 + 1], bar_width, bottom = y_offset,
               edgecolor='black', hatch="/")
        y_offset = y_offset + se_list[0::, ii * 2 + 1]
        se_inlist = ([int(np.ceil(x)) for x in se_list[0::, ii * 2+1]])
        cell_text.append(se_inlist)
        se_inlist_speeds = ([int(np.ceil(x)) for x in se_list[0::, ii * 2]])
        cell_text_speeds.append(se_inlist_speeds)
        
    
    the_table1 = ax.table(cellText = cell_text,
                         rowLabels = rows,
                         loc = "bottom")
    the_table1.auto_set_font_size (False)
    the_table1.set_fontsize(10)
    
    the_table2 = ax.table(cellText = cell_text_speeds,
                         rowLabels = rows,
                         loc = "top")
    the_table2.auto_set_font_size (False)
    the_table2.set_fontsize(8)
    ax.legend (part_name)   
    ax.set_ylabel ("Strain Energy [%]")
    ax.set_xlim ([0-bar_width/2,len(se_list)-bar_width/2 ])
    ax.plot([0-bar_width/2, len(se_list)],[20,20], "r", linewidth = 4)
    if path_save:
        plt.savefig(path_save + 'Single_Bar'+ str(part_name[0])+'R.png')

    return None
