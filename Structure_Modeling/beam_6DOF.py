"""
The module includes the models for beam elements.

Timoshenko beams are used with 6DOF for each element node
In total, 12 DOF for each element are used.

The displacement and angles used are
q : [ux, uy, uz, rx, ry, rz], where

    ux: displacement in x direction 
    uy: displacement in y direction 
    uz: displacement in z direction 
    rx: angle over x direction 
    ry: angle over y direction 
    rz: angle over z direction 
    
    z is the axial direction
    x,y are the lateral directions

"""

import numpy as np


#%%
def beam_cylindrical_elements(el_length, el_outer_diameter, el_inner_diameter, rho, E, nu, **kargs):
    
    """
    
    Definition of cylindrical beam elements.
    
    The Mass, Stiffness and Gyroscopic matrices are defined here.
    The matrices have 6 DOF
    
    Input
    el_length         : in [mm]  -> The length of the Finite Beam Element
    el_outer_diameter : in [mm]  -> The outer diamteter of the Finite Beam Element
    el_inner_diameter : in [mm]  -> The outer diamteter of the Finite Beam Element
    el_axial_force    : in [N]   -> The axial force applied to the Finite Beam Element
    el_torque         : in [Nmm] -> The torque applied of the Finite Beam Element
    
    The args arguments include the material data
        rho: density
        nu : Poisson ratio
        E  : Young's Modulus
    
    The kargs arguments include the model parameters
        shear_activation: (yes) or no
        gyroscopic_activation: (yes) or no
        rotary_inertia_activation: (yes) or no
    In parenthesis is the default value
    
    Output
    
    
    """
    
    # general calculations
    # area
    A = 0.25 * np.pi * (el_outer_diameter**2 - el_inner_diameter**2)
    # axial moment of inertia (second moment of area) for cylinder
    Ixx = 0.015625*np.pi*(el_outer_diameter**4-el_inner_diameter**4)
    # polar moment of inertia (second moment of area) for cylinder
    Izz = 2 * Ixx
    # Shear Modulus
    G = E / (2 * (1 + nu))

    # shear effects
    # the Timoschenko shear coefficient kappa is given here for a hollow shaft
    diameter_ratio = el_inner_diameter / el_outer_diameter
    # Cowper
    kappa = 6 * (1+diameter_ratio**2)**2 * (1+nu) / \
             ((1+diameter_ratio**2)**2*(7+6*nu)+diameter_ratio**2*(20+12*nu))

#    kappa = 5.50911665E-01 #Beam 1 from Nastran
#    kappa = 5.00347078E-01 #Beam 2 from Nastran
#    kappa = 5.00085056E-01 #Beam 2 from Nastran

    # Hutchinson         
    #kappa = 6 * (1+diameter_ratio**2)**2 * (1+nu)**2 / \
    #         ((1+diameter_ratio**2)**2*(7+12*nu+4*nu**2)+4*diameter_ratio**2*(5+6*nu+2*nu**2))
    phi = 12 / el_length**2 * (E*Ixx / (kappa * G * A))
    # torsional correction factor
    kappa_tor = 1.0
    
    # Finite Beam Stiffness matrix
    k1 = 12 * E * Ixx / ((1+phi) * el_length**3)
    k2 = E * A / el_length
    k3 = E * Ixx * (4+phi) / ((1+ phi) * el_length)
    k4 = E * Ixx * (2-phi) / ((1+ phi) * el_length)
    k5 = 6 * E * Ixx / ((1+phi) * el_length**2)
    k6 = kappa_tor * G * Izz / el_length
    
    Ke = np.array([[k1    ,0     ,0     ,0     ,k5    ,0     ,-k1   ,0     ,0     ,0     ,k5    ,0     ],
                   [0     ,k1    ,0     ,-k5   ,0     ,0     ,0     ,-k1   ,0     ,-k5   ,0     ,0     ],
                   [0     ,0     ,k2    ,0     ,0     ,0     ,0     ,0     ,-k2   ,0     ,0     ,0     ],
                   [0     ,-k5   ,0     ,k3    ,0     ,0     ,0     ,k5    ,0     ,k4    ,0     ,0     ],
                   [k5    ,0     ,0     ,0     ,k3    ,0     ,-k5   ,0     ,0     ,0     ,k4    ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,k6    ,0     ,0     ,0     ,0     ,0     ,-k6   ],
                   [-k1   ,0     ,0     ,0     ,-k5   ,0     ,k1    ,0     ,0     ,0     ,-k5   ,0     ],
                   [0     ,-k1   ,0     ,k5    ,0     ,0     ,0     ,k1    ,0     ,k5    ,0     ,0     ],
                   [0     ,0     ,-k2   ,0     ,0     ,0     ,0     ,0     ,k2    ,0     ,0     ,0     ],
                   [0     ,-k5   ,0     ,k4    ,0     ,0     ,0     ,k5    ,0     ,k3    ,0     ,0     ],
                   [k5    ,0     ,0     ,0     ,k4    ,0     ,-k5   ,0     ,0     ,0     ,k3    ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,-k6   ,0     ,0     ,0     ,0     ,0     ,k6    ]])
    
    # Finite Beam Mass matrix - Translational Inertia
    m1 = 312 + 588 * phi + 280 * phi**2
    m2 = (44 + 77 * phi + 35 * phi**2)*el_length
    m3 = 108 + 252 * phi + 140 * phi**2
    m4 = (26 + 63 * phi + 35 * phi**2)*el_length
    m5 = (8 + 14 * phi + 7 * phi * phi)*el_length**2
    m6 = (6 + 14*phi +7*phi**2)*el_length**2
    m7 = 280 *(1 + phi)**2
    m8 = 140 *(1 + phi)**2

    M_ti = np.array([[m1    ,0     ,0     ,0     ,m2    ,0     ,m3    ,0     ,0     ,0     ,-m4   ,0     ],
                     [0     ,m1    ,0     ,-m2   ,0     ,0     ,0     ,m3    ,0     ,m4    ,0     ,0     ],
                     [0     ,0     ,m7    ,0     ,0     ,0     ,0     ,0     ,m8    ,0     ,0     ,0     ],
                     [0     ,-m2   ,0     ,m5    ,0     ,0     ,0     ,-m4   ,0     ,-m6   ,0     ,0     ],
                     [m2    ,0     ,0     ,0     ,m5    ,0     ,m4    ,0     ,0     ,0     ,-m6   ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                     [m3    ,0     ,0     ,0     ,m4    ,0     ,m1    ,0     ,0     ,0     ,-m2   ,0     ],
                     [0     ,m3    ,0     ,-m4   ,0     ,0     ,0     ,m1    ,0     ,m2    ,0     ,0     ],
                     [0     ,0     ,m8    ,0     ,0     ,0     ,0     ,0     ,m7    ,0     ,0     ,0     ],
                     [0     ,m4    ,0     ,-m6   ,0     ,0     ,0     ,m2    ,0     ,m5    ,0     ,0     ],
                     [-m4   ,0     ,0     ,0     ,-m6   ,0     ,-m2   ,0     ,0     ,0     ,m5    ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ]])
    M_ti = rho*A*el_length*M_ti/(840*(1+phi)**2)    
    
    # Finite Beam Mass matrix - Rotational Inertia
    m9 = 36
    m10 = (3 - 15*phi)*el_length
    m11 = (4 + 5*phi +10*phi**2)*el_length**2
    m12 = (-1 - 5*phi + 5*phi**2)*el_length**2
    m13 = 10*el_length**2*Izz*(1+phi)**2/Ixx
    m14 = 5*el_length**2*Izz*(1+phi)**2/Ixx

    M_ri = np.array([[m9    ,0     ,0     ,0     ,m10   ,0     ,-m9   ,0     ,0     ,0     ,m10   ,0     ],
                     [0     ,m9    ,0     ,-m10  ,0     ,0     ,0     ,-m9   ,0     ,-m10  ,0     ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                     [0     ,-m10  ,0     ,m11   ,0     ,0     ,0     ,m10   ,0     ,m12   ,0     ,0     ],
                     [m10   ,0     ,0     ,0     ,m11   ,0     ,-m10  ,0     ,0     ,0     ,m12   ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,m13   ,0     ,0     ,0     ,0     ,0     ,m14   ],
                     [-m9   ,0     ,0     ,0     ,-m10  ,0     ,m9    ,0     ,0     ,0     ,-m10  ,0     ],
                     [0     ,-m9   ,0     ,m10   ,0     ,0     ,0     ,m9    ,0     ,m10   ,0     ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                     [0     ,-m10  ,0     ,m12   ,0     ,0     ,0     ,m10   ,0     ,m11   ,0     ,0     ],
                     [m10   ,0     ,0     ,0     ,m12   ,0     ,-m10  ,0     ,0     ,0     ,m11   ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,m14   ,0     ,0     ,0     ,0     ,0     ,m13   ]])

    M_ri = rho*Ixx*M_ri/(30*el_length*(1+phi)**2)
    Me = M_ti + M_ri
    
    # Finite Beam Gyroscopic matrix
    g1 = 36
    g2 = (3-15*phi)*el_length
    g3 = (4 + 5*phi + 10*phi**2)*el_length**2
    g4 = (-1 - 5*phi + 5*phi**2)*el_length**2
#    C1e = np.array([[0,   -g1,   g2,    0,    0,   g1,   g2,    0],\
#                    [g1,    0,    0,   g2,  -g1,    0,    0,   g2],\
#                    [-g2,   0,    0,  -g3,   g2,    0,    0,  -g4],\
#                    [0,   -g2,   g3,    0,    0,   g2,   g4,    0],\
#                    [0,    g1,  -g2,    0,    0,  -g1,  -g2,    0],\
#                    [-g1,   0,    0,  -g2,   g1,    0,    0,  -g2],\
#                    [-g2,   0,    0,  -g4,   g2,    0,    0,  -g3],\
#                    [0,   -g2,   g4,    0,    0,   g2,   g3,    0]])  
    
    Ge = np.array([[0     ,-g1   ,0     ,g2    ,0     ,0     ,0     ,g1    ,0     ,g2    ,0     ,0     ],
                   [g1    ,0     ,0     ,0     ,g2    ,0     ,-g1   ,0     ,0     ,0     ,g2    ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [-g2   ,0     ,0     ,0     ,-g3   ,0     ,g2    ,0     ,0     ,0     ,-g4   ,0     ],
                   [0     ,-g2   ,0     ,g3    ,0     ,0     ,0     ,g2    ,0     ,g4    ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,g1    ,0     ,-g2   ,0     ,0     ,0     ,-g1   ,0     ,-g2   ,0     ,0     ],
                   [-g1   ,0     ,0     ,0     ,-g2   ,0     ,g1    ,0     ,0     ,0     ,-g2   ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [-g2   ,0     ,0     ,0     ,-g4   ,0     ,g2    ,0     ,0     ,0     ,-g3   ,0     ],
                   [0     ,-g2   ,0     ,g4    ,0     ,0     ,0     ,g2    ,0     ,g3    ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ]])


    Ge = - rho*Ixx*Ge/(15*el_length*(1+phi)**2)    
    
    return Me, Ge, Ke
    
   
#%%
def beam_conical_elements(el_length, el_outer_diameter_left, el_outer_diameter_right,
                          el_inner_diameter_left, el_inner_diameter_right, rho, E, nu):
    
    '''
    Conical beam elements are implemented here. The theory behind it was mainly published in Genta and Gugliota (1988)
    "A conical element for finite element rotor dynamics". The matrix entries governing the axial displacement and
    rotation around the z axis are not published in Genta and Gugliota (1988) and the own derivations are separated
    with a ------- line. The Mass, Stiffness and Gyroscopic matrices are defined here. The beams have 2 nodes and 6 DOF,
    respectively.

    author: Lukas Braun
    
    Parameters
    ----------
    el_length: float
        the length of the beam element.
    el_outer_diameter_left: float
        the outer diameter of the beam element left.
    el_inner_diameter_left: float
        the inner diameter of the beam element left.
    el_outer_diameter_right: float
        the outer diameter of the beam element right.
    el_inner_diameter_right: float
        the inner diameter of the beam element right.
    rho: float
        density.
    E: float
        Young's Modulus.
    nu: float
        Poisson's ratio.
    
    Returns
    ----------
    Ke : array with floats
        element stiffness matrix
    Me : array with floats
        element mass matrix
    Ge : array with floats
        element gyroscopic matrix

    '''
    
    # general calculations
    # area left side of the element
    A = 0.25 * np.pi * (el_outer_diameter_left**2 - el_inner_diameter_left**2)
    # diametral moment of inertia (second moment of area) left side of the element
    Ixx = 0.015625*np.pi*(el_outer_diameter_left**4-el_inner_diameter_left**4)
    # polar moment of inertia (second moment of area) left side of the element
    Izz = 2 * Ixx
    # Shear Modulus
    G = E / (2 * (1 + nu))
    
    # geometrical definitions
    el_outer_radius_left = el_outer_diameter_left/2
    el_outer_radius_right = el_outer_diameter_right/2
    el_inner_radius_left = el_inner_diameter_left/2
    el_inner_radius_right = el_inner_diameter_right/2
    delta_outer_radius = el_outer_radius_right - el_outer_radius_left
    delta_inner_radius = el_inner_radius_right - el_inner_radius_left
    
    # geometrical coefficients (= 0 for cylindrical elements)
    alpha1 = 2*np.pi*(el_outer_radius_left*delta_outer_radius-el_inner_radius_left*delta_inner_radius)/A
    alpha2 = np.pi*(el_outer_radius_left**3*delta_outer_radius-el_inner_radius_left**3*delta_inner_radius)/Ixx
    beta1 = np.pi*(delta_outer_radius**2-delta_inner_radius**2)/A
    beta2 = 3*np.pi*(el_outer_radius_left**2*delta_outer_radius**2-el_inner_radius_left**2*delta_inner_radius**2)/2/Ixx
    gamma2 = np.pi*(el_outer_radius_left*delta_outer_radius**3-el_inner_radius_left*delta_inner_radius**3)/Ixx
    delta2 = np.pi*(delta_outer_radius**4-delta_inner_radius**4)/4/Ixx

    # shear effect
    # The Timoschenko shear coefficient kappa is given here for a hollow shaft (evaluated at the cross section
    # at the center of the element)
    diameter_ratio = (el_inner_diameter_left+el_inner_diameter_right) / (el_outer_diameter_left+el_outer_diameter_right)
    # Cowper
    kappa = 6 * (1+diameter_ratio**2)**2 * (1+nu) / \
                ((1+diameter_ratio**2)**2*(7+6*nu)+diameter_ratio**2*(20+12*nu))
    # Hutchinson         
    # kappa = 6 * (1+diameter_ratio**2)**2 * (1+nu)**2 / \
    #             ((1+diameter_ratio**2)**2*(7+12*nu+4*nu**2)+4*diameter_ratio**2*(5+6*nu+2*nu**2))
    # torsional correction factor
    kappa_tor = 1.0
    A_center = 0.25 * np.pi * (((el_outer_diameter_left+el_outer_diameter_right)/2)**2 -
                               ((el_inner_diameter_left + el_inner_diameter_right)/2)**2)
    Ixx_center = 0.015625*np.pi*(((el_outer_diameter_left+el_outer_diameter_right)/2)**4 -
                                 ((el_inner_diameter_left + el_inner_diameter_right)/2)**4)
    phi = 12 / el_length**2 * (E*Ixx_center / (kappa * G * A_center))
    
    # Finite Bending Stiffness Matrix
    k1 = 1260+630*alpha2+504*beta2+441*gamma2+396*delta2  # there is a mistake in the paper by Genta and Gugliota (1988)
    k2 = el_length*(630+210*alpha2+147*beta2+126*gamma2+114*delta2-phi*(105*alpha2+105*beta2+94.5*gamma2+84*delta2))
    k3 = el_length*(630+420*alpha2+357*beta2+315*gamma2+282*delta2+phi*(105*alpha2+105*beta2+94.5*gamma2+84*delta2))
    k4 = el_length**2*(420+210*phi+105*phi**2+alpha2*(105+52.5*phi**2)+beta2*(56-35*phi+35*phi**2)
                       + gamma2*(42-42*phi+26.25*phi**2)+delta2*(36-42*phi+21*phi**2))
    k5 = el_length**2*(210-210*phi-105*phi**2+alpha2*(105-105*phi-52.5*phi**2)+beta2*(91-70*phi-35*phi**2)
                       + gamma2*(84-52.5*phi-26.25*phi**2)+delta2*(78-42*phi-21*phi**2))
    k6 = el_length**2*(420+210*phi+105*phi**2+alpha2*(315+210*phi+52.5*phi**2)+beta2*(266+175*phi+35*phi**2)
                       + gamma2*(231+147*phi+26.25*phi**2)+delta2*(204+126*phi+21*phi**2))
    # -------
    k7 = el_length**2*(1+phi)**2*A*(105+52.5*alpha1+35*beta1)/Ixx
    
    Kb = np.array([[k1    ,0     ,0     ,0     ,k2    ,0     ,-k1   ,0     ,0     ,0     ,k3    ,0     ],
                   [0     ,k1    ,0     ,-k2   ,0     ,0     ,0     ,-k1   ,0     ,-k3   ,0     ,0     ],
                   [0     ,0     ,k7    ,0     ,0     ,0     ,0     ,0     ,-k7   ,0     ,0     ,0     ],
                   [0     ,-k2   ,0     ,k4    ,0     ,0     ,0     ,k2    ,0     ,k5    ,0     ,0     ],
                   [k2    ,0     ,0     ,0     ,k4    ,0     ,-k2   ,0     ,0     ,0     ,k5    ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [-k1   ,0     ,0     ,0     ,-k2   ,0     ,k1    ,0     ,0     ,0     ,-k3   ,0     ],
                   [0     ,-k1   ,0     ,k2    ,0     ,0     ,0     ,k1    ,0     ,k3    ,0     ,0     ],
                   [0     ,0     ,-k7   ,0     ,0     ,0     ,0     ,0     ,k7    ,0     ,0     ,0     ],
                   [0     ,-k3   ,0     ,k5    ,0     ,0     ,0     ,k3    ,0     ,k6    ,0     ,0     ],
                   [k3    ,0     ,0     ,0     ,k5    ,0     ,-k3   ,0     ,0     ,0     ,k6    ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ]])
    Kb = E*Ixx*Kb/(105*el_length**3*(1+phi)**2)    
    
    # Finite Shear Stiffness Matrix
    k8 = 12+6*alpha1+4*beta1
    k9 = el_length*(6+3*alpha1+2*beta1)
    k10 = el_length**2*(3+1.5*alpha1+beta1)
    # -------
    k11 = (1+phi)**2*kappa_tor*Izz/phi**2*(12+6*alpha2+4*beta2+3*gamma2+2.4*delta2)/A/kappa
    
    # there is a mistake in the paper by Genta and Gugliota (1988) at the matrix entries
    # (7,11), (8,10), (10,8) and (11,7)
    Ks = np.array([[k8    ,0     ,0     ,0     ,k9    ,0     ,-k8   ,0     ,0     ,0     ,k9    ,0     ],
                   [0     ,k8    ,0     ,-k9   ,0     ,0     ,0     ,-k8   ,0     ,-k9   ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,-k9   ,0     ,k10   ,0     ,0     ,0     ,k9    ,0     ,k10   ,0     ,0     ],
                   [k9    ,0     ,0     ,0     ,k10   ,0     ,-k9   ,0     ,0     ,0     ,k10   ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,k11   ,0     ,0     ,0     ,0     ,0     ,-k11  ],
                   [-k8   ,0     ,0     ,0     ,-k9   ,0     ,k8    ,0     ,0     ,0     ,-k9   ,0     ],
                   [0     ,-k8   ,0     ,k9    ,0     ,0     ,0     ,k8    ,0     ,k9    ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,-k9   ,0     ,k10   ,0     ,0     ,0     ,k9    ,0     ,k10   ,0     ,0     ],
                   [k9    ,0     ,0     ,0     ,k10   ,0     ,-k9   ,0     ,0     ,0     ,k10   ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,-k11  ,0     ,0     ,0     ,0     ,0     ,k11   ]])
    Ks = kappa*G*A*phi**2*Ks/(12*el_length*(1+phi)**2)
    Ke = Kb + Ks
    
    # Finite Translational Mass Matrix
    m1 = 468+882*phi+420*phi**2+alpha1*(108+210*phi+105*phi**2)+beta1*(38+78*phi+42*phi**2)
    m2 = el_length*(66+115.5*phi+52.5*phi**2+alpha1*(21+40.5*phi+21*phi**2)+beta1*(8.5+18*phi+10.5*phi**2))
    m3 = 162+378*phi+210*phi**2+alpha1*(81+189*phi+105*phi**2)+beta1*(46+111*phi+63*phi**2)
    m4 = el_length*(39+94.5*phi+52.5*phi**2+alpha1*(18+40.5*phi+21*phi**2)+beta1*(9.5+21*phi+10.5*phi**2))
    m5 = el_length**2*(12+21*phi+10.5*phi**2+alpha1*(4.5+9*phi+5.25*phi**2)+beta1*(2+4.5*phi+3*phi**2))
    m6 = el_length*(39+94.5*phi+52.5*phi**2+alpha1*(21+54*phi+31.5*phi**2)+beta1*(12.5+34.5*phi+21*phi**2))
    m7 = el_length**2*(9+21*phi+10.5*phi**2+alpha1*(4.5*10.5*phi+5.25*phi**2)+beta1*(2.5+6*phi+3*phi**2))
    m8 = 468+882*phi+420*phi**2+alpha1*(360+672*phi+315*phi**2)+beta1*(290+540*phi+252*phi**2)
    m9 = el_length*(66+115.5*phi+52.5*phi**2+alpha1*(45+75*phi+31.5*phi**2)+beta1*(32.5+52.5*phi+21*phi**2))
    m10 = el_length**2*(12+21*phi+10.5*phi**2+alpha1*(7.5+12*phi+5.25*phi**2)+beta1*(5+7.5*phi+3*phi**2))
    # -------
    m11 = (1+phi)**2*(420+105*alpha1+42*beta1)
    m12 = (1+phi)**2*(210+105*alpha1+63*beta1)
    m13 = (1+phi)**2*(420+315*alpha1+252*beta1)

    M_ti = np.array([[m1    ,0     ,0     ,0     ,m2    ,0     ,m3    ,0     ,0     ,0     ,-m4   ,0     ],
                     [0     ,m1    ,0     ,-m2   ,0     ,0     ,0     ,m3    ,0     ,m4    ,0     ,0     ],
                     [0     ,0     ,m11   ,0     ,0     ,0     ,0     ,0     ,m12   ,0     ,0     ,0     ],
                     [0     ,-m2   ,0     ,m5    ,0     ,0     ,0     ,-m6   ,0     ,-m7   ,0     ,0     ],
                     [m2    ,0     ,0     ,0     ,m5    ,0     ,m6    ,0     ,0     ,0     ,-m7   ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                     [m3    ,0     ,0     ,0     ,m6    ,0     ,m8    ,0     ,0     ,0     ,-m9   ,0     ],
                     [0     ,m3    ,0     ,-m6   ,0     ,0     ,0     ,m8    ,0     ,m9    ,0     ,0     ],
                     [0     ,0     ,m12   ,0     ,0     ,0     ,0     ,0     ,m13   ,0     ,0     ,0     ],
                     [0     ,m4    ,0     ,-m7   ,0     ,0     ,0     ,m9    ,0     ,m10   ,0     ,0     ],
                     [-m4   ,0     ,0     ,0     ,-m7   ,0     ,-m9   ,0     ,0     ,0     ,m10   ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ]])
    M_ti = rho*A*el_length*M_ti/(1260*(1+phi)**2)    
    
    # Finite Rotational Mass Matrix
    m14 = 252+126*alpha2+72*beta2+45*gamma2+30*delta2
    m15 = el_length*(21-105*phi+alpha2*(21-42*phi)+beta2*(15-21*phi)+gamma2*(10.5-12*phi)+delta2*(7.5-7.5*phi))
    m16 = el_length*(21-105*phi-63*alpha2*phi-beta2*(6+42*phi)-gamma2*(7.5+30*phi)-delta2*(7.5+22.5*phi))
    m17 = el_length**2*(28+35*phi+70*phi**2+alpha2*(7-7*phi+17.5*phi**2)+beta2*(4-7*phi+7*phi**2)
                        + gamma2*(2.75-5*phi+3.5*phi**2)+delta2*(2-3.5*phi+2*phi**2))
    m18 = el_length**2*(7+35*phi-35*phi**2+alpha2*(3.5+17.5*phi-17.5*phi**2)+beta2*(3+10.5*phi-10.5*phi**2)
                        + gamma2*(2.75+7*phi-7*phi**2)+delta2*(2.5+5*phi-5*phi**2))
    m19 = el_length**2*(28+35*phi+70*phi**2+alpha2*(21+42*phi+52.5*phi**2)+beta2*(18+42*phi+42*phi**2)
                        + gamma2*(16.25+40*phi+35*phi**2)+delta2*(15+37.5*phi+30*phi**2))
    # -------
    m20 = el_length**2*Izz*(1+phi)**2*(70+17.5*alpha2+7*beta2+3.5*gamma2+37*delta2)/Ixx
    m21 = el_length**2*Izz*(1+phi)**2*(35+17.5*alpha2+10.5*beta2+7*gamma2+5*delta2)/Ixx
    m22 = el_length**2*Izz*(1+phi)**2*(70+52.5*alpha2+42*beta2+35*gamma2+30*delta2)/Ixx

    # there is a mistake in the paper by Genta and Gugliota (1988) at the matrix entries (7,7) and (8,8)
    M_ri = np.array([[m14   ,0     ,0     ,0     ,m15   ,0     ,-m14  ,0     ,0     ,0     ,m16   ,0     ],
                     [0     ,m14   ,0     ,-m15  ,0     ,0     ,0     ,-m14  ,0     ,-m16  ,0     ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                     [0     ,-m15  ,0     ,m17   ,0     ,0     ,0     ,m15   ,0     ,-m18  ,0     ,0     ],
                     [m15   ,0     ,0     ,0     ,m17   ,0     ,-m15  ,0     ,0     ,0     ,-m18  ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,m20   ,0     ,0     ,0     ,0     ,0     ,m21   ],
                     [-m14  ,0     ,0     ,0     ,-m15  ,0     ,m14   ,0     ,0     ,0     ,-m16  ,0     ],
                     [0     ,-m14  ,0     ,m15   ,0     ,0     ,0     ,m14   ,0     ,m16   ,0     ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                     [0     ,-m16  ,0     ,-m18  ,0     ,0     ,0     ,m16   ,0     ,m19   ,0     ,0     ],
                     [m16   ,0     ,0     ,0     ,-m18  ,0     ,-m16  ,0     ,0     ,0     ,m19   ,0     ],
                     [0     ,0     ,0     ,0     ,0     ,m21   ,0     ,0     ,0     ,0     ,0     ,m22   ]])

    M_ri = rho*Ixx*M_ri/(210*el_length*(1+phi)**2)
    Me = M_ti + M_ri
    
    # Finite Beam Gyroscopic matrix  
    g1 = 252+126*alpha2+72*beta2+45*gamma2+30*delta2
    g2 = el_length*(21-105*phi+alpha2*(21-42*phi)+beta2*(15-21*phi)+gamma2*(10.5-12*phi)+delta2*(7.5-7.5*phi))
    g3 = el_length*(21-105*phi-63*alpha2*phi-beta2*(6+42*phi)-gamma2*(7.5+30*phi)-delta2*(7.5+22.5*phi))
    g4 = el_length**2*(28+35*phi+70*phi**2+alpha2*(7-7*phi+17.5*phi**2)+beta2*(4-7*phi+7*phi**2)
                       + gamma2*(2.75-5*phi+3.5*phi**2)+delta2*(2-3.5*phi+2*phi**2))
    g5 = el_length**2*(7+35*phi-35*phi**2+alpha2*(3.5+17.5*phi-17.5*phi**2)+beta2*(3+10.5*phi-10.5*phi**2)
                       + gamma2*(2.75+7*phi-7*phi**2)+delta2*(2.5+5*phi-5*phi**2))
    g6 = el_length**2*(28+35*phi+70*phi**2+alpha2*(21+42*phi+52.5*phi**2)+beta2*(18+42*phi+42*phi**2)
                       + gamma2*(16.25+40*phi+35*phi**2)+delta2*(15+37.5*phi+30*phi**2))
    
    # there is a mistake in the paper by Genta and Gugliota (1988) at the matrix entries (7,8) and (8,7)
    Ge = np.array([[0     ,g1    ,0     ,-g2   ,0     ,0     ,0     ,-g1   ,0     ,-g3   ,0     ,0     ],
                   [-g1   ,0     ,0     ,0     ,-g2   ,0     ,g1    ,0     ,0     ,0     ,-g3   ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [g2    ,0     ,0     ,0     ,g4    ,0     ,-g2   ,0     ,0     ,0     ,-g5   ,0     ],
                   [0     ,g2    ,0     ,-g4   ,0     ,0     ,0     ,-g2   ,0     ,g5    ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,-g1   ,0     ,g2    ,0     ,0     ,0     ,g1    ,0     ,g3    ,0     ,0     ],
                   [g1    ,0     ,0     ,0     ,g2    ,0     ,-g1   ,0     ,0     ,0     ,g3    ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [g3    ,0     ,0     ,0     ,-g5   ,0     ,-g3   ,0     ,0     ,0     ,g6    ,0     ],
                   [0     ,g3    ,0     ,g5    ,0     ,0     ,0     ,-g3   ,0     ,-g6   ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ]])
    Ge = rho*Izz*Ge/(210*el_length*(1+phi)**2)    
    
    return Me, Ge, Ke
