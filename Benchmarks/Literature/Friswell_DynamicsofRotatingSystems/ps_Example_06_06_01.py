"""
Python Script for the Example 060601 / 6.6.1 pp.272.

Use example for checking the analysis methods
"""


import numpy as np
from openAERAS.Analysis import eigenfrequency_analysis, frequency_response, transient_analysis
from openAERAS.Structure_Modeling import beam_assembly
from openAERAS.Graphical_Interface import non_graphical_interface


#%%
"""
- Rotating Part Definition
    - Shaft Definition
    - Blade / Disk / Rigid_body Definition
- Casing Definition
    - Flexible Body Definition
    - Rigid_body Definition
- Bearing Definition
- Span Coupling Definition
"""

##############################################################################
##############################################################################
##############################################################################
# Rotating Part Definition
E = 2.07e11 # Young Modulus [N/m^2]
nu = 0.3 # Poisson ratio [-]
rho = 8300 # Density [kg/m^3]
damping_factor = 0    # damping in shaft
damping_type = 'stiffness_proportional' # type of damping
#damping_factor =0.05 * 2 / (11.372*2*np.pi)
shaft_od_1 = 0.03 # Shaft Outer Diameter [m]
shaft_id_1 = 0.0  # Shaft Inner Diameter [m]
shaft_od_2 = 0.06 # Shaft Outer Diameter [m]
shaft_id_2 = 0.05 # Shaft Inner Diameter [m]
rotor_start = {"lp_1" : 0, "hp_1" : 0.152} # starting point of rotors [mm]
# rotor speed ratio
#rotor_speed_ratio = {"lp_rot_speed_ratio" : 1, "hp_rot_speed_ratio" : 1.5} 
rotor_speed_ratio = [["lp",[1],'negative_z'], ["hp", [1.5],'positive_z']]


rotor_shaft = (
[
 [[1, rotor_start["lp_1"], damping_type, damping_factor, rotor_speed_ratio[0]],      
       [["Beam"], 0.076, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.083, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.095, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.07, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.082, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.051, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       [["Beam"], 0.051, [shaft_od_1, shaft_id_1], [], rho, E, nu]],
 [[2, rotor_start["hp_1"], damping_type, damping_factor, rotor_speed_ratio[1]],      
       [["Beam"], 0.051, [shaft_od_2, shaft_id_2], [], rho, E, nu],
       [["Beam"], 0.076, [shaft_od_2, shaft_id_2], [], rho, E, nu],
       [["Beam"], 0.077, [shaft_od_2, shaft_id_2], [], rho, E, nu],
       [["Beam"], 0.05, [shaft_od_2, shaft_id_2], [], rho, E, nu]],
]
)

    
##############################################################################
# Disk Definition
#rho = 3842.63/(0.25*np.pi*disk_thick*(disk_od**2-shaft_od**2))

# rotor_blade_disk = (
# [
#  ["blade",0,4, rho, disk_thick, disk_od, shaft_od],
#  ['disk', 1,4, rho, disk_thick, disk_od, shaft_od],
#  ['rigid_body', 1,4, mass, Id, Ip]
# ]
# )


rotor_blade_disk = (
[
 ["rigid_body", "lp", 1, 2, 10.5, 0.043, 0.086],
 ["rigid_body", "lp", 1, 7, 7, 0.034, 0.068], 
 ["rigid_body", "hp", 2, 2, 7, 0.021, 0.042], 
 ["rigid_body", "hp", 2, 4, 3.5, 0.013, 0.026]
]
)


##############################################################################
##############################################################################
##############################################################################
# Casing Definition
# casing_start = {"casing_1" : 0, "casing_2" : 0.4} # starting point of casing [mm]

casing_flex = ([])
# casing_flex = (
# [
#  [[1, casing_start["casing_1"], damping_type, damping_factor],      
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu],
#        [0.2, 0.4, 0.3, rho, E, nu]],
#  [[2, casing_start["casing_2"], damping_type, damping_factor],      
#        [0.2, 0.54, 0.5, rho, E, nu],
#        [0.2, 0.54, 0.5, rho, E, nu]]
# ]
# )

casing_rigid = ([])
# casing_rigid = (
# [
#  [1, 3, 0.5,100,200] 
# ]
# )



##############################################################################
##############################################################################
##############################################################################
# Bearing Definition
# 1 Parameter type
# 2 Parameter node
# 3 Parameter: list

# rotor_bearing = (
# [
#  ["linear_bearing_cc",rotor_number,rotor_dof, 1e16,1e16,1e16,0,0,0,100,100,0,0,0,0],
# ]
# )
    
rotor_casing_bearing = (
[
 ["linear_bearing_cc",["rotor",1,1],[], [26e6,52e6,1e10,0,0,1e10],[20,20,0,0,0,0]],
 ["linear_bearing_cc",["rotor",1,8],[], [18e6,36e6,1e10,0,0,1e10],[20,20,0,0,0,0]],
 ["linear_bearing_cc",["rotor",2,1],[], [18e6,36e6,1e10,0,0,1e10],[20,20,0,0,0,0]],
 ["linear_bearing_cc",["rotor",1,6],["rotor",2,5],[9e6,9e6,0,0,0,0],[20,20,0,0,0,0]] 
]
)

##############################################################################
##############################################################################
##############################################################################
# Span Coupling Definition
span_coupling = ([])
# span_coupling = (
# [
#  ["rotor",  [1,6], [2,1], [1e16,1e16,1e16,1e16,1e16,1e16]],
#  ["casing", [1,3], [2,1], [1e16,1e16,1e16,1e16,1e16,1e16]],
#  ["casing", [1,5], [2,3], [1e16,1e16,1e16,1e16,1e16,1e16]]
# ]
# )    
    
##############################################################################
##############################################################################
##############################################################################
# Unbalance
rotor_unbalance = (
[
 ["lp", 1, 2, [[0, 0.0001, 0], [1,0.0002,0],   [4,0.0005,180]]],
 ["hp", 2, 2, [[0, 0.0004, 0], [1,0.0008,180], [4,0.0008,180]]]
]
)   


##############################################################################
##############################################################################
##############################################################################
# Collecting rotor/bearing/casing/unbalance
rotor_casing_geometry = [rotor_shaft, rotor_blade_disk, casing_flex, casing_rigid, \
                         rotor_casing_bearing, span_coupling, [rotor_unbalance]]


##############################################################################
##############################################################################
##############################################################################
# For eigenfrequency analyis
ea_ax_eq = 1 # axis equal   
ea_fmax = 400 # [Hz] max eigenfrequncy for y limit
ea_omega_max = 15000*2*np.pi/60 # [rad/s] max eigenfrequncy for the calculation of the diagram
ea_n_camp = 20 # number of points for the Campbell diagram
ea_n_eigs = 30 # number of eigenfrequencies to plot
ea_modes_plot = [] # modeshapes to plot
ea_modes_plot_speed = [] # the speed of modeshapes to plot
ea_mode_tracking = False # True of False actication of mode tracking
eig_animate = False # Animate mode shape
ea_n_p_e = 4 # 6 or 4 (axial and torsional removed)
  
    
##############################################################################
##############################################################################
##############################################################################
# For frequency response analysi
fr_ax_eq = 1 # axis equal   
fr_fmax = 15000/60  # [Hz] max frequency for the frequency response calculation
fr_n_fpoints = 1501 # number of frequency points
fr_plot_node = [[1,2], [1,7], [2,2], [2,4]] # node to be plot
fr_x_plot_lim = 15000 # [rpm] Plot limit 
fr_log_lin = "log" # "linear", or "log" y scale
fr_n_p_e = 4 # 6 or 4 (axial and torsional removed)

##############################################################################
##############################################################################
##############################################################################
# For transient analysis
tr_ax_eq = 1 # axis equal   
tr_tmax= 15  # [s] simulation time
#tr_n_fpoints = 1501 # number of frequency points
#tr_plot_node = [[1,2], [1,7], [2,2], [2,4]] # node to be plot
#tr_x_plot_lim = 15000 # [rpm] Plot limit 
#tr_log_lin = "log" # "linear", or "log" y scale
tr_n_p_e = 6 # 6 or 4 (axial and torsional removed)
omega_trans = [[["lp", "negative_z"],[0,0], [10,10000/60*2*np.pi], [15,15000/60*2*np.pi]], \
               [["hp", "positive_z"],[0,0], [7,15000/60*2*np.pi], [15,22500/60*2*np.pi]]]

    
##############################################################################
##############################################################################
##############################################################################
# For quasistatic analysis
qs_ax_eq = 1 # axis equal   
qs_tmax= 15  # [s] simulation time
g_loads = ["true", "x", "y", "z"]
gyro_loads = ["true", "yaw", "pitch", "roll"]
gyro_loads_acc = ["true", "yaw_acc", "pitch_acc", "roll_acc"]
#tr_n_fpoints = 1501 # number of frequency points
#tr_plot_node = [[1,2], [1,7], [2,2], [2,4]] # node to be plot
#tr_x_plot_lim = 15000 # [rpm] Plot limit 
#tr_log_lin = "log" # "linear", or "log" y scale
qs_n_p_e = 4 # 6 or 4 (axial and torsional removed)

    
    

##############################################################################
##############################################################################
##############################################################################
# Define Analysis Type
graphic_plot = True
eig_analysis = True
freq_analysis = False
tran_analysis = False
quasi_static_analysis = False

#%%
# Rotor/Casing Visualisation
if graphic_plot:
    non_graphical_interface.vis_rotor_casing (rotor_casing_geometry, ax_eq=1)

#%% 
# Model Creation
M_r, C_r, G_r, K_r = beam_assembly.beam_structure_assembly(rotor_casing_geometry, False)
if rotor_casing_geometry[2]: 
    M_c, C_c, K_c = beam_assembly.beam_structure_assembly(rotor_casing_geometry, False)
else:
    M = M_r
    C = C_r
    G = G_r
    K = K_r

#%%
# Eigenfrequency Analyis

if eig_analysis:
    g_vector_s_i, g_value_, AA, g_values = eigenfrequency_analysis.calcs_eigenfrequency_analysis \
                                            (M, C, G, K, rotor_casing_geometry, 
                                             ea_ax_eq, ea_omega_max, ea_fmax, ea_n_camp, ea_n_eigs, 
                                             ea_modes_plot, ea_modes_plot_speed, ea_mode_tracking, eig_animate, ea_n_p_e)
                                            
#%%
# Frequency Response Analyis

if freq_analysis:
    freq_resp = frequency_response.calcs_frequency_response (M, C, G, K, rotor_casing_geometry, 
                                                 fr_ax_eq, fr_fmax, fr_n_fpoints, 
                                                 fr_plot_node, fr_x_plot_lim, fr_log_lin, fr_n_p_e)
    
    
#%%
# Transient Analyis

if tran_analysis:
    transient_response = transient_analysis.calcs_transient_analysis (M, C, G, K, rotor_casing_geometry, 
                                                 tr_ax_eq, tr_tmax, omega_trans, tr_n_p_e)
    
#%%
# Quasi Static Analysis

if quasi_static_analysis:
    quasis_static_response = quasi_static_analysis.calcs_quasi_static_analysis (M, C, G, K, rotor_casing_geometry, 
                                                 tr_ax_eq, tr_tmax, omega_trans, qs_n_p_e)
    


