"""
This module can be used to visualise a rotor/casing without a graphical interface
"""


import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


#%% 
def vis_rotor_casing (rotor_casing_geometry, ax_eq):
    '''
    Visualization of Rotors
    
    Input:
    rotor_casing_geometry = [rotor_shaft, rotor_blade_disk, casing_flex, casing_rigid, \
                         rotor_bearing, span_coupling, rotor_unbalance]
        
    Output:
        
    '''
        
    ########################################################################
    nspans_rotor = len(rotor_casing_geometry[0])
    nspans_casing = len(rotor_casing_geometry[2])   
    # Plot   
    fig, ax = plt.subplots()
    plt.axis("off")
    if ax_eq == 1:
        ax.axis('equal')
    
    # middle line        
    l_tot = middle_line(rotor_casing_geometry) 
    l_tot_min  = l_tot[0]
    l_tot_max  = l_tot[1]
    ext_fact = 0.1
    if l_tot_min ==0:
        l_tot_min = - ext_fact * l_tot_max
    ax.plot([l_tot_min, l_tot_max*(1+ext_fact)], np.zeros(len(l_tot)),'k-.')
    
    # Shaft Plot
    for span in range (nspans_rotor):
        if rotor_casing_geometry[0][span][0][4][0] == "hp":
            plot_color = "r"
        elif rotor_casing_geometry[0][span][0][4][0] == "lp":
            plot_color = "g"
        dof_pos = span_axial_position(rotor_casing_geometry, "rotor", span)
        for elem in range(1,len(rotor_casing_geometry[0][span])):
            inner_diameter_l = rotor_casing_geometry[0][span][elem][2][1]
            outer_diameter_l = rotor_casing_geometry[0][span][elem][2][0]
            if not rotor_casing_geometry[0][span][elem][3]:
                inner_diameter_r = inner_diameter_l
                outer_diameter_r = outer_diameter_l
            else:
                inner_diameter_r = rotor_casing_geometry[0][span][elem][3][1]
                outer_diameter_r = rotor_casing_geometry[0][span][elem][3][0]
            
            # inner diameter shaft
            ax.plot([dof_pos[elem-1],dof_pos[elem]], [inner_diameter_l, inner_diameter_r], plot_color)
            #outer diameter shaft
            ax.plot([dof_pos[elem-1],dof_pos[elem]], [outer_diameter_l, outer_diameter_r], plot_color)
            # plot element vertical lines shaft
            ax.plot([dof_pos[elem-1],dof_pos[elem-1]], [inner_diameter_l, outer_diameter_l], plot_color)
            ax.plot([dof_pos[elem],dof_pos[elem]], [inner_diameter_r, outer_diameter_r], plot_color)
 
    # Disks/Blade/Rigid body rotor plot
    for disk in range(len(rotor_casing_geometry[1])):
        first_node = 0 # check if the rigid body is placed as a first element in the span
        if rotor_casing_geometry[1][disk][1] == "hp":
            plot_color = "g"
        elif rotor_casing_geometry[1][disk][1] == "lp":
            plot_color = "r"
        if rotor_casing_geometry[1][disk][0] == "blade" or rotor_casing_geometry[1][disk][0] == "disk":
            dof_pos = span_axial_position(rotor_casing_geometry, "rotor", rotor_casing_geometry[1][disk][2]-1)
            inner_diameter = rotor_casing_geometry[1][disk][7]
            outer_diameter = rotor_casing_geometry[1][disk][6]
            half_length = rotor_casing_geometry[1][disk][5]/2
            node = rotor_casing_geometry[1][disk][3]
            # inner diameter shaft
            ax.plot([dof_pos[node-1] - half_length, dof_pos[node-1] + half_length], [inner_diameter, inner_diameter], plot_color)
            #outer diameter shaft
            ax.plot([dof_pos[node-1] - half_length, dof_pos[node-1] + half_length], [outer_diameter, outer_diameter], plot_color)
            # plot element vertical lines shaft
            ax.plot([dof_pos[node-1] - half_length, dof_pos[node-1] - half_length], [inner_diameter, outer_diameter], plot_color)
            ax.plot([dof_pos[node-1] + half_length, dof_pos[node-1] + half_length], [inner_diameter, outer_diameter], plot_color)
        elif rotor_casing_geometry[1][disk][0] == "rigid_body":
            dof_pos = span_axial_position(rotor_casing_geometry, "rotor", rotor_casing_geometry[1][disk][2]-1)
            node = rotor_casing_geometry[1][disk][3] - 1 
            span = rotor_casing_geometry[1][disk][2] - 1
            if not rotor_casing_geometry[0][span][node][3]:
                if node == 0: # check if the rigid body is placed as a first element in the span
                    node = 1
                    first_node = 1 
                inner_diameter = rotor_casing_geometry[0][span][node][2][1]
                outer_diameter = rotor_casing_geometry[0][span][node][2][0]
            else:
                if node == 0: # check if the rigid body is placed as a first element in the span
                    node = 1
                    first_node = 1
                inner_diameter = rotor_casing_geometry[0][span][node][3][1]
                outer_diameter = rotor_casing_geometry[0][span][node][3][0]
            # plot at the middle of outer and inner diameter
            if first_node == 1: # check if the rigid body is placed as a first element in the span
                node = 0
            ax.plot([dof_pos[node]], 0.5*(inner_diameter + outer_diameter), plot_color+str("D"))
        else:
            pass
            
        
    # Casing Plot
    for span in range (nspans_casing):
        plot_color = "b"
        dof_pos = span_axial_position(rotor_casing_geometry, "casing", span)
        for elem in range(1,len(rotor_casing_geometry[2][span])):
            inner_diameter_l = rotor_casing_geometry[2][span][elem][2][1]
            outer_diameter_l = rotor_casing_geometry[2][span][elem][2][0]
            if not rotor_casing_geometry[2][span][elem][3]:
                inner_diameter_r = inner_diameter_l
                outer_diameter_r = outer_diameter_l
            else:
                inner_diameter_r = rotor_casing_geometry[2][span][elem][3][1]
                outer_diameter_r = rotor_casing_geometry[2][span][elem][3][0]
            
            # inner diameter shaft
            ax.plot([dof_pos[elem-1],dof_pos[elem]], [inner_diameter_l, inner_diameter_r], plot_color)
            #outer diameter shaft
            ax.plot([dof_pos[elem-1],dof_pos[elem]], [outer_diameter_l, outer_diameter_r], plot_color)
            # plot element vertical lines shaft
            ax.plot([dof_pos[elem-1],dof_pos[elem-1]], [inner_diameter_l, outer_diameter_l], plot_color)
            ax.plot([dof_pos[elem],dof_pos[elem]], [inner_diameter_r, outer_diameter_r], plot_color)


    # Casing Rigid Bodies
    for disk in range(len(rotor_casing_geometry[3])):
        first_node = 0 # check if the rigid body is placed as a first element in the span
        plot_color = "y"
        dof_pos = span_axial_position(rotor_casing_geometry, "casing", rotor_casing_geometry[3][disk][2]-1)
        node = rotor_casing_geometry[3][disk][3] - 1
        span = rotor_casing_geometry[3][disk][2] - 1
        if not rotor_casing_geometry[2][span][node][3]:
            if node == 0: # check if the rigid body is placed as a first element in the span
                node = 1
                first_node = 1 
            inner_diameter = rotor_casing_geometry[2][span][node][2][1]
            outer_diameter = rotor_casing_geometry[2][span][node][2][0]
        else:
            if node == 0: # check if the rigid body is placed as a first element in the span
                node = 1
                first_node = 1 
            inner_diameter = rotor_casing_geometry[2][span][node][3][1]
            outer_diameter = rotor_casing_geometry[2][span][node][3][0]
        # plot at the middle of outer and inner diameter
        if first_node == 1: # check if the rigid body is placed as a first element in the span
            node = 0
        ax.plot([dof_pos[node]], 0.5*(inner_diameter + outer_diameter), plot_color+str("D"))

    
    
    # Bearing Plot
    if len(rotor_casing_geometry) > 4:
        for bearing in range(len(rotor_casing_geometry[4])):
            if not rotor_casing_geometry[4][bearing][2]: # spring connects a span to ground
                span = rotor_casing_geometry[4][bearing][1][1]-1
                elem = rotor_casing_geometry[4][bearing][1][2]-1
                if rotor_casing_geometry[4][bearing][1][0] == "rotor":
                    if elem >= (len(rotor_casing_geometry[0][span])-1):
                        elem = elem - 1
                elif rotor_casing_geometry[4][bearing][1][0] == "casing": 
                    if elem >= (len(rotor_casing_geometry[2][span])-1):
                        elem = elem - 1   
                if rotor_casing_geometry[4][bearing][1][0] == "rotor":
                    dof_pos = span_axial_position(rotor_casing_geometry, "rotor", span)
                    y_pos = rotor_casing_geometry[0][span][elem+1][2][0]
                elif rotor_casing_geometry[4][bearing][1][0] == "casing":
                    dof_pos = span_axial_position(rotor_casing_geometry, "casing", span)
                    y_pos = rotor_casing_geometry[2][span][elem+1][2][0]
                x_pos = dof_pos[rotor_casing_geometry[4][bearing][1][2]-1]          
                xs, ys = visualise_bearings(x_pos, y_pos)
                ax.plot(xs, ys, c='k', lw=2)
            else: # spring connects a span to span
                span_1 = rotor_casing_geometry[4][bearing][1][1]-1
                elem_1 = rotor_casing_geometry[4][bearing][1][2]-1
                span_2 = rotor_casing_geometry[4][bearing][2][1]-1
                elem_2 = rotor_casing_geometry[4][bearing][2][2]-1
                if rotor_casing_geometry[4][bearing][1][0] == "rotor":
                    dof_pos = span_axial_position(rotor_casing_geometry, "rotor", span_1)
                    # identify if the element is at the last node
                    if elem_1 >= (len(rotor_casing_geometry[0][span_1])-1):
                        elem_1 = elem_1 - 1
                else:
                    dof_pos = span_axial_position(rotor_casing_geometry, "casing", span_1)
                    # identify if the element is at the last node
                    if elem_1 >= (len(rotor_casing_geometry[2][span_1])-1):
                        elem_1 = elem_1 - 1

                # Identify x position
                x_pos = dof_pos[rotor_casing_geometry[4][bearing][1][2]-1]
                
                if rotor_casing_geometry[4][bearing][2][0] == "rotor":
                    dof_pos = span_axial_position(rotor_casing_geometry, "rotor", span_2)
                    # identify if the element is at the last node
                    if elem_2 >= (len(rotor_casing_geometry[0][span_2])-1):
                        elem_2 = elem_2 - 1
                else:
                    dof_pos = span_axial_position(rotor_casing_geometry, "casing", span_2)
                    # identify if the element is at the last node
                    if elem_2 >= (len(rotor_casing_geometry[2][span_2])-1):
                        elem_2 = elem_2 - 1

                # identify position 1
                if elem_1 == 0:
                    elem_1_ = 1
                else:
                    elem_1_ = elem_1
                if rotor_casing_geometry[4][bearing][1][0] == "rotor":
                    if rotor_casing_geometry[0][span_1][elem_1_][0][0] is "Beam":
                        y_pos_1 = (rotor_casing_geometry[0][span_1][elem_1+1][2][1] + rotor_casing_geometry[0][span_1][elem_1+1][2][0])/2  
                    else:
                        y_pos_1 = 0
                if rotor_casing_geometry[4][bearing][1][0] == "casing":
                    if rotor_casing_geometry[2][span_1][elem_1_][0][0] is "Beam":
                        y_pos_1 = (rotor_casing_geometry[2][span_1][elem_1+1][2][1] +  rotor_casing_geometry[2][span_1][elem_1+1][2][0])/2 
                    else:
                        y_pos_1 = 0
                # identify position 2
                if elem_2 == 0:
                    elem_2_ = 1
                else:
                    elem_2_ = elem_2
                if rotor_casing_geometry[4][bearing][2][0] == "rotor":
                    if rotor_casing_geometry[0][span_2][elem_2_][0][0] is "Beam":
                        y_pos_2 = (rotor_casing_geometry[0][span_2][elem_2+1][2][0] + rotor_casing_geometry[0][span_2][elem_2+1][2][1])/2
                    else:
                        y_pos_2 = 0
                if rotor_casing_geometry[4][bearing][2][0] == "casing":
                    if rotor_casing_geometry[2][span_2][elem_2_][0][0] is "Beam":
                        y_pos_2 = (rotor_casing_geometry[2][span_2][elem_2+1][2][0] + rotor_casing_geometry[2][span_2][elem_2+1][2][1])/2
                    else:
                        y_pos_2 = 0
                xs, ys = visualise_bearings(x_pos, y_pos_1, y_pos_2)
                ax.plot(xs, ys, c='m', lw=2)
           
    return
        
        

#%%    
def middle_line(rotor_casing_geometry):
    """
    Find the most left and most right position to identify the limits of the middle line.
   
    Input: 
        rotor_casing_geometry = [rotor_shaft, rotor_blade_disk, casing_flex, casing_rigid, \
                         rotor_bearing, span_coupling, rotor_unbalance]
        
    Output:
        l_tot: array with two floats [left, right]  
    """
    
    nspans_rotor = len(rotor_casing_geometry[0])
    nspans_casing = len(rotor_casing_geometry[2])

    # most left position of rotor
    rotor_span_left = []
    casing_span_left = []

    for span in range(nspans_rotor):
        rotor_span_left.append(rotor_casing_geometry[0][span][0][1])
    if nspans_casing > 0:
        for span in range(nspans_casing):
            casing_span_left.append(rotor_casing_geometry[2][span][0][1])
        
    if nspans_casing > 0:
        rotor_casing_left = rotor_span_left + casing_span_left
    else:
        rotor_casing_left = rotor_span_left
        
    middle_line_left = min (rotor_casing_left)
    
    # most right position of rotor
    rotor_span_length = []
    rotor_span_right = []
    for span in range(nspans_rotor):
        ndofs_rotor = len(rotor_casing_geometry[0][span])-1
        rotor_length = 0
        for dof in range(ndofs_rotor):
            rotor_length += rotor_casing_geometry[0][span][dof+1][1]
        rotor_span_length.append(rotor_length)
        rotor_span_right.append(rotor_length + rotor_casing_geometry[0][span][0][1])
        
    if nspans_casing > 0:
        casing_span_length = []
        casing_span_right = []
        for span in range(nspans_casing):
            ndofs_casing = len(rotor_casing_geometry[2][span])-1
            casing_length = 0
            for dof in range(ndofs_casing):
                casing_length += rotor_casing_geometry[2][span][dof+1][1]
            casing_span_length.append(casing_length)
            casing_span_right.append(casing_length + rotor_casing_geometry[2][span][0][1])
            
    
    if nspans_casing > 0:
        rotor_casing_right = rotor_span_right + casing_span_right
    else:
        rotor_casing_right = rotor_span_right
    
    middle_line_right = max (rotor_casing_right)
    
    l_tot = [middle_line_left, middle_line_right]
    
    return l_tot


#%%
def span_axial_position(rotor_casing_geometry, structure, span): 
    """
    This function allows the calculation of the axial position of the DOFs
    of the selected span, rotor or casing
    
    Input:
        - rotor_casing_geometry
        - structure: "rotor" or "casing"
        - span
    """
    
    if structure == "rotor":
        spanstart = rotor_casing_geometry[0][span][0][1]
        # list of dof axial position
        dof_pos = []
        dof_pos_tmp = spanstart
        for dof in range(1,len(rotor_casing_geometry[0][span])):
            dof_pos_tmp += rotor_casing_geometry[0][span][dof][1]
            dof_pos.append(dof_pos_tmp)
        dof_pos = [spanstart] + dof_pos
    else:
        spanstart = rotor_casing_geometry[2][span][0][1]
        # list of dof axial position
        dof_pos = []
        dof_pos_tmp = spanstart
        for dof in range(1,len(rotor_casing_geometry[2][span])):
            dof_pos_tmp += rotor_casing_geometry[2][span][dof][1]
            dof_pos.append(dof_pos_tmp)
        dof_pos = [spanstart] + dof_pos
    
    return dof_pos


#%%
def visualise_bearings(x, y, *y_pos_2, y_min=-0.2):
    """
    Function to visualize the bearings

    Parameters
    ----------
    x : TYPE
        DESCRIPTION.
    y : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """

    
    #fig, ax = plt.subplots()
    if not y_pos_2:
        L = (y-y_min)*100
    else:
        L = (y-y_pos_2[0])*100
    theta = 0
    # Spring turn radius, number of turns
    rs, ns = 0.01, 1500
    # Number of data points for the helix
    Ns = 500
    # We don't draw coils all the way to the end of the pendulum:
    # pad a bit from the anchor and from the bob by these number of points
    ipad1, ipad2 = 250, 150
    w = np.linspace(0, L/100, Ns) - y 
    # Set up the helix along the x-axis ...
    xp = np.zeros(Ns) - x
    xp[ipad1:-ipad2] = rs * np.sin(2*np.pi * ns * w[ipad1:-ipad2] / L) - x
    # ... then rotate it to align with  the pendulum and plot.
    R = np.array([[np.cos(theta), -np.sin(theta)], 
                  [np.sin(theta), np.cos(theta)]])
    xs, ys = - R @ np.vstack((xp, w))
    #ax.plot(xs, ys, c='b', lw=2)
    
    
    return xs, ys

#%%
# def rotor_casing_3D(rotor_casing_geometry, ax_eq):
#     """
#     This function is used to represent a 3D rotor

#     Returns
#     -------
#     None.

#     """
#     ########################################################################
#     nspans_rotor = len(rotor_casing_geometry[0])
#     nspans_casing = len(rotor_casing_geometry[2])   
#     # Plot   
#     fig =  plt.figure()
#     ax = fig.add_subplot(111, projection = "3d")
#     # if ax_eq == 1:
#     #     ax.axis('equal')
#     # ylim
#     #plt.ylim([-1.5*np.max(outer_diameter), 1.5*np.max(outer_diameter_disk)]) 
    
#     # middle line        
#     l_tot = middle_line(rotor_casing_geometry)    
#     ax.plot(l_tot, np.zeros(len(l_tot)), 0, 'k-.')
    
#     # points in circumference
#     n_points = 36
#     circ_point_y = np.cos(np.linspace(0,2*np.pi,n_points))
#     circ_point_z = np.sin(np.linspace(0,2*np.pi,n_points))
    
#     # Shaft Plot
#     for span in range (nspans_rotor):
#         if rotor_casing_geometry[0][span][0][4][0] == "hp":
#             plot_color = "r"
#         elif rotor_casing_geometry[0][span][0][4][0] == "lp":
#             plot_color = "g"
#         dof_pos = span_axial_position(rotor_casing_geometry, "rotor", span)
#         for elem in range(1, len(rotor_casing_geometry[0][span])):
#             inner_diameter = rotor_casing_geometry[0][span][elem][2]
#             outer_diameter = rotor_casing_geometry[0][span][elem][1]
#             #left_dof = left_dof + 
#             # inner diameter shaft
#             ax.plot_surface([dof_pos[elem-1]]*n_points, inner_diameter*circ_point_y, inner_diameter*circ_point_z, plot_color)
#             ax.plot_surface([dof_pos[elem]]*n_points, inner_diameter*circ_point_y, inner_diameter*circ_point_z, plot_color)
#             #outer diameter shaft
#             ax.plot([dof_pos[elem-1]]*n_points, outer_diameter*circ_point_y, outer_diameter*circ_point_z, plot_color)
#             ax.plot([dof_pos[elem]]*n_points, outer_diameter*circ_point_y, outer_diameter*circ_point_z, plot_color)
#             # axial connection of elements
#             ax.plot([dof_pos[elem-1], dof_pos[elem]], [inner_diameter, inner_diameter], [inner_diameter, inner_diameter], 
#                     plot_color)
#             ax.plot([dof_pos[elem]]*n_points, inner_diameter*circ_point_y, inner_diameter*circ_point_z, plot_color)
#         # plot element vertical lines shaft
#         for elem in range(1,len(rotor_casing_geometry[0][span])+1):
#             ax.plot([dof_pos[elem-1],dof_pos[elem-1]], [inner_diameter, outer_diameter], plot_color)
#             ax.plot([dof_pos[elem-1],dof_pos[elem-1]], [inner_diameter, outer_diameter], plot_color)
 
#     # Disks/Blade/Rigid body rotor plot
#     for disk in range(len(rotor_casing_geometry[1])):
#         if rotor_casing_geometry[1][disk][1] == "hp":
#             plot_color = "g"
#         elif rotor_casing_geometry[1][disk][1] == "lp":
#             plot_color = "r"
#         if rotor_casing_geometry[1][disk][0] == "blade" or rotor_casing_geometry[1][disk][0] == "disk":
#             dof_pos = span_axial_position(rotor_casing_geometry, "rotor", rotor_casing_geometry[1][disk][2]-1)
#             inner_diameter = rotor_casing_geometry[1][disk][7]
#             outer_diameter = rotor_casing_geometry[1][disk][6]
#             half_length = rotor_casing_geometry[1][disk][5]/2
#             node = rotor_casing_geometry[1][disk][3]
#             # inner diameter shaft
#             ax.plot([dof_pos[node-1] - half_length, dof_pos[node-1] + half_length], [inner_diameter, inner_diameter], plot_color)
#             #outer diameter shaft
#             ax.plot([dof_pos[node-1] - half_length, dof_pos[node-1] + half_length], [outer_diameter, outer_diameter], plot_color)
#             # plot element vertical lines shaft
#             ax.plot([dof_pos[node-1] - half_length, dof_pos[node-1] - half_length], [inner_diameter, outer_diameter], plot_color)
#             ax.plot([dof_pos[node-1] + half_length, dof_pos[node-1] + half_length], [inner_diameter, outer_diameter], plot_color)

#         else:
#             pass
    
#     return

#%%
# nspans_rotor = len(rotor_casing_geometry[0])
# nspans_casing = len(rotor_casing_geometry[2])   
# # Plot   
# fig =  plt.figure()
# ax = fig.add_subplot(111, projection = "3d")
# # if ax_eq == 1:
# #     ax.axis('equal')
# # ylim
# #plt.ylim([-1.5*np.max(outer_diameter), 1.5*np.max(outer_diameter_disk)]) 

# # middle line        
# l_tot = middle_line(rotor_casing_geometry)    
# ax.plot(l_tot, np.zeros(len(l_tot)), 0, 'k-.')

# # points in circumference
# n_points = 36
# circ_point_y = np.cos(np.linspace(0,2*np.pi,n_points))
# circ_point_z = np.sin(np.linspace(0,2*np.pi,n_points))

# # Shaft Plot
# for span in range (nspans_rotor):
#     if rotor_casing_geometry[0][span][0][4][0] == "hp":
#         plot_color = "r"
#     elif rotor_casing_geometry[0][span][0][4][0] == "lp":
#         plot_color = "g"
#     dof_pos = span_axial_position(rotor_casing_geometry, "rotor", span)
#     for elem in range(1, len(rotor_casing_geometry[0][span])):
#         inner_diameter = rotor_casing_geometry[0][span][elem][2]
#         outer_diameter = rotor_casing_geometry[0][span][elem][1]
#         #left_dof = left_dof + 
#         # inner diameter shaft
#         ax.plot_surface([dof_pos[elem-1]]*n_points, inner_diameter*circ_point_y, inner_diameter*circ_point_z, plot_color)
#         ax.plot_surface([dof_pos[elem]]*n_points, inner_diameter*circ_point_y, inner_diameter*circ_point_z, plot_color)
#         #outer diameter shaft
#         ax.plot([dof_pos[elem-1]]*n_points, outer_diameter*circ_point_y, outer_diameter*circ_point_z, plot_color)
#         ax.plot([dof_pos[elem]]*n_points, outer_diameter*circ_point_y, outer_diameter*circ_point_z, plot_color)
#         # axial connection of elements
#         ax.plot([dof_pos[elem-1], dof_pos[elem]], [inner_diameter, inner_diameter], [inner_diameter, inner_diameter], 
#                 plot_color)
#         ax.plot([dof_pos[elem]]*n_points, inner_diameter*circ_point_y, inner_diameter*circ_point_z, plot_color)
