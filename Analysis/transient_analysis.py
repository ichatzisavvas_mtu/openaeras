"""
This modules `Transient Analysis` is used to performed a time-tranisent analysis

Several time intergration solver can be used depending on the problem at hand
"""


import numpy as np
import matplotlib.pyplot as plt
from openAERAS.Bearings.Radial_Bearings.linear_bearings import calcs_linear_bearing
from scipy import interpolate





#%%
def calcs_transient_analysis (M, C, G, K, rotor_casing_geometry, 
                                                 tr_ax_eq, tr_tmax, omega_trans, n_p_e):
    """
    Main function to perform the transien Analysis

    Parameters
    ----------
    M : TYPE
        DESCRIPTION.
    C : TYPE
        DESCRIPTION.
    G : TYPE
        DESCRIPTION.
    K : TYPE
        DESCRIPTION.
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    tr_ax_eq : TYPE
        DESCRIPTION.
    tr_tmax : TYPE
        DESCRIPTION.
    omega_trans : TYPE
        DESCRIPTION.
    n_p_e : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """

    
    # Constant coefficient bearings
    # Number of bearings
    n_bearings = len(rotor_casing_geometry[4])
    # Check if there is speed dependent bearing
    n_speed_dep = []
    for bearing in range(n_bearings):
        if rotor_casing_geometry[4][bearing][0] == "linear_bearing_vc":
            n_speed_dep.append(bearing)
    # Incoorporate of bearings with constant coefficient    
    if not n_speed_dep:
        C_b, K_b = calcs_linear_bearing(rotor_casing_geometry, C, K)
    
    
    # create main state space matrix
    ndof = len (M)
    AA = np.zeros([2*ndof,2*ndof])
    A11 =  np.zeros([ndof,ndof])
    A12 =  np.eye(ndof,ndof)
    A21 = -np.linalg.inv(M).dot(K_b)
    A22 = -np.linalg.inv(M).dot(C_b)
    AA[0:ndof, 0:ndof] = A11
    AA[ndof:2*ndof, 0:ndof] = A12
    AA[0:ndof, ndof:2*ndof] = A21
    AA[ndof:2*ndof, ndof:2*ndof] = A22
    
    AA_G = np.zeros([2*ndof,2*ndof])
    A11 =  np.zeros([ndof,ndof])
    A12 =  np.zeros([ndof,ndof])
    A21 =  np.zeros([ndof,ndof])
    A22 = -np.linalg.inv(M).dot(G)
    AA[0:ndof, 0:ndof] = A11
    AA[ndof:2*ndof, 0:ndof] = A12
    AA[0:ndof, ndof:2*ndof] = A21
    AA_G[ndof:2*ndof, ndof:2*ndof] = A22
    
    # rotational speed
    omega_lp_vec, omega_hp_vec, omega_ip_vec = rotational_speed_vec (rotor_casing_geometry)
    omega_alpha_theta = transient_rotor_speed(omega_trans, tr_tmax)
    
    # unbalance    
    f_un_all, ndof = unb_vec (rotor_casing_geometry, omega_trans)

    # inverted mass matrix
    M_inv = np.linalg.inv(M)
    
    # time integration
    y0 = np.zeros([2*ndof])
    sol = solve_ivp(function_to_integrate,[0,1],y0,args = [AA, AA_G, rotor_casing_geometry, M_inv, f_un_all, ndof, 
                                                        omega_lp_vec, omega_hp_vec, omega_ip_vec, 
                                                        omega_alpha_theta])
    
    
    return sol
    
#%%    
def function_to_integrate(t, y, AA, AA_G, rotor_casing_geometry, M_inv, f_un_all, ndof, 
                                                        omega_lp_vec, omega_hp_vec, omega_ip_vec, 
                                                        omega_alpha_theta):
    """
    Give the function to be intrated
    

    Parameters
    ----------
    t : TYPE
        DESCRIPTION.
    y : TYPE
        DESCRIPTION.
    M : TYPE
        DESCRIPTION.
    C : TYPE
        DESCRIPTION.
    G : TYPE
        DESCRIPTION.
    K : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    #theta = 
    #unbalance = 0.5 * alpha_lp(t)
    omega_tot = omega_alpha_theta[0](t)*omega_lp_vec + omega_alpha_theta[1](t)*omega_hp_vec + omega_alpha_theta[2](t)*omega_ip_vec
    omega_tot_vec = np.zeros([2*len(M_inv),1])
    omega_tot_vec [0:len(M_inv)] = np.zeros([len(M_inv),1])
    omega_tot_vec [len(M_inv):2*len(M_inv) ] = omega_tot
    
    t_slots = list(np.sort(f_un_all[0][3] + [t]))
    time_slot = t_slots.index(t)

    f_unb = trans_resp_unb (rotor_casing_geometry, time_slot, f_un_all, ndof)
    
    force_vec = M_inv * f_unb * \
                    (omega_alpha_theta[0](t)**2*omega_lp_vec + omega_alpha_theta[1](t)**2*omega_hp_vec + omega_alpha_theta[2](t)**2*omega_ip_vec + \
                         omega_alpha_theta[3](t)*omega_lp_vec + omega_alpha_theta[4](t)*omega_hp_vec + omega_alpha_theta[5](t)*omega_ip_vec) * \
                     np.exp(1j)
    force_tot_vec = np.zeros([2*len(M_inv),1])
    force_tot_vec [0:len(M_inv)] = np.zeros([len(M_inv),1])
    force_tot_vec [len(M_inv):2*len(M_inv) ] = force_vec
    
    dydt = AA.dot(y) + AA_G.dot(y).dot(omega_tot_vec) + np.real(force_tot_vec)
    
    return dydt
    
#%%
def unb_vec (rotor_casing_geometry, omega_trans):
    """
    Creation of the unbalance force vector
    The unbalance vector f_un = mr [gmm]

    Parameters
    ----------
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    r_dofs : TYPE
        DESCRIPTION.
    r_dof_span : TYPE
        DESCRIPTION.
    r_dof_span_start : TYPE
        DESCRIPTION.

    Returns
    -------
    f_un : TYPE
        DESCRIPTION.

    """
    
    # rotor DoFs
    r_dofs, r_dof_span, r_dof_span_start = rotor_span_dof(rotor_casing_geometry)
    
    # casing DoFs
    c_dofs, c_dof_span, c_dof_span_start = casing_span_dof(rotor_casing_geometry)
    
    # Total DoF
    ndof = r_dofs + c_dofs
    
    # Unbalance sign definition
    lp_unb_sign = 0
    hp_unb_sign = 0
    ip_unb_sign = 0
    
    for omega_rotor in range(len(omega_trans)):
        if omega_trans[omega_rotor][0][0] == "lp":
            if omega_trans[omega_rotor][0][1] == "positive_z":
                lp_unb_sign = 1
            elif omega_trans[omega_rotor][0][1] == "negative_z":
                lp_unb_sign = -1
        elif omega_trans[omega_rotor][0][0] == "hp":
            if omega_trans[omega_rotor][0][1] == "positive_z":
                hp_unb_sign = 1
            elif omega_trans[omega_rotor][0][1] == "negative_z":
                hp_unb_sign = -1
        elif omega_trans[omega_rotor][0][0] == "ip":
            if omega_trans[omega_rotor][0][1] == "positive_z":
                ip_unb_sign = 1
            elif omega_trans[omega_rotor][0][1] == "negative_z":
                ip_unb_sign = -1
    unb_sign = [lp_unb_sign, hp_unb_sign, ip_unb_sign]           
            
    # check where the unbalance is applied
    hp_unb = []
    lp_unb = []
    ip_unb = []
    for n_unb in range(len(rotor_casing_geometry[6][0])):
        if rotor_casing_geometry[6][0][n_unb][0] == "hp":
            hp_unb.append(n_unb)
        elif rotor_casing_geometry[6][0][n_unb][0] == "lp":
            lp_unb.append(n_unb)
        elif rotor_casing_geometry[6][0][n_unb][0] == "ip":
            ip_unb.append(n_unb)
    if hp_unb:
        print ("Unbalance is applied at hp")
    if lp_unb:
        print ("Unbalance is applied at lp")
    if ip_unb:
        print ("Unbalance is applied at ip")
        

    # Unbalance vector in state space (including casing if applicable)
    #unb_vec = np.zeros((2*ndof,1)) + 1j*np.zeros ((2*ndof,1))

        
    # Unbalance Position Detection
    f_un_pos = []
    f_un_times = []
    for n_unb in range(len(rotor_casing_geometry[6][0])):
        #DoF where the unbalance is placed
        f_un_pos.append(r_dof_span_start[rotor_casing_geometry[6][0][n_unb][1]-1] + 6*(rotor_casing_geometry[6][0][n_unb][2]-1))
        for unb_inst in range(len(rotor_casing_geometry[6][0][n_unb][3])):
            f_un_times.append(rotor_casing_geometry[6][0][n_unb][3][unb_inst][0])
    f_un_times = list(set(f_un_times))
    

    f_un_all = [] 
    for n_unb in range(len(rotor_casing_geometry[6][0])):
        if rotor_casing_geometry[6][0][n_unb][0] == "hp":
            unb_sign = hp_unb_sign
        elif rotor_casing_geometry[6][0][n_unb][0] == "lp":
            unb_sign = lp_unb_sign
        elif rotor_casing_geometry[6][0][n_unb][0] == "ip":
            unb_sign = ip_unb_sign
        unb_mag = []
        unb_pha = []
        unb_time = []
        for unb_inst in range(len(rotor_casing_geometry[6][0][n_unb][3])):
            unb_mag.append(rotor_casing_geometry[6][0][n_unb][3][unb_inst][1])
            unb_pha.append(rotor_casing_geometry[6][0][n_unb][3][unb_inst][2])
            unb_time.append(rotor_casing_geometry[6][0][n_unb][3][unb_inst][0])
    
        f_un_all.append([f_un_pos[n_unb], unb_mag, unb_pha, unb_time, unb_sign])
    
    return f_un_all, ndof
#%%
def trans_resp_unb (rotor_casing_geometry, time_slot, f_un_all, ndof):
    """
    Creation of the unbalance force vector
    The unbalance vector f_un = mr [gmm]

    Parameters
    ----------
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    r_dofs : TYPE
        DESCRIPTION.
    r_dof_span : TYPE
        DESCRIPTION.
    r_dof_span_start : TYPE
        DESCRIPTION.

    Returns
    -------
    f_un : TYPE
        DESCRIPTION.

    """
    
    # Unbalance vector in state space (including casing if applicable)
    #unb_vec = np.zeros((2*ndof,1)) + 1j*np.zeros ((2*ndof,1))

        
    # Unbalance Vector for rotors only
    f_un = np.zeros ((ndof,1)) + 1j*np.zeros ((ndof,1))
    for n_unb in range(len(rotor_casing_geometry[6][0])):
        f_un[f_un_all[n_unb][0]] = f_un_all[n_unb][1][time_slot] * np.exp(1j*f_un_all[n_unb][1][time_slot])
        f_un[f_un_all[n_unb][0]+1] = -1j*f_un_all[n_unb][1][time_slot]  * np.exp(1j*f_un_all[n_unb][1][time_slot]) * f_un_all[n_unb][4]
    
  
    return f_un


#%%
def rotational_speed_vec (rotor_casing_geometry):
    """
    

    Returns
    -------
    None.

    """
    
    # rotor DoFs
    r_dofs, r_dof_span, r_dof_span_start = rotor_span_dof(rotor_casing_geometry)

    # casing DoFs
    c_dofs, c_dof_span, c_dof_span_start = casing_span_dof(rotor_casing_geometry)
    
    # Total DoF
    ndof = r_dofs + c_dofs
    
    omega_lp_vec = np.zeros ((ndof,1)) 
    omega_hp_vec = np.zeros ((ndof,1)) 
    omega_ip_vec = np.zeros ((ndof,1)) 
    for span in range(len(rotor_casing_geometry[0])):
        if rotor_casing_geometry[0][span][0][4][0] == "lp":            
            omega_lp_vec[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]] = 1
        elif rotor_casing_geometry[0][span][0][4][0] == "hp":            
            omega_hp_vec[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]] = 1
        elif rotor_casing_geometry[0][span][0][4][0] == "ip":            
            omega_ip_vec[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]] = 1
            
            
    return omega_lp_vec, omega_hp_vec, omega_ip_vec
            
                
                
#%%
def transient_rotor_speed(omega_trans, tr_tmax):
    """
    

    Returns
    -------
    None.

    """               
    omega_lp_time = []
    omega_lp_speed = []
    omega_hp_time = []
    omega_hp_speed = []
    omega_ip_time = []
    omega_ip_speed = []
    omega_lp = []
    omega_hp = []
    omega_ip = []
    alpha_lp_time = []
    alpha_lp_speed = []
    alpha_hp_time = []
    alpha_hp_speed = []
    alpha_ip_time = []
    alpha_ip_speed = []
    alpha_lp = []
    alpha_hp = []
    alpha_ip = []

    
    
    for rotor in range (len(omega_trans)): # number of terms in omega (identical to number of rotors)
        for rot_speed in range(len(omega_trans[rotor][1:])):
            if omega_trans[rotor][0][0] == "lp":
                omega_lp_time.append(omega_trans[rotor][rot_speed+1][0])
                omega_lp_speed.append(omega_trans[rotor][rot_speed+1][1]) 
            elif omega_trans[rotor][0][0] == "hp":
                omega_hp_time.append(omega_trans[rotor][rot_speed+1][0])
                omega_hp_speed.append(omega_trans[rotor][rot_speed+1][1])  
            elif omega_trans[rotor][0][0] == "ip":
                omega_ip_time.append(omega_trans[rotor][rot_speed+1][0])
                omega_ip_speed.append(omega_trans[rotor][rot_speed+1][1]) 
        for rot_speed in range(len(omega_trans[rotor][1:-1])):
            if omega_trans[rotor][0][0] == "lp":
                alpha_lp_time.append(omega_trans[rotor][rot_speed+2][0]-omega_trans[rotor][rot_speed+1][0])
                alpha_lp_speed.append((omega_trans[rotor][rot_speed+2][1]-omega_trans[rotor][rot_speed+1][1]) / omega_trans[rotor][rot_speed+2][0]-omega_trans[rotor][rot_speed+1][0]) 
            elif omega_trans[rotor][0][0] == "hp":
                alpha_hp_time.append(omega_trans[rotor][rot_speed+2][0]-omega_trans[rotor][rot_speed+1][0])
                alpha_hp_speed.append((omega_trans[rotor][rot_speed+2][1]-omega_trans[rotor][rot_speed+1][1]) / omega_trans[rotor][rot_speed+2][0]-omega_trans[rotor][rot_speed+1][0]) 
            elif omega_trans[rotor][0][0] == "ip":
                alpha_ip_time.append(omega_trans[rotor][rot_speed+2][0]-omega_trans[rotor][rot_speed+1][0])
                alpha_ip_speed.append((omega_trans[rotor][rot_speed+2][1]-omega_trans[rotor][rot_speed+1][1]) / omega_trans[rotor][rot_speed+2][0]-omega_trans[rotor][rot_speed+1][0]) 
                
    #max_time = np.max([omega_lp_speed[0], omega_hp_speed[0], omega_ip_speed[0]])
    if omega_lp_time:
        omega_lp = interpolate.interp1d(omega_lp_time, omega_lp_speed) 
        alpha_lp = interpolate.interp1d(omega_lp_time, alpha_lp_speed) 
    else:
        omega_lp = interpolate.interp1d([0, tr_tmax], [0, 0])
        alpha_lp = interpolate.interp1d([0, tr_tmax], [0, 0])
    if omega_hp_time:   
        omega_hp = interpolate.interp1d(omega_hp_time, omega_hp_speed)
        alpha_hp = interpolate.interp1d(omega_hp_time, alpha_hp_speed) 
    else:
        omega_hp = interpolate.interp1d([0, tr_tmax], [0, 0])
        alpha_hp = interpolate.interp1d([0, tr_tmax], [0, 0])
    if omega_ip_time:  
        omega_ip = interpolate.interp1d(omega_ip_time, omega_ip_speed)  
        alpha_ip = interpolate.interp1d(omega_ip_time, alpha_ip_speed) 
    else:
        omega_ip = interpolate.interp1d([0, tr_tmax], [0, 0])
        alpha_ip = interpolate.interp1d([0, tr_tmax], [0, 0])

    exp_vec = [omega_lp, omega_hp, omega_ip, alpha_lp, alpha_hp, alpha_ip]
    return exp_vec
                

    
