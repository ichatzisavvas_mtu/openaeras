"""
Example of how to include a flexibility element in a shaft that consists of beam elements.

author: Lukas Braun
"""

import numpy as np
from openAERAS.Analysis import eigenfrequency_analysis, frequency_response, transient_analysis
from openAERAS.Structure_Modeling import beam_assembly
from openAERAS.Graphical_Interface import non_graphical_interface


##############################################################################
# Flexibility Element Definition

# file name that correspondents to the input file name of the flexibility element (see 3D_example.py)
filename = "flexibility_element_example_input"

# read material and geometry from input file
data = np.genfromtxt("%s.txt" % filename, skip_header=5, delimiter=",")
E = data[0]  # Young's modulus [N/m^2]
nu = data[1]  # Poisson's ratio [-]
rho = data[3]  # Density [kg/mm^3]
shaft_od_left = data[18]*2  # Shaft Outer Diameter [m]
shaft_id_left = data[9]*2  # Shaft Inner Diameter [m]
shaft_od_right = data[15]*2  # Shaft Outer Diameter [m]
shaft_id_right = data[12]*2  # Shaft Inner Diameter [m]
length = (data[14] - data[11])

# read flexibility coefficients
parameters = np.genfromtxt("%s_coefficients.txt" % filename, delimiter=",")
alpha = parameters[0]
beta = parameters[1]
gamma = parameters[2]
delta = parameters[3]
epsilon = parameters[4]

# mass and moments of inertia for a hollow cylinder
mass = rho*length/4*np.pi*(((shaft_od_left+shaft_od_right)/2)**2-((shaft_id_left+shaft_id_right)/2)**2) 
Id = mass/2/4*(((shaft_od_left+shaft_od_right)/2)**2+((shaft_id_left+shaft_id_right)/2)**2) 
Ip = mass/12*(3/4*(((shaft_od_left+shaft_od_right)/2)**2+((shaft_id_left+shaft_id_right)/2)**2)+length**2)


##############################################################################
# Beam elements definition
E = 2.11e11  # Young's modulus [N/m^2]
nu = 0.29926  # Poisson's ratio [-]
rho = 7810  # Density [kg/m^3]
damping_factor = 0    # damping in shaft
damping_type = 'stiffness_proportional'  # type of damping
shaft_od_1 = 0.05  # Shaft Outer Diameter [m]
shaft_id_1 = 0.01  # Shaft Outer Diameter [m]
rotor_start = {"hp_1": 0.0}  # starting point of rotors [mm]
rotor_speed_ratio = [["hp", [1], 'positive_z']]

##############################################################################
# Rotor & Shaft Definition
rotor_shaft = ([
 [[1, rotor_start["hp_1"], damping_type, damping_factor, rotor_speed_ratio[0]],
  [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
  [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
       
  # choose between beam element or flexibility element
  [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
  # [["Static_Red"], length, [shaft_od_left, shaft_id_left], [], rho, E, nu, [alpha, beta, gamma, delta, epsilon], []],
  # [["Static_Red"], length, [shaft_od_left, shaft_id_left], [], rho, E, nu, [alpha, beta, gamma, delta, epsilon], [mass, Ip, Id]],
       
  [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
  [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu],
  [["Beam"], 0.25, [shaft_od_1, shaft_id_1], [], rho, E, nu]]
])

##############################################################################
# Disk Definition
rotor_blade_disk = ([
  ["disk", "hp", 1, 3, 7810, 0.07, 0.28, 0.05],
  ["disk", "hp", 1, 5, 7810, 0.07, 0.35, 0.05]
])

##############################################################################
# Casing Definition
casing_flex = ([])
casing_rigid = ([])

##############################################################################
# Bearing Definition
rotor_casing_bearing = ([
  ["linear_bearing_cc", ["rotor", 1, 1], [], [1e6, 1e6, 1e10, 0, 0, 1e10], [0, 0, 0, 0, 0, 0]],
  ["linear_bearing_cc", ["rotor", 1, 7], [], [1e6, 1e6, 1e10, 0, 0, 1e10], [0, 0, 0, 0, 0, 0]]
])

##############################################################################
# Span Coupling Definition
span_coupling = ([]) 
    
##############################################################################
# Unbalance
rotor_unbalance = ([])   

##############################################################################
# Collecting rotor/bearing/casing/unbalance
rotor_casing_geometry = [rotor_shaft, rotor_blade_disk, casing_flex, casing_rigid,
                         rotor_casing_bearing, span_coupling, [rotor_unbalance]]

##############################################################################
# For eigenfrequency analysis
ea_ax_eq = 1  # axis equal
ea_fmax = 150  # [Hz] max eigenfrequncy for y limit
ea_omega_max = 4000*2*np.pi/60  # [rad/s] max eigenfrequncy for the calculation of the diagram
ea_n_camp = 10  # number of points for the Campbell diagram
ea_n_eigs = 30  # number of eigenfrequencies to plot
ea_modes_plot = [1, 2]  # modeshapes to plot
ea_modes_plot_speed = [1]  # the speed of modeshapes to plot
ea_mode_tracking = False  # True of False actication of mode tracking
eig_animate = True  # Animate mode shape
ea_n_p_e = 4  # 6 or 4 (axial and torsional removed)

##############################################################################
# Define Analysis Type
graphic_plot = True
eig_analysis = True

#%%
# Rotor/Casing Visualisation
if graphic_plot:
    non_graphical_interface.vis_rotor_casing(rotor_casing_geometry, ax_eq=1)

#%% 
# Model Creation
M, C, G, K = beam_assembly.beam_structure_assembly(rotor_casing_geometry, "false")

#%%
# Eigenfrequency Analyis
if eig_analysis:
    g_vector_s_i, g_value_, AA, g_values = eigenfrequency_analysis.calcs_eigenfrequency_analysis\
                                           (M, C, G, K, rotor_casing_geometry,
                                            ea_ax_eq, ea_omega_max, ea_fmax, ea_n_camp, ea_n_eigs,
                                            ea_modes_plot, ea_modes_plot_speed, ea_mode_tracking, eig_animate, ea_n_p_e)
