"""
This file creates the geometry parameters for a number of conical element samples by means of latin hypercube sampling.

author: Lukas Braun
"""

import numpy as np
import openAERAS.Optimization.g_latin_hypercube_sampling as g_lhs
    
# define the parameters for the current parameter study
#        nvar (number of independent variables)
#        n_samples (number of samples)
#        var_par (vector with min and max values for each independent variable)
#        g_dist (distribution of the geometry parameters)
#        plot_param (choose whether parameters should be plotted or not)
tb_params = g_lhs.g_lhs(5, 1000, [0, 60,  20, 100,  1, 6,  0.01, 100,  100, 100], "uniform", "hist")

# export the parameters in a csv file
a = np.asarray(np.transpose(tb_params))
np.savetxt("samples.csv", a, delimiter=",")
