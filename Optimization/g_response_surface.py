"""
This module is used to create and plot a response surface
"""

from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#%%
def g_resp_surf_calcs(model, lhs_vector, input_param_plot, lhs_in_out, samples_number, inp_vector):
    """
    
    Returns
    -------
    None.

    """
  
#    fig, ax  =plt.subplots()
#    ax.plot(tot_pre,'s')
#    ax.plot(y,'*')
    

    #g_resp_surf_calcs(s_model, lhs_vector, input_param_plot, lhs_in_out, samples_number)
    inp_param = []
    for ii in input_param_plot:
        inp_param.append(np.linspace(lhs_vector[ii][0],lhs_vector[ii][1],samples_number))

    param_1, param_2 = np.meshgrid(inp_param[0], inp_param[1])
    
    ss = inp_vector # the values that all the other variables have except the ones plotted
    g_res = np.zeros((samples_number,samples_number))
    for ii in range (samples_number):
        for jj in range (samples_number):
            ss[input_param_plot[0]] = param_1[ii,jj]
            ss[input_param_plot[1]] = param_2[ii,jj]
            xx = model.predict(np.array(ss).reshape(1,-1))
            g_res[ii,jj]= xx [0]
    
    fig, ax = plt.subplots()
    im_plot = ax.imshow (g_res, cmap = "viridis", aspect = "auto",\
               extent = [lhs_vector[input_param_plot[0]][0], lhs_vector[input_param_plot[0]][1], \
                                lhs_vector[input_param_plot[1]][1], lhs_vector[input_param_plot[1]][0]])
    cbar = fig.colorbar(im_plot)
    ax.set_xlabel(lhs_in_out[0][input_param_plot[0]-1])
    ax.set_ylabel(lhs_in_out[0][input_param_plot[0]-1])
    
    fig, ax = plt.subplots()
    CS = ax.contour (param_1, param_2, g_res, cmap = "viridis", aspect = "auto",\
               extent = [lhs_vector[input_param_plot[0]][0], lhs_vector[input_param_plot[0]][1], \
                                lhs_vector[input_param_plot[1]][1], lhs_vector[input_param_plot[1]][0]])
    ax.clabel(CS, inline=1, fontsize=10)
    return
