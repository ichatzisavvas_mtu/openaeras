import numpy as np
import os

    
#%%
def flex_coefficients_extract(filename):    
    """    
    The function extracts alpha, beta, gamma, delta and epsilon from the 
    static analysis result file

    author: Lukas Braun

    Parameters
    ----------------------------------------------------------------------
    filename: string
        name of the input file defined in the beginning of the routine.
    
    Returns
    ------------------------------------------------------------
    coefficients: vector with floats
        flexibility coefficients [alpha, beta, gamma, delta, epsilon].

    """
    
    # open the .dat result file, extract the lines of interest and write them
    # into a .txt file
    os.chdir("%s" % filename)
    linelist = open("%s.dat" % filename).readlines()
    f = open("%s.txt" % filename, "w")
    for line in linelist[int(3)]:
        f.write(line)
    for line in linelist[int(4)]:
        f.write(line)
    for line in linelist[int(9)]:  
        f.write(line)
    for line in linelist[int(13)]:  
        f.write(line)
    for line in linelist[int(19)]:      
        f.write(line)
    f.close()
    
    # open the .txt file and extract alpha, beta, gamma, delta and epsilon
    parameters = np.loadtxt("%s.txt" % filename, usecols=(1, 2, 3))
    alpha = parameters[0, 0]
    beta = parameters[2, 1]
    gamma = parameters[1, 1]
    delta = parameters[3, 2]
    epsilon = parameters[4, 2]
    
    os.chdir("..")
    
    f = open("%s_coefficients.txt" % filename, "w")
    f.truncate(0)
    f.write("%s,%s,%s,%s,%s" % (alpha, beta, gamma, delta, epsilon))
    
    coeffcients = [alpha, beta, gamma, delta, epsilon]
   
    return coeffcients
