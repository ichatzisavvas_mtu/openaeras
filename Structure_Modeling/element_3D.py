import numpy as np


#%%
def element_3D_stiff_matrix(filename, alpha, beta, gamma, delta, epsilon):
    
    """  
    The function builds the 12x12 stiffness matrix of a 3D element by means
    of alpha, beta, gamma, delta, epsilon and the element length.

    author: Lukas Braun
    
    Parameters
    ----------
    filename : string
        name of the 3D element input file.
    alpha, beta, gamma, delta, epsilon: float
        flexibility coefficients.
    
    Returns
    ----------
    Ke : array with floats
        Element stiffness matrix.

    """
    
    # get the element length from the input file
    header = 5
    data = np.genfromtxt("%s.txt" % filename, skip_header=header, delimiter=",")
    number_points = int((len(data)-8)/3)   
    length = data[10+3*number_points//2]
    
    # Flexibility matrix
    Flex = np.array([[alpha,    0,      0,      0,      gamma,  0],
                     [0,        alpha,  0,  -   gamma,  0,      0],
                     [0,        0,      delta,  0,      0,      0],
                     [0,        -gamma, 0,      beta,   0,      0],
                     [gamma,    0,      0,      0,      beta,   0],
                     [0,        0,      0,      0,      0,      epsilon]])
    
    # Transformation matrix (from f_left + T* f_right = 0)
    T = np.array([[1,       0,         0,       0,     0,       0],
                  [0,       1,         0,       0,     0,       0],
                  [0,       0,         1,       0,     0,       0],
                  [0,       -length,   0,       1,     0,       0],
                  [length,  0,         0,       0,     1,       0],
                  [0,       0,         0,       0,     0,       1]])
    
    # get the sub-matrices of the 12x12 element stiffness matrix
    K22 = np.linalg.inv(Flex)
    K12 = -np.dot(T, K22)
    K21 = -np.dot(K22, np.transpose(T))
    K111 = np.dot(T, K22)
    K11 = np.dot(K111, np.transpose(T))

    # assemble the 12x12 element stiffness matrix
    K_upper = np.append(K11, K12, axis=1)
    K_lower = np.append(K21, K22, axis=1)
    Ke = np.append(K_upper, K_lower, axis=0)
    
    return Ke


#%%
def static_red_stiff_matrix(length, alpha, beta, gamma, delta, epsilon):
    
    """  
    The function builds the 12x12 stiffness matrix of a flexibility element, by means
    of alpha, beta, gamma, delta, epsilon and the element length
    
    author: Lukas Braun

    Parameters
    ----------
    length : float
        axial length of the element.
    alpha, beta, gamma, delta, epsilon: float
        flexibility coefficients.

    Returns
    ----------
    Ke : array with floats
        Element stiffness matrix

    """

    # Flexibility matrix
    Flex = np.array([[alpha,    0,      0,      0,      gamma,  0],
                     [0,        alpha,  0,      -gamma, 0,      0],
                     [0,        0,      delta,  0,      0,      0],
                     [0,        -gamma, 0,      beta,   0,      0],
                     [gamma,    0,      0,      0,      beta,   0],
                     [0,        0,      0,      0,      0,      epsilon]])

    # Transformation matrix (from f_left + T* f_right = 0)
    T = np.array([[1,       0,          0, 0, 0, 0],
                  [0,       1,          0, 0, 0, 0],
                  [0,       0,          1, 0, 0, 0],
                  [0,       -length,    0, 1, 0, 0],
                  [length,  0,          0, 0, 1, 0],
                  [0,       0,          0, 0, 0, 1]])
    
    # get the sub-matrices of the 12x12 element stiffness matrix
    K22 = np.linalg.inv(Flex)
    K12 = -np.dot(T, K22)
    K21 = -np.dot(K22, np.transpose(T))
    K111 = np.dot(T, K22)
    K11 = np.dot(K111, np.transpose(T))

    # assemble the 12x12 element stiffness matrix
    K_upper = np.append(K11, K12, axis=1)
    K_lower = np.append(K21, K22, axis=1)
    Ke = np.append(K_upper, K_lower, axis=0)
    
    return Ke


#%%
def static_red_lumped_matrix(m, Ip, Id):
    
    """  
    The function builds the 12x12 lumped mass and gyroscopic matrices of a flexibility element. The mass is equally
    distributed at the two end nodes of the element.

    author: Lukas Braun
    
    Parameters
    ----------
    mass : float
        mass of the element.
    Ip: float
        polar moment of inertia.
    Id: float
        diametral moment of inertia.
    
    Returns
    ----------
    Me : array with floats
        element mass matrix.
    Ge : array with floats
        element gyroscopic matrix.

    """

    # lumped mass matrix
    Me = np.array([[m/2   ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,m/2   ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,m/2   ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,Id/2  ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,Id/2  ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,Ip/2  ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,m/2   ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,m/2   ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,m/2   ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,Id/2  ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,Id/2  ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,Ip/2  ]])

    # lumped gyroscopic matrix
    Ge = np.array([[0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,Id/2  ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,-Id/2 ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,Id/2  ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,-Id/2 ,0     ,0     ],
                   [0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ,0     ]])
    
    return Me, Ge
