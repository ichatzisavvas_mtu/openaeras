"""
This example shows how the stiffness matrix / flexibility coefficients of a 3D element can be obtained with the
routine implemented in openAERAS, with interfaces to the software tools CalculiX, Gmsh and Nastran.

SECOND PART

author: Lukas Braun

"""

from openAERAS.Structure_Modeling import element_3D
from openAERAS.External_Software.CalculiX import flexibility_coefficients

# Define the name of the input file
filename = "3D_example"

# FLEXIBILITY COEFFICIENT EXTRACTION
# the .dat file written by CalculiX is read and the five flexibility coefficients are extracted and written into
# a text file with the name "FILENNAME_coefficients.txt"
# The coefficients can now be used for the definition of a flexibility element, for example in Nastran as GENEL
coefficients = flexibility_coefficients.flex_coefficients_extract(filename)

# TRANSFORM FLEXIBILITY COEFFICIENTS INTO A 12x12 STIFFNESS MATRIX
alpha = coefficients[0]
beta = coefficients[1]
gamma = coefficients[2]
delta = coefficients[3]
epsilon = coefficients[4]
Ke = element_3D.element_3D_stiff_matrix(filename, alpha, beta, gamma, delta, epsilon)
