"""
The modules `Eigenfrequency Analysis` is used to performed the eigenvalue analysis.

Calculations:
    1. Eigefrequencies and Eigenvectors
    2. Campbell Diagram
    3. Critical Speeds (excited by unbalance)
    4. Strain Energy - Percentage and Absolute Value (Kinetic Energy is also calculated)
"""


import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from openAERAS.Bearings.Radial_Bearings.linear_bearings import calcs_linear_bearing
from openAERAS.Structure_Modeling.beam_help_function import span_dof, n_rotors, lateral_dof
import scipy as sp




#%%
def calcs_eigenfrequency_analysis (M, C, G, K, rotor_casing_geometry, 
                                   ax_eq=1, omega_max=500*2*np.pi, 
                                   fmax=500, n_camp=20, n_eigs=6, 
                                   modes_plot=2, modes_plot_speed=2, 
                                   mode_tracking = False, eig_animate = False, n_p_e = 6):
    """ 
    Main function for the eigenfrequency calculations.
        
    Parameters
    ----------
    M: array with floats
        assembled global mass matrix of structure (bearings & span coupling not included).
    C: array with floats
        assembled global damping matrix (bearings & span coupling not included).
    G: array with floats
        assembled global gyroscopic matrix.
    K: array with floats
        assembled global stiffness matrix (bearings & span coupling not included).
    rotor_casing_geometry : list 
        all rotor/casing/bearing information of the system.
    ax_eq : TYPE, optional
        DESCRIPTION. The default is 1.
    omega_max : TYPE, optional
        DESCRIPTION. The default is 500*2*np.pi.
    fmax : TYPE, optional
        DESCRIPTION. The default is 500.
    n_camp : TYPE, optional
        DESCRIPTION. The default is 20.
    n_eigs : TYPE, optional
        DESCRIPTION. The default is 6.
    modes_plot : TYPE, optional
        DESCRIPTION. The default is 2.
    modes_plot_speed : TYPE, optional
        DESCRIPTION. The default is 2.
    mode_tracking : TYPE, optional
        DESCRIPTION. The default is False.

    Returns
    -------
    None.        
    
    """
    # Minimun frequency for the Campbell
    omega_min = 0
    
    # rotor DoFs
    r_dofs, r_dof_span, r_dof_span_start = span_dof(rotor_casing_geometry, "rotor")
    
    # Number of bearings
    n_bearings = len(rotor_casing_geometry[4])
    
    # Check if there is speed dependent bearing
    n_speed_dep = []
    for bearing in range(n_bearings):
        if rotor_casing_geometry[4][bearing][0] == "linear_bearing_vc":
            n_speed_dep.append(bearing)
    
    # eigenfrequencies & eigenvalues calculation for constant coeff. and speed dependent coeff.    
    if not n_speed_dep: # if bearing coeff. are constant
        C_b, K_b = calcs_linear_bearing(rotor_casing_geometry, C, K)
        omega = np.linspace(omega_min, omega_max, n_camp)
        g_value, g_vector, AA = eigen_v_v (M, C_b, G, K_b, omega, rotor_casing_geometry, 
                                           r_dof_span, r_dof_span_start, n_p_e, n_eigs)
    else:
        pass
    

    # Sort Eigenvectors according to eigenvalues imaginary part
    g_value_p_i = [] # eigenvalues - plot - imaginary
    g_vector_s_i = [] # eigenvector sorted according to imag eigenvalues 
    for ii in range (len(omega)):
        idx = np.abs(g_value[ii]).argsort()   
        g_value_p_i.append(np.imag(g_value[ii][idx]))
        g_vector_s_i.append(g_vector[ii][:,idx])
        
    # Sort Eigenvectors according to eigenvalues absolute value
    g_value_p_a = [] # eigenvalues - plot - absolute
    g_vector_s_a = [] # eigenvector sorted according to absolute value of eigenvalues 
    for ii in range (len(omega)):
        idx = np.abs(g_value[ii]).argsort()   
        g_value_p_a.append(np.abs(g_value[ii][idx]))
        g_vector_s_a.append(g_vector[ii][:,idx])
        
    # Check if mode tracking is needed
    if mode_tracking is True:
        g_vector_s_i_mac, g_value_p_i = g_mode_tracking (g_vector_s_i, g_value_p_i, omega, n_eigs)
    
    g_camp = []
    g_camp_Hz = []
    for i in range (n_eigs):
        g_camp_temp = []
        g_camp_temp_Hz = []
        for ii in range (len(omega)):
            g_camp_temp.append(g_value_p_i[ii][i])
            #g_camp_temp_Hz.append(g_value_p_i[ii][i]/2/np.pi)
            g_camp_temp_Hz.append(np.abs(g_value_p_i[ii][0:n_eigs]/2/np.pi))
        g_camp.append(g_camp_temp)
        g_camp_Hz.append(g_camp_temp_Hz)
    

    # Plot Campbell Diagram
    campbell_diagram(omega, g_camp_Hz, fmax, n_eigs, rotor_casing_geometry)
    
    # Plot Mode Shapes
    plot_modeshapes(M, modes_plot, modes_plot_speed, g_vector_s_i, omega, n_p_e, eig_animate)
    
    
    return g_vector_s_i, g_value_p_i,  AA, g_value
#%%
def eigen_v_v(M, C_b, G, K_b, omega, rotor_casing_geometry, r_dof_span, r_dof_span_start, n_p_e, n_eigs):
    """
    Calculate eingevalues and eingevectors or the linear system of equation.

    Parameters
    ----------
    M : TYPE
        DESCRIPTION.
    C_b : TYPE
        DESCRIPTION.
    G : TYPE
        DESCRIPTION.
    K_b : TYPE
        DESCRIPTION.
    omega : TYPE
        DESCRIPTION.
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    r_dof_span : TYPE
        DESCRIPTION.
    r_dof_span_start : TYPE
        DESCRIPTION.

    Returns
    -------
    g_value : TYPE
        DESCRIPTION.
    g_vector : TYPE
        DESCRIPTION.
    AA : TYPE
        DESCRIPTION.

    """
    # Eigenvalue analyis    
    ndof = len (M)
    if n_p_e == 4:
        Matrices= lateral_dof(M, C_b, K_b)
        M = Matrices[0]
        C_b = Matrices[1]
        K_b = Matrices[2]
    g_value = []
    g_vector= []
    G_speed_factor = np.zeros([ndof,ndof]) # gyroscopic matrix
    for i in range (len(omega)):
        for span in range(len(rotor_casing_geometry[0])):
            speed_ratio = rotor_casing_geometry[0][span][0][4][1][0]
            G_speed_factor[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]] = \
                G[r_dof_span_start[span]:r_dof_span_start[span] + r_dof_span[span]]  * speed_ratio
        # Matrix reduction
        if n_p_e == 4:
            Matrices = lateral_dof(G_speed_factor)
            G1 = Matrices[0]
            ndof = len (M)
        else:
            G1 = G_speed_factor
        C = C_b + omega[i]*G1
        
        A11 =  np.zeros([ndof,ndof])
        A12 =  np.eye(ndof,ndof)
        A21 = -np.linalg.inv(M).dot(K_b)
        A22 = -np.linalg.inv(M).dot(C)
        AA = np.concatenate((np.concatenate((A11,A12),axis=1),np.concatenate((A21,A22),axis=1)),axis=0)
        w, v = np.linalg.eig (AA) 
        #w, v = sp.sparse.linalg.eigs (AA, 10)
        g_value.append(w)
        g_vector.append(v)


    # xx2 = np.sort(np.imag(w)/2/np.pi)
    return g_value, g_vector, AA

#%%	
def critical_speed(M0, C0_b, C1, K0_b, omega):
    '''
    Calculation of the critical speeds of the rotor bearing system
	
	Input
		M0
		C0_b
		C1
		K0_b
		omega
	Output
		g_value
		g_vector
		AA
    '''

    return 


#%%	
def campbell_diagram(omega, g_camp_Hz, fmax, n_eigs, rotor_casing_geometry):
    '''
    Calculation and plot of the Campbell Diagram
	
	Input
		M0
		C0_b
		C1
		K0_b
		omega
	Output
		g_value
		g_vector
		AA
    '''
    
    rotor_in_model = n_rotors(rotor_casing_geometry)
    
    # Plot Campbell Diagramm - Undamped Natural Frequencies
    fig, ax = plt.subplots()
    for ii in range (n_eigs):
        ax.plot(omega/2/np.pi*60,g_camp_Hz[ii],'-s')
    ax.set_xlabel ('Rotational Speed [rpm]')
    ax.set_ylabel ('Undamped Eigenfrequncy [Hz]')
    ax.grid(color='k', linestyle='--', linewidth=0.2)
    ax.set_xlim(omega[0]/2/np.pi*60, omega[-1]/2/np.pi*60)
    ax.set_ylim(0, fmax)
    for rotor in rotor_in_model:
        for span in range(len(rotor_casing_geometry[0])):
            if rotor_casing_geometry[0][span][0][4][0] == rotor:
                rotor_speed_factor = rotor_casing_geometry[0][span][0][4][1][0]
                ax.plot(omega/2/np.pi*60,rotor_speed_factor*omega/2/np.pi,'--')
                break
    
    return 


#%% Plot Mode Shapes
def plot_modeshapes(M, modes_plot, modes_plot_speed,  g_vector_s_i, omega, n_p_e, eig_animate = True):
    '''
    Plot the mode shapes
    '''
    if n_p_e == 4:
        Matrices= lateral_dof(M)
        M = Matrices[0]
    N = len(M)
    #print (modes_plot, modes_plot_speed)
    for jj in modes_plot_speed:
        for mode_num in modes_plot:
            fig =  plt.figure()
            ax = fig.add_subplot(111, projection = "3d")
            #ax.plot(g_vector_s_i[jj-1][N+ii-1,N+1::4])
            # x_mode = np.abs(g_vector_s_i[jj][0:N,mode_num*2][0::6])
            # y_mode = np.abs(g_vector_s_i[jj][0:N,mode_num*2][1::6])
            xx = g_vector_s_i[jj][0:N,mode_num*2][0::n_p_e]
            x_mode_ap = np.array([np.abs(item)*np.cos(np.angle(item)) for item in  xx])
            yy = g_vector_s_i[jj][0:N,mode_num*2][1::n_p_e]
            y_mode_ap = np.array([np.abs(item)*np.cos(np.angle(item)) for item in  yy])
            ax.plot(np.arange(N/n_p_e), x_mode_ap , y_mode_ap ,'c')
            ax.set_title ('Mode Shape \n Rotational Speed %f rpm \n Mode %d' %(omega[jj-1], mode_num))
            plt.show()
            
            
    
   
    
            
    # if eig_animate == True:
    #     fig =  plt.figure()
    #     ax = fig.add_subplot(111, projection = "3d")
        
    #     for jj in modes_plot_speed:
    #         for mode_num in modes_plot:
    #             #mode_num = 4
    #             plot_ani = np.linspace(0,1,360)
 
                
    #             xx = g_vector_s_i[jj][0:N,mode_num*2][0::6]
    #             x_mode_ap = np.array([np.abs(item)*np.cos(np.angle(item)) for item in  xx])
    #             yy = g_vector_s_i[jj][0:N,mode_num*2][1::6]
    #             y_mode_ap = np.array([np.abs(item)*np.cos(np.angle(item)) for item in  yy])
                
    #             ax.set_ylim([-np.max(np.abs(x_mode_ap)), np.max(np.abs(x_mode_ap))])
    #             ax.set_zlim([-np.max(np.abs(y_mode_ap)), np.max(np.abs(y_mode_ap))])
    #             ax.plot(np.arange(7), np.zeros(7), 0, 'k-.')
    #             for ii in range(len(plot_ani)):
    #                 l1 = ax.plot(np.arange(7), x_mode_ap * np.cos(2*np.pi*20*plot_ani)[ii], y_mode_ap * np.cos(np.pi/2 + 2*np.pi*20*plot_ani)[ii],'c')
    #                 plt.pause(0.1)
    #                 l1.pop(0).remove()
        
            
    return 


#%%
def g_mode_tracking (g_vector_s_i, g_value_p_i, omega, n_eigs):
    """
    Mode tracking for the Campbell Diagram.

    Parameters
    ----------
    g_vector_s_i : TYPE
        DESCRIPTION.
    g_value_p_i : TYPE
        DESCRIPTION.
    omega : TYPE
        DESCRIPTION.
    n_eigs : TYPE
        DESCRIPTION.

    Returns
    -------
    g_vector_s_i_mac : TYPE
        DESCRIPTION.
    g_value_p_i : TYPE
        DESCRIPTION.

    """
    # Accumulate all eigenvectors
    g_vector_s_i_mac = []
    for ii in range (len(omega)):
        g_vector_s_i_mac.append(g_vector_s_i[ii][:,0:(n_eigs+1)])
    
    # Mode tracking
    for ii in range(len(omega)-1):
        for jj in range(n_eigs):
            x_t1 = g_vector_s_i_mac [ii][:,jj]
            for kk in range(n_eigs):
                x_t2 = g_vector_s_i_mac [ii][:,kk]
                xx = np.abs((np.sum(x_t1 * x_t2) + np.sum(np.conj(x_t1) * x_t2) )**2 /
                        (np.sum(x_t1 * x_t1) + np.sum(np.conj(x_t1) * x_t1) *
                          np.sum(x_t2 * x_t2) + np.sum(np.conj(x_t2) * x_t2) ))
                if xx > 0.95:
                    g_vector_s_i_mac [ii+1][:,kk] = g_vector_s_i_mac [ii+1][:,jj]
                    g_value_p_i [ii+1][kk] = g_value_p_i [ii+1][jj]
                    #print(str(kk+1), str(jj+1))  
            print ('Eigs=', str(jj))
  
    return g_vector_s_i_mac, g_value_p_i
    


