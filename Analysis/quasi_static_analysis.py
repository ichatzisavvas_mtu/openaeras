"""
The modules `Quasi Static Analysis` is used to performed the quasi static analysis.

A quasi static analysis usually of one of the followng
- g loads: The response of the system is due to gravity loads in all directions x,y,z
- gyro loads
Then the Critical Speeds are calculated.
Finally, a Campbell diagram can be drawn
"""

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from openAERAS.Bearings.Radial_Bearings.linear_bearings import calcs_linear_bearing
from openAERAS.Structure_Modeling.beam_help_function import span_dof, n_rotors, lateral_dof





#%%
def calcs_quasi_static_analysis (M, C, G, K, rotor_casing_geometry, 
                                   qs_ax_eq, qs_n_p_e, analysis_type ):
    """
    

    Parameters
    ----------
    M : TYPE
        DESCRIPTION.
    C : TYPE
        DESCRIPTION.
    G : TYPE
        DESCRIPTION.
    K : TYPE
        DESCRIPTION.
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    ax_eq : TYPE, optional
        DESCRIPTION. The default is 1.
    omega_max : TYPE, optional
        DESCRIPTION. The default is 500*2*np.pi.
    fmax : TYPE, optional
        DESCRIPTION. The default is 500.
    n_camp : TYPE, optional
        DESCRIPTION. The default is 20.
    n_eigs : TYPE, optional
        DESCRIPTION. The default is 6.
    modes_plot : TYPE, optional
        DESCRIPTION. The default is 2.
    modes_plot_speed : TYPE, optional
        DESCRIPTION. The default is 2.
    mode_tracking : TYPE, optional
        DESCRIPTION. The default is False.
    ea_full_map : TYPE, optional
        DESCRIPTION. The default is "half".

    Returns
    -------
    None.

    """
    # rotor DoFs
    r_dofs, r_dof_span, r_dof_span_start = span_dof(rotor_casing_geometry, "rotor", qs_n_p_e)
    c_dofs, c_dof_span, c_dof_span_start = span_dof(rotor_casing_geometry, "casing", qs_n_p_e)
    

    
    # Number of bearings
    n_bearings = len(rotor_casing_geometry[4])
    # Check if there is speed dependent bearing
    n_speed_dep = []
    for bearing in range(n_bearings):
        if not rotor_casing_geometry[4][bearing][0] == "linear_bearing_cc":
            # try:
            n_speed_dep.append(bearing)
            print ('Only Constant Coefficient Linear Bearings are allowed')
            raise Exception("Only Constant Coefficient Linear Bearings are allowed")
            # except:
            #     pass
    C_b, K_b = calcs_linear_bearing(rotor_casing_geometry, C, K, qs_n_p_e)
    
    if qs_n_p_e == 4:
        Matrices = lateral_dof(K_b)
        K_b = Matrices[0]
        
    #check which type of analysis is to be performed
    if analysis_type[0][0] == True:
        pass#g_loads_analysis():
    elif analysis_type[1][0] == True:
        pass#gyro_loads_analysis():
    elif analysis_type[2][0] == True:
        pass#gyro_loads_acc_analysis():
    elif analysis_type[3] == True:
        g_force = rotor_casing_geometry[5][1]
        disp = g_static_analysis (K_b, g_force, rotor_casing_geometry, qs_n_p_e)
        return disp

    

    return None


#%%
def calcs_quasi_static_analysis_flex (M, C, G, K, rotor_casing_geometry, 
                                   qs_ax_eq, qs_n_p_e, analysis_type, subcase):
    """
    Function for determining the flexibility coefficients.

    Parameters
    ----------
    M : TYPE
        Mass matrix
    C : TYPE
        Damping matrix
    G : TYPE
        Gyroscopic matrix
    K : TYPE
        Stiffness matrix
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    qs_ax_eq : TYPE, optional
        DESCRIPTION. The default is 1.
    qs_n_p_e: TYPE, 6 or 4
        4: without torsion
    analysis_type: TYPE
        DESCRIPTION.
    subcase: TYPE, 
        number of subcase (e.g. forcing).

    Returns
    -------
    None.

    """
    
    # rotor DoFs
    r_dofs, r_dof_span, r_dof_span_start = span_dof(rotor_casing_geometry, "rotor", qs_n_p_e)
    
    # Number of bearings
    n_bearings = len(rotor_casing_geometry[4])
    # Check if there is speed dependent bearing
    n_speed_dep = []
    for bearing in range(n_bearings):
        if not rotor_casing_geometry[4][bearing][0] == "linear_bearing_cc":
            # try:
            n_speed_dep.append(bearing)
            print('Only Constant Coefficient Linear Bearings are allowed')
            raise Exception("Only Constant Coefficient Linear Bearings are allowed")
            # except:
            #     pass
    C_b, K_b = calcs_linear_bearing(rotor_casing_geometry, C, K)
    
    # Select type of analysis
    if analysis_type == "static_force":
        g_force = rotor_casing_geometry[7][subcase-1]
        disp = g_static_analysis_flex(K_b, g_force, rotor_casing_geometry, qs_n_p_e)
        return disp

    return


#%%
def g_loads_analysis (M, K_b, g_factor):
    """
    Use this analysis to investigate the influency of gravity loads.
    
    This analysis is considered quasi static, however, it is a purely static analysis.
    The g load (or a multiple of this value)
    
    

    Parameters
    ----------
    M : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    # number of DoF
    ndof = len(M)
    
    # diagonal elements of mass matrix
    mass_diag = np.diag(M)
    
    # Force vector initialization
    fx = np.zeros(ndof)
    fy = np.zeros(ndof)
    fz = np.zeros(ndof)

    
#%% 
def g_static_analysis (K, g_force, rotor_casing_geometry, qs_n_p_e):
    """
    Perform purely static analysis.

    Parameters
    ----------
    K : TYPE
        Stiffness Matrix.
    g_force : TYPE
        force & DoF to be applied.
    rotor_casing_geometry : TYPE
        Description.    

    Returns
    -------
    None.

    """
    r_dofs, r_dof_span, r_dof_span_start = span_dof(rotor_casing_geometry, "rotor", qs_n_p_e)
    c_dofs, c_dof_span, c_dof_span_start = span_dof(rotor_casing_geometry, "casing", qs_n_p_e)
    
    c_dof_span_start_all = [c_dof_span_start[i] + r_dof_span_start[-1] +  r_dof_span[-1]\
                            for i in range(len(c_dof_span_start))]

    g_force_applied = np.zeros([len(K),1])
    
    for fn in range(len(g_force)):#force numbers
        if g_force[fn][0][0] == "rotor":
            span = g_force[fn][0][1]
            dof = g_force[fn][0][2] 
            dof_force = r_dof_span_start[span-1] + (dof-1) * qs_n_p_e
            if qs_n_p_e == 4:
                g_force_applied[dof_force] = g_force[fn][1][0]
                g_force_applied[dof_force+1] = g_force[fn][1][1]
                g_force_applied[dof_force+2] = g_force[fn][1][3]
                g_force_applied[dof_force+3] = g_force[fn][1][4]
            elif qs_n_p_e == 6:
                g_force_applied[dof_force] = g_force[fn][1][0]
                g_force_applied[dof_force+1] = g_force[fn][1][1]
                g_force_applied[dof_force+2] = g_force[fn][1][2]
                g_force_applied[dof_force+3] = g_force[fn][1][3]
                g_force_applied[dof_force+4] = g_force[fn][1][4]
                g_force_applied[dof_force+5] = g_force[fn][1][5]
            
            
        elif g_force[fn][0][0] == "casing":
            span = g_force[fn][0][1]
            dof = g_force[fn][0][2] 
            dof_force = c_dof_span_start_all[span-1] + (dof-1) * qs_n_p_e
            if qs_n_p_e == 4:
                g_force_applied[dof_force] = g_force[fn][1][0]
                g_force_applied[dof_force+1] = g_force[fn][1][1]
                g_force_applied[dof_force+2] = g_force[fn][1][3]
                g_force_applied[dof_force+3] = g_force[fn][1][4]
            elif qs_n_p_e == 6:
                g_force_applied[dof_force] = g_force[fn][1][0]
                g_force_applied[dof_force+1] = g_force[fn][1][1]
                g_force_applied[dof_force+2] = g_force[fn][1][2]
                g_force_applied[dof_force+3] = g_force[fn][1][3]
                g_force_applied[dof_force+4] = g_force[fn][1][4]
                g_force_applied[dof_force+5] = g_force[fn][1][5]
        
    disp = sp.linalg.solve(K, g_force_applied)
    
    return disp
    
    
#%% 
def g_static_analysis_flex (K, g_force, rotor_casing_geometry, n_p_e):
    """
    Perform purely static analysis, 
    compatible with the unction for determing the coefficients
    of the flexibility matrix, calcs_quasi_static_analysis_flex

    Parameters
    ----------
    K : TYPE
        Stiffness Matrix.
    g_force : TYPE
        force & DoF to be applied.
    n_p_e: TYPE, 6 or 4
        4: without torsion    

    Returns
    -------
    Displacements and rotations due to forcing.

    """
    dofs, dof_span, dof_span_start = span_dof(rotor_casing_geometry, "rotor", n_p_e)

    g_force_applied = np.zeros([len(K),1])
    
    for fc in range(len(g_force[1])): # force components
        span = g_force[0][1]
        dof = g_force[0][2] 
        dof_force = dof_span_start[span-1] + (dof-1) * n_p_e
        g_force_applied[dof_force+fc] = g_force[1][fc]
        
    disp = sp.linalg.solve(K, g_force_applied)
    
    return disp
    
    
    
    