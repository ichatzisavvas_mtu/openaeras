----------------
openAERAS
----------------
openAERAS stands for Aircraft Engine Rotordynamic Analysis Software.

It is developed by Ioannis Chatzisavvas (mailto:ichatzisavvas@gmail.com) and Lukas Braun (mailto:lukas.braun1996@yahoo.com), 
and is distributed under the terms of the GNU General Public License, Version 3 or later.


----------------
About 
----------------
openAERAS is an open source software for rotordynamic analysis of aircraft engines, 
but also applicable to other turbo systems, e.g. turbocharger (automotive / marine) 
or stationary steam and gas turbines.

It aims to provide simple and efficient solutions to learning problems
that are accessible to everybody and reusable in various contexts. 


----------------
Installation and using the program
----------------
To be edited


----------------
Dependencies
----------------

* Gmsh
* matplotlib
* NumPy
* pandas
* pickle
* pyDOE
* pydot
* scikit-learn
* SciPy
* seaborn

The dependencies can all be installed with pip.
