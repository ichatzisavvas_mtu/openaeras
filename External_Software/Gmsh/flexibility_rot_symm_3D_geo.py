import numpy as np
import gmsh
import os


#%%  
def assemble_input_files(filenames_input, number_horizontal, number_vertical, number_rotational,
                         fix_stat, software, filename):

    """
    This function assembles several files that were previously exported.

    USER INFORMATION:
        -   It is also possible to mesh geometries of several linear elements with vertical steps in between.
            Therefore, an additional, very thin element (0.1 mm) is added automatically where either
            the outer or inner x coordinates remains constant over the thin element
        -   Right now, there exist no rule/recommendation concerning the mesh parameters. It is recommended to
            check the mesh quaility by means of the Jacobian determinant

    author: Lukas Braun
       
    Parameters
    ----------
    filenames_input: string
        names of the exported files
    number_horizontal : integer
        number of horizontal grid points (along the axial axis) - for each imported linear element.
    number_vertical : integer
        number of vertical grid points (perpendicular to the axial axis) - for each imported linear element.
    number_rotational : integer
        number of peripheral grid points - for a quarter section.
    fix_stat: integer
        fixation boundary conditions: = 0: whole surface, = 1: only inner circumferential line,
        = 2: inner and outer circumferential line
    software : integer
        solver for static analysis: = 0: CalculiX, = 1: Nastran, = 2: Abaqus.
    filename : string
        name of the input file defined in the beginning of the routine.
    
    Returns
    ----------
    None.

    """    
    
    # when should a step be detected? [mm]
    threshhold_stepsize = 0.2
    
    thickness_additional_el = 0.01
    
    number_of_files = len(filenames_input)

    # assemble files if more than one file was exported
    if number_of_files > 1:

        # read the exported files
        data = np.genfromtxt("%s.txt" % filenames_input[0], skip_header=1, delimiter=",", dtype=float)
        for i in range(1, number_of_files):
            row = np.genfromtxt("%s.txt" % filenames_input[i], skip_header=1, delimiter=",", dtype=float)
            data = np.vstack([data, row])
        
        # extract material
        E = data[0, 12]
        nu = data[0, 13]
        rho = data[0, 14]
        T = 500

        # write header
        f = open("%s.txt" % filename, "w")
        f.truncate(0)
        f.write("% Material: E [N/mm²], nu [-], T [K], rho [t/mm³]\n")
        f.write("% 3D mesh number horizontal grid points (for each partial linear element), "
                "number vertical grid points (for each partial linear element), "
                "number peripheral grid points (FOR A QUARTER SECTION)\n")
        f.write("% Boundary Conditions: =0: whole surface, =1: only inner circumferential line, "
                "=2: inner and outer circumferential line\n")
        f.write("% Solver: =0: CalculiX, =1: Nastran, =2: Abaqus\n")
        f.write("% Coordinates [mm]: x coord point 1, y coord point 1 , z coord point 1, etc. "
                "(inner left corner and then counter clockwise)\n")
        # write data
        f.write("%s, %s, %s, %s, %s, %s, %s, %s, %s" % (E, nu, T, rho, number_horizontal, number_vertical,
                                                        number_rotational, fix_stat, software))
            
        # inner radius line
        for i in range(number_of_files-1):
            diff_in = abs(data[i,4]-data[i+1,1])
            diff_out = abs(data[i,7]-data[i+1,10])
            if diff_in > threshhold_stepsize or diff_out > threshhold_stepsize:
                data[i,3] = data[i,3]-thickness_additional_el
                f.write(", %s,%s,%s" % (data[i,1], data[i,2], data[i,0]))
                f.write(", %s,%s,%s" % (data[i,4], data[i,5], data[i,3]))
            else:    
                f.write(", %s,%s,%s" % (data[i,1], data[i,2], data[i,0]))
        f.write(", %s,%s,%s" % (data[number_of_files-1,1], data[number_of_files-1,2], data[number_of_files-1,0]))
        f.write(", %s,%s,%s" % (data[number_of_files-1,4], data[number_of_files-1,5], data[number_of_files-1,3]))
    
        # outer radius line
        f.write(", %s,%s,%s" % (data[number_of_files-1,7], data[number_of_files-1,8], data[number_of_files-1,6]))
        f.write(", %s,%s,%s" % (data[number_of_files-1,10], data[number_of_files-1,11], data[number_of_files-1,9]))
        for i in range(number_of_files-1)[::-1]:
            diff_in = abs(data[i,4]-data[i+1,1])
            diff_out = abs(data[i,7]-data[i+1,10])
            if diff_in > threshhold_stepsize or diff_out > threshhold_stepsize:
                data[i,6] = data[i,6]-thickness_additional_el
                f.write(", %s,%s,%s" % (data[i,7], data[i,8], data[i,6]))
                f.write(", %s,%s,%s" % (data[i,10], data[i,11], data[i,9]))
            else:    
                f.write(", %s,%s,%s" % (data[i,10], data[i,11], data[i,9]))

    # no assembly necessary if only one file was exported
    else:

        # read the exported file
        data = np.genfromtxt("%s.txt" % filenames_input[0], skip_header=1, delimiter=",", dtype=float)
        
        # extract material
        E = data[12]
        nu = data[13]
        rho = data[14]
        T = 500

        # write header
        f = open("%s.txt" % filename, "w")
        f.truncate(0)
        f.write("% Material: E [N/mm²], nu [-], T [K], rho [t/mm³]\n")
        f.write("% 3D mes   h number horizontal grid points (for each partial linear element), "
                "number vertical grid points (for each partial linear element), "
                "number peripheral grid points (FOR A QUARTER SECTION)\n")
        f.write("% Boundary Conditions: =0: whole surface, =1: only inner circumferential line, "
                "=2: inner and outer circumferential line\n")
        f.write("% Coordinates [mm]: x coord point 1, y coord point 1 , z coord point 1, etc. "
                "(inner left corner and then counter clockwise)\n")
        f.write("% Solver: =0: CalculiX, =1: Nastran, =2: Abaqus\n")
        # write data
        f.write("%s, %s, %s, %s, %s, %s, %s, %s, %s" % (E, nu, T, rho, number_horizontal, number_vertical,
                                                      number_rotational, fix_stat, software))
        f.write(", %s,%s,%s" % (data[1], data[2], data[0]))
        f.write(", %s,%s,%s" % (data[4], data[5], data[3]))
        f.write(", %s,%s,%s" % (data[7], data[8], data[6]))
        f.write(", %s,%s,%s" % (data[10], data[11], data[9]))
    
    return None
        

#%%
def create_3D_mesh(filename): 
    
    """
    This function creates the 3D mesh with the software Gmsh.
    
    The following steps are performed in this function:
    1) The material data, mesh parameters and geometry are loaded from the input file
    2) The 2d geometry of the element is created
    3) The 3d geometry is extruded
    4) The 3d mesh is generated
    5) Physical groups for the static analysis are defined
    6) The mesh is exported

    The input file contains a list of parameters which are mentioned in the following:
        - Material: E, nu, T, rho,
        - 3D mesh: number horizontal grid points, number vertical grid points, number peripheral grid points
        - Fixation boundary condition
        - Solver
        - Coordinates: x coord point 1, y coord point 1 , z coord point 1, etc.
        (inner left corner and then counter clockwise).

    author: Lukas Braun
    
    Parameters
    ----------
    filename : string
        name of the input file defined in the beginning of the routine.
    
    Returns
    ----------
    None.

    """    
    
    gmsh.initialize()
    gmsh.model.add("%s" % filename)
    
    # material
    header = 5
    data = np.genfromtxt("%s.txt" % filename, skip_header=header, delimiter=",")
    E = data[0]
    nu = data[1]
    T = data[2]
    rho = data[3]
    
    # mesh parameter:
    # number of grid points in z axis direction
    number_horizontal = int(data[4])
    # number of grid points in x axis direction
    number_vertical = int(data[5])
    # number of grid points in peripheral direction (FOR A QUARTER SECTION)
    number_rotational = int(data[6])
    
    # specify nodes for fixation 
    #   whole surface (fix_stat = 0)
    #   only inner circumferential line (fix_stat = 1)
    #   inner and outer circumferential line (fix_stat = 2)
    fix_stat = data[7]
    
    # solver
    software = data[8]
    
    # read coordinates
    number_points = int((len(data)-9)/3)    
    for i in range(0, number_points):
        gmsh.model.geo.addPoint(data[9+3*i], data[10+3*i], data[11+3*i], i+1)
            
    # define points for static analysis
    p1 = gmsh.model.geo.addPoint(0, 0, 0, 1000001)
    p2 = gmsh.model.geo.addPoint(0, 0, data[11+3*number_points//2], 1000002)
    p3 = gmsh.model.geo.addPoint(1e-5, 0, data[11+3*number_points//2], 1000003)  # only for CalculiX
            
    # define lines            
    for j in range(number_points-1):
        gmsh.model.geo.addLine(j+1, j+2, j+1)    
    gmsh.model.geo.addLine(number_points, 1, number_points)  
        
    # define surface            
    gmsh.model.geo.addCurveLoop(np.linspace(1, number_points, number_points), 1)
    poly = gmsh.model.geo.addPlaneSurface([1], 1)
        
    # define transfinite curve and surface for a rectangular 2D grid      
    for i in range(0, number_points//2-1):
        gmsh.model.geo.mesh.setTransfiniteCurve(i, number_horizontal, "Progression", 1.0)
    for i in range(number_points//2, number_points-1):
        gmsh.model.geo.mesh.setTransfiniteCurve(i, number_horizontal, "Progression", 1.0)    
    gmsh.model.geo.mesh.setTransfiniteCurve(number_points, number_vertical, "Progression", 1.0)
    gmsh.model.geo.mesh.setTransfiniteCurve(number_points//2, number_vertical, "Progression", 1.0)
    gmsh.model.geo.mesh.setTransfiniteSurface(1, "Left", [1, number_points//2, number_points//2+1, number_points]) 
    gmsh.model.geo.mesh.setRecombine(2, 1)
            
    # revolve the whole surface as fixation
    if fix_stat == 0:
        # create nodes for fixation
        fix1 = gmsh.model.geo.revolve([(1,number_points)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix2 = gmsh.model.geo.revolve([(1,fix1[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix3 = gmsh.model.geo.revolve([(1,fix2[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix4 = gmsh.model.geo.revolve([(1,fix3[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        # create nodes for force and momentum     
        force1 = gmsh.model.geo.revolve([(1,number_points//2)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force2 = gmsh.model.geo.revolve([(1,force1[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force3 = gmsh.model.geo.revolve([(1,force2[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force4 = gmsh.model.geo.revolve([(1,force3[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
            
    # inner circumferential line as fixation
    elif fix_stat == 1:
        # create nodes for fixation
        fix1 = gmsh.model.geo.revolve([(0,1)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix2 = gmsh.model.geo.revolve([(0,fix1[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix3 = gmsh.model.geo.revolve([(0,fix2[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix4 = gmsh.model.geo.revolve([(0,fix3[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        # create nodes for force and momentum     
        force1 = gmsh.model.geo.revolve([(0,number_points//2)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force2 = gmsh.model.geo.revolve([(0,force1[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force3 = gmsh.model.geo.revolve([(0,force2[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force4 = gmsh.model.geo.revolve([(0,force3[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        
    # inner and circumferential line as fixation
    elif fix_stat == 2:
        # create nodes for fixation
        fix1 = gmsh.model.geo.revolve([(0,1)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix2 = gmsh.model.geo.revolve([(0,fix1[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix3 = gmsh.model.geo.revolve([(0,fix2[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix4 = gmsh.model.geo.revolve([(0,fix3[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix5 = gmsh.model.geo.revolve([(0,number_points)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix6 = gmsh.model.geo.revolve([(0,fix5[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix7 = gmsh.model.geo.revolve([(0,fix6[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        fix8 = gmsh.model.geo.revolve([(0,fix7[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        # create nodes for force and momentum     
        force1 = gmsh.model.geo.revolve([(0,number_points//2)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force2 = gmsh.model.geo.revolve([(0,force1[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force3 = gmsh.model.geo.revolve([(0,force2[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force4 = gmsh.model.geo.revolve([(0,force3[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force5 = gmsh.model.geo.revolve([(0,number_points//2+1)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force6 = gmsh.model.geo.revolve([(0,force5[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force7 = gmsh.model.geo.revolve([(0,force6[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
        force8 = gmsh.model.geo.revolve([(0,force7[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
            
    # create 3D geometry    
    vol1 = gmsh.model.geo.revolve([(2,1)],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
    vol2 = gmsh.model.geo.revolve([(2,vol1[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
    vol3 = gmsh.model.geo.revolve([(2,vol2[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
    vol4 = gmsh.model.geo.revolve([(2,vol3[0][1])],0,0,0,0,0,1,2*np.pi/4,[number_rotational], [], True)
            
    # create the 3D mesh (20 node brick)    
    gmsh.model.geo.synchronize()
    # with the following two commands a mesh of 20 node bricks is created
    gmsh.option.setNumber("Mesh.ElementOrder", 2)
    gmsh.option.setNumber("Mesh.SecondOrderIncomplete", 1)
    gmsh.model.mesh.generate(3)
            
    # define physical groups 
    # whole surface as fixation
    if fix_stat == 0:
        Fixation = gmsh.model.addPhysicalGroup(2, [fix1[1][1], fix2[1][1], fix3[1][1], fix4[1][1]])
        gmsh.model.setPhysicalName(2, Fixation, "Fixation")
        Force_moment = gmsh.model.addPhysicalGroup(2, [force1[1][1], force2[1][1], force3[1][1], force4[1][1]])
        gmsh.model.setPhysicalName(2, Force_moment, "Force_moment")
                
    # inner circumferential line as fixation    
    if fix_stat == 1:
        Fixation = gmsh.model.addPhysicalGroup(1, [fix1[1][1], fix2[1][1], fix3[1][1], fix4[1][1]])
        gmsh.model.setPhysicalName(1, Fixation, "Fixation")
        Force_moment = gmsh.model.addPhysicalGroup(1, [force1[1][1], force2[1][1], force3[1][1], force4[1][1]])
        gmsh.model.setPhysicalName(1, Force_moment, "Force_moment")     
        
    # inner and outer circumferential line as fixation    
    if fix_stat == 2:
        Fixation = gmsh.model.addPhysicalGroup(1, [fix1[1][1], fix2[1][1], fix3[1][1], fix4[1][1],
                                                   fix5[1][1], fix6[1][1], fix7[1][1], fix8[1][1]])
        gmsh.model.setPhysicalName(1, Fixation, "Fixation")
        Force_moment = gmsh.model.addPhysicalGroup(1, [force1[1][1], force2[1][1], force3[1][1], force4[1][1],
                                                       force5[1][1], force6[1][1], force7[1][1], force8[1][1]])
        gmsh.model.setPhysicalName(1, Force_moment, "Force_moment")     
            
    Volume = gmsh.model.addPhysicalGroup(3, [vol1[1][1], vol2[1][1], vol3[1][1], vol4[1][1]])
    gmsh.model.setPhysicalName(3, Volume, "Volume")
    REFNOD = gmsh.model.addPhysicalGroup(0, [p2])
    gmsh.model.setPhysicalName(0, REFNOD, "REFNOD")
    ROTNOD = gmsh.model.addPhysicalGroup(0, [p3])
    gmsh.model.setPhysicalName(0, ROTNOD, "ROTNOD")
    OUTPUT = gmsh.model.addPhysicalGroup(0, [p2, p3])
    gmsh.model.setPhysicalName(0, OUTPUT, "OUTPUT")
            
    # export the mesh   
    # export options
    gmsh.option.setNumber("Mesh.SaveGroupsOfElements", 1)
    gmsh.option.setNumber("Mesh.SaveGroupsOfNodes", 1)
    # gmsh.write("conical_geo_" + str(l+1) + ".geo_unrolled")
        
    if software == 0 or 2:
        gmsh.write("%s_raw.inp" % filename)
    if software == 1:
        gmsh.write("%s_raw.bdf" % filename)
            
    # in the following the .inp file is modified and adapted to CalculiX   
    flag = 1
    if software == 0 or 2:
        f = open("%s.inp" % filename, "w")
        f.truncate(0)
        # delete 2D elements in the .inp file 
        linelist = open("%s_raw.inp" % filename).readlines()
    if software == 1:
        f = open("%s.bdf" % filename, "w")
        f.truncate(0)
        # delete 2D elements in the .inp file 
        linelist = open("%s_raw.bdf" % filename).readlines()
        
    for line in linelist:
        if line.startswith("*ELEMENT, type=CPS8"):
            flag = 0
        if line.startswith("*ELEMENT, type=T3D3"):
            flag = 0   
        if line.startswith("*ELSET,ELSET=Fixation"):
            flag = 0   
        if line.startswith("*ELSET,ELSET=Force_moment"):
            flag = 0        
        if line.startswith("*ELEMENT, type=C3D20"):
            flag = 1
        if line.startswith("*ELSET,ELSET=Volume"):
            flag = 1    
        if flag:
             f.write(line)
    
    # delete file that included 2D elements
    if software == 0 or 2:
        os.remove("%s_raw.inp" % filename)
    if software == 1:
        os.remove("%s_raw.bdf" % filename)

    # finish gmsh  
    # gmsh.fltk.run()
    gmsh.finalize()                        
    
    return None
