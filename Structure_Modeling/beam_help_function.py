import numpy as np

#%%
def rotor_span_coupling (rotor_casing_geometry, K_r):
    """
    Coupling between spans of the same rotor

    Parameters
    ----------
    rotor_casing_geometry : TYPE
        DESCRIPTION.
    K_r : TYPE
        stiffness matrix

    Returns
    -------
    K_rc: TYPE
        stiffness matrix

    """
    # number of coupling points
    n_rotor_coupling = 0
    n_rotor_coupling_pos =[]
    n_spancoupl = len(rotor_casing_geometry[5])
    for n_rotor in range(n_spancoupl):
        if rotor_casing_geometry[5][n_rotor][0] == "rotor":
            n_rotor_coupling += 1
            n_rotor_coupling_pos.append(n_rotor)
            
    # calculate the DoFs per span and the starting DoF for each span
    dofs, dof_span, dof_span_start = rotor_span_dof(rotor_casing_geometry)
    
    K_rc = K_r # stiffness matrix
    # Implement coupling for the stiffness matrix
    for span in range(n_rotor_coupling):
        # identify the spans to be coupled
        span_1 = rotor_casing_geometry[5][n_rotor_coupling_pos[span]][1][0]
        span_2 = rotor_casing_geometry[5][n_rotor_coupling_pos[span]][2][0]
        # identify the DoFs to be coupled
        dof_tmp_1 = rotor_casing_geometry[5][n_rotor_coupling_pos[span]][1][1]
        dof_tmp_2 = rotor_casing_geometry[5][n_rotor_coupling_pos[span]][2][1]
        dof_1 = dof_span_start[span_1-1] + 6 * (dof_tmp_1 - 1)
        dof_2 = dof_span_start[span_2-1] + 6 * (dof_tmp_2 - 1)
        # DoFs for matrix
        dof_mat_1 = [dof_1, dof_1+1, dof_1+2, dof_1+3, dof_1+4, dof_1+5]
        dof_mat_2 = [dof_2, dof_2+1, dof_2+2, dof_2+3, dof_2+4, dof_2+5]
        # direct coupling values
        K_rc[dof_mat_1, dof_mat_1] = K_rc[dof_mat_1, dof_mat_1] + rotor_casing_geometry[5][n_rotor_coupling_pos[span]][3]
        K_rc[dof_mat_2, dof_mat_2] = K_rc[dof_mat_2, dof_mat_2] + rotor_casing_geometry[5][n_rotor_coupling_pos[span]][3]
        # cross coupling values
        K_rc[dof_mat_1, dof_mat_2] = K_rc[dof_mat_1, dof_mat_2] - rotor_casing_geometry[5][n_rotor_coupling_pos[span]][3]
        K_rc[dof_mat_2, dof_mat_1] = K_rc[dof_mat_2, dof_mat_1] - rotor_casing_geometry[5][n_rotor_coupling_pos[span]][3]
        
    
        
    return K_rc


#%%
def span_dof(rotor_casing_geometry, span_type, ndof=6):
    """
    Identify the number of DoFs in each span and the starting DoF for each span

    Parameters
    ----------
    rotor_casing_geometry : TYPE
        DESCRIPTION.
        
    ndof : TYPE
        DESCRIPTION. The default is 6.

    Returns
    -------
    dofs : TYPE
        total number of DoFs 
    dof_span : TYPE
        number of DoFs for each span
    dof_span_start : TYPE
        starting DoF of each span

    """    
    
    # Number of spans (all)
    if span_type == "rotor":
        nspans = len(rotor_casing_geometry[0])
        geometry = rotor_casing_geometry[0]
    elif span_type == "casing":
        nspans = len(rotor_casing_geometry[2])
        geometry = rotor_casing_geometry[2]
    
    # Calculate the total number of DoFs
    dof_span = []
    for span in range (nspans):
        dof_span.append(ndof*len(geometry[span]))
    dofs = int(np.sum(dof_span))
    
    if nspans == 0:
        dof_span = [0]
    
    # Calculate the starting/stoping DoF of each span
    dof_span_start = [0]
    for span in range (1,nspans):
        dof_span_start.append(np.sum(dof_span[0:span]))
        

    return  dofs, dof_span, dof_span_start


#%%
def lateral_dof(*matrices):
    """
    Removal of the axial and torsional DoF

    Parameters
    ----------
    M : TYPE
        DESCRIPTION.
    C : TYPE
        DESCRIPTION.
    G : TYPE
        DESCRIPTION.
    K : TYPE
        DESCRIPTION.

    Returns
    -------
    M : TYPE
        DESCRIPTION.
    C : TYPE
        DESCRIPTION.
    G : TYPE
        DESCRIPTION.
    K : TYPE
        DESCRIPTION.

    """
    red_mat = []
    for matrix in matrices:
        ndofs = len(matrix)
        dof_rem = np.arange(2, ndofs,3)
        matrix = np.delete(matrix, dof_rem,axis=0)
        matrix = np.delete(matrix, dof_rem,axis=1)
        red_mat.append(matrix)
        
    return red_mat

#%%
def n_rotors(rotor_casing_geometry):
    """
    Identify which rotors are included in the model.

    Parameters
    ----------
    rotor_casing_geometry : TYPE
        DESCRIPTION.

    Returns
    -------
    rotors_in_model : TYPE
        DESCRIPTION.

    """
    n_spans = rotor_casing_geometry[0]
    lp_rotor = []
    hp_rotor = []
    ip_rotor = []
    rotors_in_model = []
    for span in range (len(n_spans)):
        if rotor_casing_geometry[0][span][0][4][0] == "lp":
            lp_rotor.append(span)
        if rotor_casing_geometry[0][span][0][4][0] == "hp":
            hp_rotor.append(span)
        if rotor_casing_geometry[0][span][0][4][0] == "ip":
            ip_rotor.append(span)
    
    if lp_rotor:
        rotors_in_model.append("lp")
    if hp_rotor:
        rotors_in_model.append("hp")
    if ip_rotor:
        rotors_in_model.append("ip")
        
    return rotors_in_model
    

#%%
def rotor_mass(rotor_casing_geometry):
    """
    Use this routine to calculate the mass of the rotor.

    The following are calculated
    - mass per element / shaft + disks + blades
    - total mass
    - total mass shaft
    - total mass disk / blades

    Parameters
    ----------
    rotor_casing_geometry : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    pass

    return
