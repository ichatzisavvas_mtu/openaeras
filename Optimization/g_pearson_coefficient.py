import scipy as sp
import numpy as np
import pandas as pd

def g_pearson_coeff_calc(param_study, input_param, output_param):
    """
    Calculate and plot the Pearson coefficients
    
    Input
    
    Output
        
    """
    if isinstance (param_study, pd.DataFrame):
        pass
    else:
        param_study=(pd.read_csv(param_study, sep =","))
    
    p_coef = np.zeros([input_param, output_param])
    p_coef_p = np.zeros([input_param, output_param])
    for jj in range (input_param):
        for kk in range (output_param):
            print (param_study.iloc[:,jj+1])
            p_coef[jj,kk] = sp.stats.pearsonr(param_study.iloc[:,jj+1], param_study.iloc[:,input_param + kk + 1])[0]
            p_coef_p[jj,kk] = sp.stats.pearsonr(param_study.iloc[:,jj+1], param_study.iloc[:,input_param + kk + 1])[1]
            
    
    return p_coef, p_coef_p
